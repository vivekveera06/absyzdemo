({
	callopportunities : function(component, event, helper) {
		var action= component.get("c.getopportunities");
        action.setCallback(this,function(response){
            if(response.getState()){
                component.set("v.opps",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    handleEEvent : function(component, event, helper) {
        var s= event.getParam("searchS	tring");
        alert("s: "+s);
        component.set("v.str",s);
    }
})