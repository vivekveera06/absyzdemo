({
	myAction : function(component, event, helper) {
		
        
        var action = component.get('c.getAccountDetails');
             action.setCallback(this, function(actionResult) {
     component.set('v.accounts', actionResult.getReturnValue());
    });
    $A.enqueueAction(action);
	},
    openRelatedList: function(com, _event){
   var relatedListEvent = $A.get("e.force:navigateToRelatedList");
   relatedListEvent.setParams({
      "relatedListId": "Contacts",
      "parentRecordId": com.get("v.accounts.Id")
   });
   relatedListEvent.fire();
}
})