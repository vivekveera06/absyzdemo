({
    /*
     *  Map the Field to the desired component config, including specific attribute values
     *  Source: https://www.salesforce.com/us/developer/docs/apexcode/index_Left.htm#CSHID=apex_class_Schema_FieldSetMember.htm|StartTopic=Content%2Fapex_class_Schema_FieldSetMember.htm|SkinName=webhelp
     *
     *  Change the componentDef and attributes as needed for other components
     */
     configMap: {
        'anytype': { componentDef: 'ui:inputText', attributes: {} },
        'base64': { componentDef: 'ui:inputText', attributes: {} },
        'boolean': {componentDef: 'ui:inputCheckbox', attributes: {} },
        'combobox': { componentDef: 'ui:inputText', attributes: {} },
        'currency': { componentDef: 'ui:inputText', attributes: {} },
        'datacategorygroupreference': { componentDef: 'ui:inputText', attributes: {} },
        'date': {
            componentDef: 'ui:inputDate',
            attributes: {
                displayDatePicker: true,
                format: 'MM/dd/yyyy'
            }
        },
        'datetime': { componentDef: 'ui:inputDateTime', attributes: {} },
        'double': { componentDef: 'ui:inputNumber', attributes: {} },
        'email': { componentDef: 'ui:inputEmail', attributes: {} },
        'encryptedstring': { componentDef: 'ui:inputText', attributes: {} },
        'id': { componentDef: 'ui:inputText', attributes: {} },
        'integer': { componentDef: 'ui:inputNumber', attributes: {} },
        'percent': { componentDef: 'ui:inputNumber', attributes: {} },
        'phone': { componentDef: 'ui:inputPhone', attributes: {} },
        'reference': { componentDef: 'ui:inputText', attributes: {} },
        'string': { componentDef: 'ui:inputText', attributes: {} },
        'textarea': { componentDef: 'ui:inputText', attributes: {} },
        'time': { componentDef: 'ui:inputDateTime', attributes: {} },
        'url': { componentDef: 'ui:inputText', attributes: {} }
    },
    callToServer : function(component, method, callback, params, storable) {
        
        var action=component.get(method);
        if(storable){
            //  action.setStorable();
        }
        if(params){
            action.setParams(params);
        }
        console.log('after action----');
        action.setCallback(this,function(responsethis){
            
            console.log('In actiom.setCallback()-----'+responsethis.getState());
            var state = responsethis.getState();
            
            if (state === "SUCCESS") {
                console.log('inside success---'+responsethis.getReturnValue());
                callback.call(this, responsethis.getReturnValue());
            } 
            else if(state === "ERROR"){
                var errors = response.getError();
                 var errorMessage = response.getError()[0].message;
                    
                    toastEvent.setParams({
                        "title": "Error",
                        "message": "The record was not saved. Error: " + errorMessage,
                        "type": "error"
                    });
                    
                    toastEvent.fire();
                
            } else {
                console.log('error in else');
                /* component.set("v.alertMessage", "ERROR: Unknown Error");
                component.set("v.isAlert", true);*/
            }
        });
        $A.enqueueAction(action);
        
    },
    InitialMethod :function(component, event){
        
        
        var recordId = component.get("v.SingleWorkflow").Id;
        
         var fieldList = [];
        fieldList.push("Name__c");
        fieldList.push("Category__c");
        fieldList.push("IsChinaUser__c");
        fieldList.push("Predefined_Approver_01__c");
        
        var self = this;
        self.callToServer(
            component,
            "c.getForm",    /* server method */
            function(response) {
                
               
                console.log('FieldSetFormController getFormAction callback');
                
                
                if (component.isValid()) {        
                    var form = response;
                    component.set('v.fields', form.Fields);
                    component.set('v.record', form.Record);
                    var self2 = this;
                    self2.createForm(component);
                    //helper.createForm(cmp);
                }
            },
            {
                /*fieldSetName: fieldSetName,
                objectName: sobjectName,*/
                recordId: recordId,
                "fieldList":fieldList
                
            },
            true
        );
        
    },
    SaveMethod:function(component, event){
        
        var record = component.get('v.record');
        
        if (!record.sobjectType) {
            record.sobjectType = component.get('v.sObjectName');
        }
        
        var self = this;
        self.callToServer(
            component,
            "c.upsertRecord",    /* server method */
            function(response) {
                
				console.log('FieldSetFormController getFormAction callback');
                
                
                var toastEvent = $A.get("e.force:showToast");
                if (component.isValid()) {
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "The record has been upserted successfully.",
                        "type": "success"
                    });
                    
                    toastEvent.fire();
                    $A.get('e.force:refreshView').fire();
                }
            },
            {
                recordToUpsert: record
                
            },
            true
        );
        
    },
    createForm:function(component){
        
        console.log('FieldSetFormHelper.createForm');
        var fields = component.get('v.fields');
        var record = component.get('v.record');
        var inputDesc = [];
        
        for (var i = 0; i < fields.length; i++) {
            var field = fields[i];
            var type = field.Type.toLowerCase();
            console.log('type---'+type);
            
            var configTemplate = this.configMap[type];
            console.log('configTemplate--'+JSON.stringify(configTemplate));
            if (!configTemplate) {
                console.log('type ${ type } not supported');
                continue;
            }
            
            // Copy the config so that subsequent types don't overwrite a shared config for each type.
            var config = JSON.parse(JSON.stringify(configTemplate));
            
            config.attributes.label = field.Label;
            config.attributes.required = field.Required;
            config.attributes.value = component.getReference(' v.record.' + field.APIName);
            config.attributes.fieldPath = field.APIName;
            
            if (!config.attributes['class']) {
                config.attributes['class'] = 'slds-m-vertical_x-small';
            }
            
            inputDesc.push([
                config.componentDef,
                config.attributes
            ]);
        }
        
        $A.createComponents(inputDesc, function(cmps) {
            console.log('createComponents');
            
            component.set('v.body', cmps);
        });
        
    }
})