({
    insert : function(component, event, helper) {
        
        var action=component.get("c.InsertCon");
        action.setParams({
            "cc":component.get("v.con")
            
        });
        action.setCallback(this,function(a){
            
            var state=a.getState();
            if(state==='SUCCESS'){
                
                alert(state);
                var evt = $A.get("e.force:navigateToComponent");
                
                evt.setParams({
                    
                    componentDef :'c:AccordianCmp2'
                });
                evt.fire();
                
            }
            else if(state==='ERROR'){
                
               alert('error'+state); 
            }
        });
        $A.enqueueAction(action);
    },
     options:function(component, event, helper){
        var evt = $A.get("e.force:navigateToComponent");
                
                evt.setParams({
                    
                    componentDef :'c:AccordianCmp2'
                });
                evt.fire();
    },
    handleComponentEvent:function(component, event, helper){
        
        var message = event.getParam("lookup");
		component.set("v.accid",message);
        alert('account selected'+message);
    }
})