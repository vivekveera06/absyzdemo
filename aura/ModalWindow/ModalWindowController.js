({
    doInit : function(component, event, helper) {
        component.set("v.isOpen",true);
    },
    Destroy : function(component, event, helper) {
		helper.Destroy(component);
	}
})