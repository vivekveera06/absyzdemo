({
    Destroy : function(component, event) {
		component.destroy();
	},
    Cancel : function(component, event) {
		component.set("v.isOpen",false);
	}
})