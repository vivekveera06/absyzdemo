({
	doInit : function(component, event, helper) {
		
        $A.createComponent("c:AccountWizard", {}, 
                           function(newcmp){
            
                               if(component.isValid()){
                                   component.set("v.body",newcmp);
                                   alert(component.get("v.body"));
                               }
        });
    },
    showContact: function(component, event, helper){
        
        var acdata=event.getParam("accdata");
        var newcon=component.get("v.contacts");
        alert(' contacts '+JSON.stringify(newcon));
        component.set("v.accounts",acdata);
        alert('account data'+acdata);
        
        if(newcon==null){
            $A.createComponent("c:ContactWizard",{},
                           function(newcmp){
                               if(component.isValid()){
                                   component.set("v.body",newcmp);
                                   alert(component.get("v.body"));
                               }
                               
                           });
            component.set("v.progress","step2");
        }
        else{
              $A.createComponent("c:ContactWizard",{'con':newcon},
                           function(newcmp){
                               if(component.isValid()){
                                   component.set("v.body",newcmp);
                                   alert(component.get("v.body"));
                               }
                               
                           });
             component.set("v.progress","step2");
        }
        
    },
    showOpp:function(component, event, helper){
        
        var ccdata=event.getParam("condata");
        var newopp=component.get("v.opportunities");
        component.set("v.contacts",ccdata);
         alert('Return value'+JSON.stringify("v.contacts"));
        var prog=component.get("v.progress");
        component.set("v.progress",prog+50);
        if(newopp==null){
        	$A.createComponent("c:OppWizard", {}, 
                           function(newcmp){
            
                               if(component.isValid()){
                                   component.set("v.body",newcmp);
                                   alert(component.get("v.body"));
                               }
        	});
            component.set("v.progress","step3");
        }
    	else{
    		$A.createComponent("c:OppWizard", {'opp':newopp}, 
                           function(newcmp){
            
                               if(component.isValid()){
                                   component.set("v.body",newcmp);
                                   alert(component.get("v.body"));
                               }
        	});
    		component.set("v.progress","step3");
		}
 
    },
    showprev:function(component, event, helper){
      
        var value=event.getParam("prevdata");
        if(value==1){
            alert('entered 1 show account');
            
            var ccdata=event.getParam("condata");
            alert('contact values'+JSON.stringify(ccdata));
            component.set("v.contacts",ccdata);
        	alert('Return value'+JSON.stringify("v.contacts"));
            
            var oldacc=component.get("v.accounts");
          
            //component.set("v.body",oldacc);
            $A.createComponent("c:AccountWizard",{'acc':oldacc},
                           function(newcmp){
                               if(component.isValid()){
                                   component.set("v.body",newcmp);
                                   alert(JSON.stringify(component.get("v.body")));
                               }
                               
                           });
            
        	component.set("v.progress","step1");
            
        }
        else if(value==2){
            
            var oppvalues=event.getParam("oppdata");
            component.set("v.opportunities",oppvalues);
            
            var oldcon=component.get("v.contacts");
            alert('oldcon'+oldcon);
            $A.createComponent("c:ContactWizard", {'con':oldcon}, 
                           function(newcmp){
            
                               if(component.isValid()){
                                   component.set("v.body",newcmp);
                                   alert(component.get("v.body"));
                               }
        	});
            
        	component.set("v.progress","step2");
            
        }
        
    },
    Save:function(component, event, helper){
        
        
        var opp=event.getParam("oppdata");
        alert('opportunity'+opp);
        component.set("v.opportunities",opp);
        var acc=component.get("v.accounts");
        alert('account'+acc);
        var con=component.get("v.contacts");
        alert('contacts'+con);
        
        var action=component.get("c.insertRecords");
        action.setParams({
            "ac":acc,
            "co":con,
            "op":opp
        });
        action.setCallback(this,function(a){
            
            var state=a.getState();
            if(state==='SUCCESS'){
                alert('alert all records are inserted');
            }
            else {
                alert('failure');
            }
        });
        $A.enqueueAction(action);
        
    }
    
})