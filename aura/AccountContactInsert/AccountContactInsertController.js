({
	CreateContact : function(component, event, helper) {
		alert(JSON.stringify(component.get("v.accounts")));
        $A.createComponent("c:TestContactCmp",{},
                           function(newConCmp){
                               if(component.isValid()){
                                   component.set("v.body",newConCmp);
                               }
							});
	},
    Save:function(component, event, helper){
        var accnts=component.get("v.accounts");
        alert("accounts added"+ accnts);
        
        var con=component.get("v.body")[0].ContactMethod;
        component.set("v.contacts");
        
        // for validations on accounts.
        
        var inputname=component.find("accountname");
        
        if($A.util.isEmpty(accnts.Name)){
            
            inputname.set("v.errors",[{message:"Enter Account name"}]);
		}
        else{
            
            var action=component.set("c.insertRecords");
            action.setParams({
                
                "ac":component.set("v.accounts"),
                "co":component.set("v.contacts")
            });
            action.setCallback(this,function(a){
                var state=action.getState();
                if(state==='SUCCESS'){
                    
                    alert('inserted successfully');
                    
                }
            });
        }
        
    }
})