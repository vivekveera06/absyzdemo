({
	myAction : function(component, event, helper) {
		
        
        //var action=component.get("c.getOppStatus");
        var action = component.get("c.getOppStatus");
        var inputsel=component.find("pick");
        var opts=[];
        action.setCallback(this,function(a){
            
            for(var i=0;i<a.getReturnValue().length;i++){
			
                opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
 			}
            inputsel.set("v.options", opts);
        });
        $A.enqueueAction(action);
	}
})