({
    afterDragulaLoaded: function(component, event, helper) {
        
        // Components
        var container = component.find('container');
        var from = component.find('from-draggable');
        var to = component.find('to-draggable');
        
       alert('script loaded');
        
        // Dragula needs the DOM elements
        var drake = dragula([from.getElement(), to.getElement()], {
            direction: 'vertical',
            mirrorContainer: container.getElement()
        });
        
        // Show/hide the "Drag and Drop Here" item
        // $A.getCallback makes safe to invoke from outside the Lightning Component lifecycle
        drake.on('drop', $A.getCallback(function(el, target, source, sibling) {
            if (source.children.length <= 1) {
                //alert('first');
                alert('target'+target);
                $A.util.removeClass(component.find(helper.placekeeperAuraIdFor(source)), 'slds-hide');
            }
          	
            $A.util.addClass(component.find(helper.placekeeperAuraIdFor(target)), 'slds-hide');
        }));
        
      //helper.secondLoading(component);
                
        
    },
    afterDragulaLoaded2: function(component, event, helper){
        // Components
        var container = component.find('container');
        var from = component.find('from-draggable');
        var to = component.find('to-draggable');
        var too=component.find('to-draggable1');
       alert('script loaded 2nd');
        
        // Dragula needs the DOM elements
        var drake = dragula([from.getElement(), too.getElement()], {
            direction: 'vertical',
            mirrorContainer: container.getElement()
        });
        alert('2nd dragulla method');
        // Show/hide the "Drag and Drop Here" item
        // $A.getCallback makes safe to invoke from outside the Lightning Component lifecycle
        drake.on('drop', $A.getCallback(function(el, target, source, sibling) {
            if (source.children.length <= 1) {
                //alert('first');
                alert('target'+target);
                $A.util.removeClass(component.find(helper.placekeeperAuraIdFor2(source)), 'slds-hide');
            }
          	
            $A.util.addClass(component.find(helper.placekeeperAuraIdFor2(target)), 'slds-hide');
        }));
        
        
    },
    print : function(component, event, helper) {
        alert('do init loaded');
        
        var acc=component.get("c.go");
        acc.setCallback(this,function(response){
            var state=response.getState();
            if(state=='SUCCESS')
            {
                var resp=response.getReturnValue();
                component.set("v.fieldsList",resp);
            }
        });
        $A.enqueueAction(acc);
    },
    
    getfields: function(component, event, helper) {
       var a= component.find("picklist").get("v.value");
        if(a=='feedback')
        {
            component.set("v.jbround", true);
        }
        else
        {
          component.set("v.jbround", false);  
        }
    }
})