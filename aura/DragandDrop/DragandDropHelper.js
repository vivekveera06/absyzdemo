({
    placekeeperAuraIdFor: function(element) {
        alert('inside placekeeperAuraIdFor');
        // Hard to get from DOM back to aura:id so using classes as markers
        if (element.classList.contains('from-draggable')) return 'from-placekeeper';
        else if (element.classList.contains('to-draggable')) return 'to-placekeeper';
            else if (element.classList.contains('to-draggable1')) return 'to-placekeeper1';
                else return null;
    },
    placekeeperAuraIdFor2: function(element) {
        alert('2nd draggable');
        // Hard to get from DOM back to aura:id so using classes as markers
        if (element.classList.contains('from-draggable')) return 'from-placekeeper';
        else if (element.classList.contains('to-draggable1')) return 'to-placekeeper1';
            else return null;
    },
    secondLoading : function(component){
        var container = component.find('container');
        var from = component.find('from-draggable');
        //var to = component.find('to-draggable');
        var too=component.find('to-draggable1');
        
        alert('script loaded in javascript');
        
        // Dragula needs the DOM elements
        var drake = dragula([from.getElement(), too.getElement()], {
            direction: 'vertical',
            mirrorContainer: container.getElement()
        });
        
        // Show/hide the "Drag and Drop Here" item
        // $A.getCallback makes safe to invoke from outside the Lightning Component lifecycle
        drake.on('drop', $A.getCallback(function(el, target, source, sibling) {
            if (source.children.length <= 1) {
                //alert('first');
                $A.util.removeClass(component.find(helper.placekeeperAuraIdFor(source)), 'slds-hide');
            }
            
            $A.util.addClass(component.find(helper.placekeeperAuraIdFor(target)), 'slds-hide');
        }));
        
    }
})