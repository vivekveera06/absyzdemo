({
	doInit : function(component, event, helper) {
		var action = component.get("c.getaccounts");
        action.setCallback(this, function(a){
            var state = a.getState();
            alert(state);
            if(state == "SUCCESS"){
                component.set("v.accounts",a.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	}
})