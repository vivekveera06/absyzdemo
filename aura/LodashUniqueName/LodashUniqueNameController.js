({
	myAction : function(component, event, helper) {
		
	},
    doInit : function(component, event, helper) {
		
        var action = component.get('c.getAccountDetails');
     action.setCallback(this, function(actionResult) {
     component.set('v.accounts', actionResult.getReturnValue());
    });
    $A.enqueueAction(action);   
    },
    sortByName: function(component){
        
        var items = component.get("v.accounts");
        
        if(component.get("v.sort1") === "down"){
            
 		var uniqueItems = _.orderBy(items, function(x){
 		return x.Name;
 			},['desc']);
 		component.set("v.accounts", uniqueItems);
 		component.set("v.sort1", "up");
 		} else {
 			var uniqueItems = _.orderBy(items, function(x){
 		return x.Name;
 		},['asc']);
 			component.set("v.accounts", uniqueItems);
 			//component.set("v.sort1", "down");
 		}
 	}
        
        
    
    
})