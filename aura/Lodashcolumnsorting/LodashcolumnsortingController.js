({
	myAction : function(component, event, helper) {
		var action=component.get("c.getcontacts");
        action.setCallback(this, function(actionResult) {
     component.set('v.contacts', actionResult.getReturnValue());
    });
    $A.enqueueAction(action);       
	},
    sortByFirstName : function(component) {
 		var items = component.get("v.contacts");
 		if(component.get("v.sort1") === "down")
        {
 		var uniqueItems = _.orderBy(items, function(x){
 		return x.FirstName;
 		},['desc']);
 			component.set("v.contacts", uniqueItems);
 			component.set("v.sort1", "up");
 		}
        else 
        {
 			var uniqueItems = _.orderBy(items, function(x){
 			return x.FirstName;
 			},['asc']);
 			component.set("v.contacts", uniqueItems);
 			component.set("v.sort1", "down");
 		}
 	},
    sortByLastName : function(component) {
 		var items = component.get("v.contacts");
 		if(component.get("v.sort2") === "down"){
 		var uniqueItems = _.orderBy(items, function(x){
		 return x.LastName;
 		},['desc']);
 		component.set("v.contacts", uniqueItems);
 			component.set("v.sort2", "up");
 		} else {
 		var uniqueItems = _.orderBy(items, function(x){
 			return x.LastName;
 		},['asc']);
 			component.set("v.contacts", uniqueItems);
 					component.set("v.sort1", "down");
 		}
     },
        sortByDescription : function(component) {
 		var items = component.get("v.contacts");
 		if(component.get("v.sort3") === "down"){
 		var uniqueItems = _.orderBy(items, function(x){
		 return x.Description;
 		},['desc']);
 		component.set("v.contacts", uniqueItems);
 			component.set("v.sort3", "up");
 		} else {
 		var uniqueItems = _.orderBy(items, function(x){
 			return x.Description;
 		},['asc']);
 			component.set("v.contacts", uniqueItems);
 					component.set("v.sort3", "down");
 		}
     }
})