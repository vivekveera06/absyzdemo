({
	AddNewRow : function(component, event, helper) {
		component.getEvent("AddRowEvt").fire();
	},
    removeRow: function(component, event, helper){
        var evt=component.getEvent("RemoveRow");
        evt.setParams({
            indexno:component.get("v.rowIndex")
        });
		evt.fire();
    }

})