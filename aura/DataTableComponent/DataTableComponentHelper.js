({
    sortData: function (component, fieldName, sortDirection) {
        var data = component.get("v.filteredData");
        var reverse = sortDirection !== "asc";
        data.sort(this.sortBy(fieldName, reverse))
        component.set("v.filteredData", data);
    },
    
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) { return primer(x[field]) } :
        function(x) { return x[field] };
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    },
    
    findByName: function(component, event) {  
        var searchKey = event.getParam("searchKey");
        var AllRecordList = component.get("v.AllRecordList");
        var searchList = [];
        
        if(searchKey.length == 0) {
            for(var i=0; i<AllRecordList.length; i++) {
                searchList.push(AllRecordList[i]);
            }
        }
        
        else if(searchKey.length >= 3) {
            for(var i=0; i<AllRecordList.length; i++){
                var tempRecord = AllRecordList[i];
                for(var j in tempRecord){
                    if(tempRecord[j].toLowerCase().indexOf(searchKey.toLowerCase()) > -1) {
                        if(!searchList.includes(AllRecordList[i])) {
                            searchList.push(AllRecordList[i]);
                        }
                    }
                }
            }
        } 
        component.set("v.filteredData", searchList);
    },
    
    goToRecordDetails : function(component,event) {
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:displayAccountDetail",
            componentAttributes: {
                // accountDetail : component.get("v.passtoChild")
            }
        });
        evt.fire();
    }
})