({
    doInit : function(component, event, helper) {
        
        component.set("v.showSpinner", true);
        
        /* Set the column names and fields for the Datatable */
        component.set("v.mycolumns", [
            {label: "Workflow Name", fieldName: "Name", type: "url", 
             typeAttributes: {label: { fieldName: "Name" }}, sortable: true},
            {label: "Description", fieldName: "Description__c", type: "text area", sortable: true},
            {label: "Application Key", fieldName: "Application_Key__c", type: "text", sortable: true},
            {label: "Location", fieldName: "Location_Info__c", type: "text", sortable: true},
            {label: "Category", fieldName: "Category__c", type: "text", sortable: true}
        ]);
        
        var action = component.get("c.getWorkflowRecords");
        action.setCallback(this, function(response){
            component.set("v.showSpinner", false);
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.AllRecordList",response.getReturnValue());
                component.set("v.filteredData",response.getReturnValue());
                helper.sortData(component, component.get("v.sortedBy"), component.get("v.sortedDirection"));
            }
        });
        $A.enqueueAction(action); 
    },
    
    updateColumnSorting: function (component, event, helper) {
        var fieldName = event.getParam("fieldName");
        var sortDirection = event.getParam("sortDirection");
        component.set("v.sortedBy", fieldName);
        component.set("v.sortedDirection", sortDirection);
        helper.sortData(component, fieldName, sortDirection);
    },
    
    handlesearchKeyChange : function(component, event, helper) {
        helper.findByName(component,event); 
    },
    
    handleRowAction : function(component, event, helper) {
        helper.goToRecordDetails(component,event);
    }
})