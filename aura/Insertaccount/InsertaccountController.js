({
	doInit : function(component, event, helper) {
    var action = component.get("c.getAccountType");
    var inputsel = component.find("InputSelectDynamic");
    var opts=[];
    action.setCallback(this, function(a) {
        for(var i=0;i< a.getReturnValue().length;i++){
            opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
        }
        inputsel.set("v.options", opts);

    });
    $A.enqueueAction(action); 
	},	
    
    create : function(component, event, helper) {       
        
        var account = component.get("v.Accounts");     
       
        //Calling the Apex Function
        var action = component.get("c.createAccount");
        
        //Setting the Apex Parameter
        action.setParams({
            acc : account
        });
        
        //Setting the Callback
        action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();
            
            //check if result is successfull
            if(state == "SUCCESS"){
                //Reset Form
                var newAccount = {'sobjectType': 'Account',
                                    'Name': '',
                                    'Phone': '',
                                   	'Type': ''                                    
                                   };
                //resetting the Values in the form
                component.set("v.Accounts",newAccount);
                alert('Record is Created Successfully');
            } else if(state == "ERROR"){
                alert('Error in calling server side action');
            }
        });
        
		//adds the server-side action to the queue        
        $A.enqueueAction(action);

	}   
})