({
    myAction : function(component, event, helper) {
        
    },
    handleMenuSelect :function(component, event, helper){
        
        var id= event.getSource().get("v.value");
        //alert('id'+id);
        var editRecordEvent = $A.get("e.force:editRecord");
        //alert('editRecordEvent '+editRecordEvent);
        editRecordEvent.setParams({
            "recordId": id
        });
        editRecordEvent.fire();
        
    },
    
    handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "CHANGED") {
            alert("changed");
            var c=component.find("recordHandler");
            console.log(c);
            c.reloadRecord();
            
        } else if(eventParams.changeType === "LOADED") {
            alert("loaded");
            
        } else if(eventParams.changeType === "REMOVED") {
            var c=component.find("recordHandler");
            console.log(c);
            console.log(component);
            
            
            var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({
                "title": "Deleted",
                "message": "The record was deleted."
            });
            resultsToast.fire();
            // record is deleted and removed from the cache
        } else if(eventParams.changeType === "ERROR") {
            // there’s an error while loading, saving or deleting the record
        }
    },
    handleDeleteRecord:function(component, event, helper){
        
        alert('delete Record');
        component.find("recordHandler").deleteRecord($A.getCallback(function(deleteResult) {
            // NOTE: If you want a specific behavior(an action or UI behavior) when this action is successful 
            // then handle that in a callback (generic logic when record is changed should be handled in recordUpdated event handler)
            if (deleteResult.state === "SUCCESS" || deleteResult.state === "DRAFT") {
                alert('record deleted');
                var c=component.find("recordHandler");
                console.log(c);
                console.log(component);
                <!--We can directly use component.destroy() method to remove the child component.
                -->
                component.destroy();
                console.log("Record is deleted.");
                
            }
        }));
    }
    
    
})