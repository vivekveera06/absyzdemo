({
    /*
     *  Map the Field to the desired component config, including specific attribute values
     *  Source: https://www.salesforce.com/us/developer/docs/apexcode/index_Left.htm#CSHID=apex_class_Schema_FieldSetMember.htm|StartTopic=Content%2Fapex_class_Schema_FieldSetMember.htm|SkinName=webhelp
     *
     *  Change the componentDef and attributes as needed for other components
     */
   configMap: {
        'anytype': { componentDef: 'lightning:input', attributes: {} },
        'boolean': {componentDef: 'lightning:input', attributes: {} },
        'datacategorygroupreference': { componentDef: 'lightning:input', attributes: {} },
        'id': { componentDef: 'lightning:input', attributes: {} },
        'multipicklist': { componentDef: 'lightning:input', attributes: {} },
        'picklist': { componentDef: 'lightning:input', attributes: {} },
        'reference': { componentDef: 'lightning:input', attributes: {} },
       'string': { componentDef: 'lightning:input', attributes: {} },
        'textarea': { componentDef: 'lightning:input', attributes: {} },
    },

    createForm: function(cmp) {
        console.log("FieldSetFormHelper.createForm");
        var fields = cmp.get('v.fields');
        var record = cmp.get('v.record');
        var inputDesc = [];
        
        for (var i = 0; i < fields.length; i++) {
            var field = fields[i];
            var type = field.Type.toLowerCase();

            var configTemplate = this.configMap[type];

            if (!configTemplate) {
                console.log(`type ${ type } not supported`);
            	continue;
            }
            
            // Copy the config so that subsequent types don't overwrite a shared config for each type.
            var config = JSON.parse(JSON.stringify(configTemplate));
            
            config.attributes.label = field.Label;
            config.attributes.required = field.Required;
            config.attributes.value = cmp.getReference(' v.record.' + field.APIName);
            config.attributes.fieldPath = field.APIName;
            config.attributes.readonly = true;
            
            if (!config.attributes['class']) {
                console.log('insode loh');
            	config.attributes['class'] = 'slds-m-vertical_x-small';
            }

            inputDesc.push([
                config.componentDef,
                config.attributes
            ]);
        }
		console.log("inputDesc"+JSON.stringify(inputDesc));
        $A.createComponents(inputDesc, function(cmps) {
            console.log('createComponents');

            cmp.set('v.body', cmps);
        });
    }
})