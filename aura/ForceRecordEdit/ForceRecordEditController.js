({
	edit : function(component, event, helper) {
		var id = component.get("v.recordId");
        component.set("v.currentId",id);
	},
    save : function(component, event, helper) {
        component.find("save").get("e.recordSave").fire();
        var id = component.get("v.recordId");
    	var urlEvent = $A.get("e.force:navigateToURL");
    	urlEvent.setParams({
      	"url":"/"+ id 
   		});
    	urlEvent.fire();
    }
})