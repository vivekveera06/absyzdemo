({
    doInit : function(component, event, helper) {
        var action=component.get("c.getAcc");
        
        action.setCallback(this,function(a){
            
            var state=a.getState();
            if(state==='SUCCESS'){
                
                var uniqueItems=_.uniqBy(a.getReturnValue(), function(x){
                    return x.Name;
                },['asc']); 
                component.set("v.acc", uniqueItems);
            }
            else if (a.getState() === "ERROR"){
                 $A.log("Errors", a.getError())
                
            }
        });
        $A.enqueueAction(action);
    }
})