({
	doInit : function(component, event, helper) {
		
        var action=component.get("c.getRecords");
        action.setCallBack(this,function(result){
            
            component.set("v.accounts",result.getReturnValue());
            
        });
        $A.enqueueAction(action);
	}
    
})