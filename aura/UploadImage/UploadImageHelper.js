({
	upload : function(component, file,recordid, base64Data, callback) {
        alert('inside helper');
		var action=component.get("c.uploadFile");
        action.setParams({
            fileName:file.name,
            base64Data:base64Data,
            contentType:file.type,
            cid:recordid
         });
        action.setCallback(this,function(a){
           
            var state=a.getState();
            if(state=='SUCCESS'){
                callback(a.getReturnValue());
            }
			
	            
        });
        $A.enqueueAction(action);
	},
    show:function(component,event){
        
        var spinner=component.find("mySpinner");
        $A.util.removeClass(spinner,"slds-hide");
        $A.util.addClass(spinner,"slds-show");
        
    },
    hide:function(component,event){
        
       	var spinner=component.find("mySpinner");
        $A.util.removeClass(spinner,"slds-show");
        $A.util.addClass(spinner,"slds-hide"); 
    }
})