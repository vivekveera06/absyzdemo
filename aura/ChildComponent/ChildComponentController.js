({
	doInit : function(component, event, helper) {
		
        var action=component.get("c.getRecords");
        action.setParams({
            "parentid":component.get("v.recordId")
        });
        action.setCallback(this,function(result){
            var state=result.getState();
            if(state==	'SUCCESS'){
                component.set("v.Child",result.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	}
})