({
    
    getId :function(component){
        var idd=component.get("v.recordId");
        component.set("v.currentid",idd);
    },
	edit : function(component, event, helper) {
        
       component.find("edit").get("e.recordSave").fire();
	},
    click:function(component,helper,event)
    {
     component.set("v.flag",true); 
    },
	save : function(component, event, helper) {
		component.find("edit").get("e.recordSave").fire();
	}
})