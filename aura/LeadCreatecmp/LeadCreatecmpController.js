({
	createlead : function(component, event, helper) {
        var newld = component.get("v.newlead");
		var action = component.get("c.savelead");
        action.setParams({ 
        "ld": newld
    	});
        action.setCallback(this, function(a) {
           var state = a.getState();
        alert("hello from here"+state);
            if (state === "SUCCESS") {
                var name = a.getReturnValue();
                
                var cmpEvent = component.getEvent("cmpEvent");
                cmpEvent.setParams({
                    "newlead":name
                });
                cmpEvent.fire();
                component.set("v.newlead",{ 'sobjectType': 'Lead',
                                            'FirstName':'',
                                            'LastName':'',
                                            'Email':'',
                                            'Phone':'',
                                            'Company':''});
            }
        });
    $A.enqueueAction(action);
	}
})