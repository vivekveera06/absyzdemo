({
	showRecord : function(component, event) {
		 var selectedRecord = component.get("v.customSettingRecord");
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:DynamicParent",
            componentAttributes: {
                SingleWorkflow: selectedRecord
            }
        });
        evt.fire();
	}
})