({
	doInit : function(component, event, helper) {
		var action = component.get("c.getOpp");
        action.setCallback(this, function(a){
            var state = a.getState();
            alert(state);
            if(state == "SUCCESS"){
                component.set("v.opportunities",a.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    handleComponentEvent : function(component, event, helper) {
        var oppname = event.getParam("message");
		component.set("v.OppName","you have selected '"+ oppname+"'");
    }
})