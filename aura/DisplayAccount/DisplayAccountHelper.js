({
	fetchDetails : function(component,event) {
        var action=component.get("c.getCustomSettinglist");
        //component.set("v.showSpinner", true);
        action.setStorable();
        action.setCallback(this,function(response){
            component.set("v.showSpinner", false);
            console.log('response.getReturnValue() '+response.getReturnValue());
            if(response!=null && response.getState()=='SUCCESS'){
                console.log('response.getReturnValue() '+response.getReturnValue());
                component.set("v.customSettinglist",response.getReturnValue());
            }
            else{
                component.set("v.isAlert",true);
            }
        });
        $A.enqueueAction(action);
		
	}
})