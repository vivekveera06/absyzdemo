({
	myAction : function(component, event) {
		
        var server=component.get("v.save");
        var action=component.get("c.newAccount");
        
        action.setParams({          
		"acc": server            
        });

        action.setCallback(this, function(a) {
			 var state = a.getState();
            if (state === "SUCCESS") {               
                var name=a.getReturnValue();
                
                alert("Account created"+name);
               
            }
        });
        
        $A.enqueueAction(action);
        
	}
      
})