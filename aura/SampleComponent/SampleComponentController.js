({
	fireComponentEvent : function(cmp, event) {
        //get the componet event by using aura:registerEvent
        var cmpevent=cmp.getEvent("cmpEvent");
        cmpevent.setParams({
            
            "message":"fired component event"
            
        });
        cmpevent.fire();
	},
    handleComponentEvent:function(cmp,event){
        var message=event.getParam("message");
        //set the handle attribute based on event data
        cmp.set("v.messageFromEvent",message);
        var numEventHandled=parseInt(cmp.get("v.numevent"))+1;
        cmp.set("v.numevent",numEventHandled);
        
        
    }
})