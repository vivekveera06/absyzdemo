({
           doinit : function(component, event, helper) {
		var action=component.get("c.getaccounts");
        action.setCallback(this,function(response){
            var state=response.getState();
            alert('state'+state);
            if(state=="SUCCESS"){
            component.set("v.Accounts1",response.getReturnValue());
                alert(response.getReturnValue());
            }
        });
        $A.enqueueAction(action);        
    },
	deleteacc : function(component, event, helper) {
		 var accdel=event.getParam("AccountId");
        var action=component.get("c.deleteaccount");
        action.setParams({
            "acc":accdel
        });
        action.setCallback(this,function(res){
            var state=res.getState();
            if(state=="SUCCESS"){
                alert("Account deleted");
            }
            else 
                alert("ERROR");
        });
        $A.enqueueAction(action);
	}
})