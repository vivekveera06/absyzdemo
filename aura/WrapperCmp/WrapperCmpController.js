({
    doInit: function (component, event, helper) {
        var action = component.get("c.getEmployeeContactDetails");
       // action.setStorable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var values = response.getReturnValue();
                component.set("v.employeeInfo",values.employee);
                console.log(JSON.stringify(component.get("v.employeeInfo")));
                component.set("v.contactInfo",values.contact); 
            }
        });
        $A.enqueueAction(action);
    },
    showEmployee : function (component, event) {
        
        component.set("v.con",false);
        component.set("v.emp",true);
        
    },
    showContact : function (component, event) {
        component.set("v.emp",false);
        component.set("v.con",true);
        
       /* var auraid = component.find("show-contact");
        $A.util.removeClass(auraid, 'slds-hide');
        $A.util.addClass(auraid, 'slds-show');
        var auraidemp = component.find("show-employee");
        $A.util.removeClass(auraidemp, 'slds-show');
        $A.util.addClass(auraidemp, 'slds-hide'); */
        
    }
})