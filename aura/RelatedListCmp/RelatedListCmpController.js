({
    doInit : function(component, event, helper) {
        
        //alert('inside doInit');
        var action=component.get("c.getOpp");
        //alert('record id'+component.get("v.recordId"));
        action.setParams({
            
            "aid":component.get("v.recordId")
        });
        
        action.setCallback(this, function(response){
            
            //alert('inside callback');
            var state = response.getState();
            if (state === "SUCCESS"){
                //alert('state'+state);
                component.set("v.opp",response.getReturnValue());
                component.set("v.size",response.getReturnValue().length);
                //alert(response.getReturnValue().length);
            }
            else{
                alert('error');
            }
            
        });
        $A.enqueueAction(action);
    },
    handleNew :function(component, event, helper){
        alert('accoid '+component.get("v.recordId"));
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Opportunity",
            "defaultFieldValues": {
			            "AccountId":component.get("v.recordId")
            }
        });
        createRecordEvent.fire();
        /*var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/"+component.get("v.recordId")
        });
        urlEvent.fire();*/
    }
    
})