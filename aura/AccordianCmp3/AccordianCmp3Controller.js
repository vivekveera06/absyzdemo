({
	CreateAcc : function(component, event, helper) {
		
         var evt = $A.get("e.force:navigateToComponent");

        evt.setParams({
            
            componentDef :"c:AccountCmp2"
                    });
        evt.fire();
	},
    CreateCon :function(component, event, helper){
    	
    	 var evt = $A.get("e.force:navigateToComponent");

        evt.setParams({
            
            componentDef :"c:ContactCmp3"
                    });
        evt.fire();
    
    
	},
    doInit: function(component, event, helper){
        alert('inside doinit');
        var action=component.get("c.getFiveRecords");
        
        action.setCallback(this,function(a){
            alert('inside callback');
            var state=a.getState();
            
            if(state==='SUCCESS'){
                component.set("v.accounts",a.getReturnValue());
                alert(JSON.stringify(a.getReturnValue()));
				                
            }
            
        });
        $A.enqueueAction(action);
    }
})