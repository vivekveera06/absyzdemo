({
	doInit: function(component, event, helper){
     var action = component.get("c.getAccountDetails");
     action.setCallback(this, function(actionResult) {
       // var items=actionResult.getReturnValue();
         var uniqueItems =  _.uniqBy(actionResult.getReturnValue(), function(temp) {
         return temp.Name;
         });
     component.set('v.accounts', uniqueItems);
    });
    $A.enqueueAction(action);
    }
})