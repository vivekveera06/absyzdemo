({
	myAction : function(component, event, helper) {
		
        var action=component.get("v.one");
        var act=component.get("v.two");
        var search=component.get("c.method");
        search.setParams({
            "n":action,
            "e":act
            
        });
	
        search.setCallback(this,function(a){
            component.set("v.contact",a.getReturnValue());
            
        });
        $A.enqueueAction(search);
    }
    
})