({
	Next : function(component, event, helper) {
		
        var cmpevt=component.getEvent("contactevt");
        alert('next function in contact')
        cmpevt.setParams({
            'condata':component.get("v.con")
        });
        alert('before fire');
        cmpevt.fire();
        alert('after fire');
	},
    Prev: function(component, event, helper){
         
        var precon=component.getEvent("prev");
        alert('previous for contact');
        
        precon.setParams({
            'condata':component.get("v.con"),
            "prevdata":1
        });
        alert('before fire of prev');
        precon.fire();
        alert('after fire of prev');
        
        
    }
})