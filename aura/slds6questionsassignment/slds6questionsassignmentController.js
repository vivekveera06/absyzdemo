({
	getRelated : function(component, event, helper) {
		
        var conaction=component.set("c.getCon");
        conaction.setParams({
            
            idcon:component.get("recordId");
        });	
        conaction.setCallBack(this,function(c){
            if(c.getState()==="SUCCESS")
                component.set("v.contacts",c.getReturnValue());
            else if(c.getState()==="ERROR")
                $A.log("Errors",c.getError());
            
        });
        var oppaction=component.set("c.getOpp");
        oppaction.setParams({
            idopp:component.get("recordId");
        });
        oppaction.setCallBack(this,function(p){  
            if(p.getState()==="SUCCESS")
                component.set("v.opportunities",p.getReturnValue());
            else if(c.getState()==="ERROR")
                $A.log("Errors",p.getError());
        });
         $A.enqueueAction(conaction);
        //	$A.enqueueAction(oppaction);   
	},
    getContacts: function(component, event, helper){
        
        conEvt.setParams({
            "recordId":component.get("v.contacts");
            "slideDevName": "related"
            
        });
        conEvt.fire();
    },
    getopportunity:function(component, event, helper){
        oppEvt.setParams({
            "recordId":component.get("v.opportunities");
            "slideDevName": "related"
        });
        oppEvt.fire();    
    }
    
})