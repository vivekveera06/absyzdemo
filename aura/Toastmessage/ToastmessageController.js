({
	createAccount : function(component, event, helper) {
        alert("type "+component.get("v.acc.Type"));
		var action= component.get("c.createAcc");
        action.setParams({
            "name":component.get("v.acc.Name"),
            "state":component.get("v.acc.BillingState"),
            "city":component.get("v.acc.BillingCity"),
            "type": component.get("v.acc.Type")
        });
        action.setCallback(this,function(response){
            if(response.getState() == "SUCCESS"){
                var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
        "title": "Success!",
        "type": "success",
        "message": "Account was created."
    });
    toastEvent.fire();
            }
            else{
                var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
        "title": "Failed!!",
        "type": "error",
        "message": "Account not created."
    });
    toastEvent.fire();
            }
        }); 
        $A.enqueueAction(action);
        
	},
    doinit: function(component, event, helper){
        var optionsList = [];
        var action= component.get("c.accTypePicklist");
        action.setCallback(this, function(response){
            if(response.getState() == "SUCCESS"){
               alert("doinit call back"+response.getReturnValue());
               var allvalues= response.getReturnValue();
                /*if(allvalues!=undefined){
                    optionsList.push({
                        class:"optionClass",
                        label:"--none--",
                        value:""
                    });
                }*/
               component.set("v.accTypes",allvalues);
            }
        });
        $A.enqueueAction(action);
        
    }
})