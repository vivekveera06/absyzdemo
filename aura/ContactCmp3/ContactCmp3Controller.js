({
    insert : function(component, event, helper) {
        
        var action=component.get("c.InsertCon");
        action.setParams({
            "cc":component.get("v.con"),
            'aid':component.get("v.accid")
            
        });
        action.setCallback(this,function(a){
            
            var state=a.getState();
            if(state==='SUCCESS'){
                
                alert(state);
                var evt = $A.get("e.force:navigateToComponent");
                
                evt.setParams({
                    
                    componentDef :'c:AccordianCmp3'
                });
                evt.fire();
                
            }
            else if(state==='ERROR'){
                
               alert('error'+state); 
            }
        });
        $A.enqueueAction(action);
    },
     options:function(component, event, helper){
        var evt = $A.get("e.force:navigateToComponent");
                
                evt.setParams({
                    
                    componentDef :'c:AccordianCmp3'
                });
                evt.fire();
    },
    handleComponentEvent:function(component, event, helper){
        
        var message = event.getParam("lookup");
		component.set("v.accid",message);
        alert('account selected'+message);
    }
})