({
    handleUploadFinished: function (component, event) {
        // Get the list of uploaded files
        var uploadedFiles = event.getParam("files");
        // show success message – with no of files uploaded
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "type" : "success",
            "message": uploadedFiles.length+"files has been updated successfully!"
        });
        toastEvent.fire();
        
       
        /*var cmpTarget1 = component.find('changeIt');
        $A.util.addClass(cmpTarget1, 'change');
        var action = component.get("c.getAttachment");
        action.setParams({
            ContactId: component.get("v.recordId")
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS") {
                console.log('return '+JSON.stringify(a.getReturnValue()));
                component.set("v.attachmentList",a.getReturnValue());
            }
        });*/
        
        var action = component.get("c.ImageDelete");
        action.setParams({
            ContactId: component.get("v.recordId")
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS") {
                var appevt=$A.get("e.c:ImageEvent");
       			appevt.fire();
            }
        });
        
        $A.enqueueAction(action);
    },
    
    doInit : function (component, event) {
        var action = component.get("c.getAttachment");
        action.setParams({
            ContactId: component.get("v.recordId")
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS") {
                console.log('return '+JSON.stringify(a.getReturnValue()));
                component.set("v.attachmentList",a.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    /*closeModal: function(component, event, helper) {
        var cmpTarget1 = component.find('changeIt');
        $A.util.addClass(cmpTarget1, 'change');
    },
    
    handleClick : function(component, event, helper) {
        var cmpTarget1 = component.find('changeIt');
        $A.util.removeClass(cmpTarget1, 'change');
    },*/
    
    FileDeleteEvent : function(component, event, helper) {
        console.log('event handler');
        if(event.getParam("Deleted") == "true")
        {
            console.log('Deleted  == true');
            var action = component.get("c.getAttachment");
            action.setParams({
                ContactId: component.get("v.recordId")
            });
            action.setCallback(this, function(a) {
                var state = a.getState();
                if (state === "SUCCESS") {
                    component.set("v.attachmentList",a.getReturnValue());
                    // alert('return1 '+JSON.stringify(component.get("v.attachmentList")));
                }
            });
            $A.enqueueAction(action);
        }
    },    
})