({
    doInit : function(component, event, helper) {
        var cmpTarget1 = component.find('changeIt');
        $A.util.addClass(cmpTarget1, 'change');
    },
    
    DeleteAttachment : function(component, event, helper) {
        var action = component.get("c.AttachmentDelete");
        action.setParams({
            AttachmentId: component.get("v.oAttachment.Id")
        });
        action.setCallback(this,function(a){
            var state = a.getState();
            if (state === "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Attachment successfully deleted."
                });
                toastEvent.fire();
            }
            console.log('event fired');
            var event = component.getEvent("FileDeleteEvent");
            event.setParams({
                "Deleted" : "true"
            });
            event.fire();
        });
        $A.enqueueAction(action);
    },
    
    handleClick : function(component, event, helper) {
        var cmpTarget1 = component.find('changeIt');
        $A.util.removeClass(cmpTarget1, 'change');
    },
    closeModal: function(component, event, helper) {
        var cmpTarget1 = component.find('changeIt');
        $A.util.addClass(cmpTarget1, 'change');
    },
    
    updateAttachment: function(component, event, helper) {
        var action = component.get("c.AttachmentEdit");
        action.setParams({
            AttachmentId: component.get("v.oAttachment.Id"),
            fileTitle: component.get("v.oAttachment.Title"),
            fileDiscription: component.get("v.oAttachment.Description")
        });
        action.setCallback(this,function(a){
            var state = a.getState();
            if (state === "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Attachment successfully Update."
                });
                toastEvent.fire();
                var cmpTarget1 = component.find('changeIt');
                $A.util.addClass(cmpTarget1, 'change');
            }
            console.log('event fired');
            var event = component.getEvent("FileDeleteEvent");
            event.setParams({
                "Deleted" : "true"
            });
            event.fire();
        });
        $A.enqueueAction(action);
    }
    
})