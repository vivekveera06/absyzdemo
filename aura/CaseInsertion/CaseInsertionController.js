({
    doInit : function(component, event, helper) {
        
        /*
         this is not required if you are using force:inputfield  
        var action=component.get("c.getCaseSubject");
        var inputsel = component.find("subject");
        var opts=[];
        action.setCallback(this,function(a){
            
            for(var i=0;i< a.getReturnValue().length;i++){
                opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
            }
            inputsel.set("v.options", opts);
            
        });
        $A.enqueueAction(action);
        */
        
    },
    handleClick :function(component, event, helper){
        
        var action=component.get("c.checkCase");        
        var ContactPhone1=component.find("casesContactPhone").get("v.value");
        var Subject1=component.find("Subject").get("v.value");
        var Status1=component.find("Status").get("v.value");
        var Origin1=component.find("Origin").get("v.value");
        var Type1=component.find("Type").get("v.value");
        if(ContactPhone1=='' || ContactPhone1==null){
            
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type":"error",
                "title": "warning!",
                "message": "Please fill Phone."
            });
            toastEvent.fire();
            
        }
        else if(Subject1=='' || Subject1==null){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type":"error",
                "title": "warning!",
                "message": "Please fill Subject ."
            });
            toastEvent.fire();
        }
            else if(Status1==''|| Status1==null){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type":"error",
                    "title": "warning!",
                    "message": "Please fill Status."
                });
                toastEvent.fire();     
            }
                else if(Origin1==''|| Origin1==null){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type":"error",
                        "title": "warning!",
                        "message": "Please fill Origin."
                    });
                    toastEvent.fire();    
                }
                    else if(Type1==''|| Type1==null){
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type":"error",
                            "title": "warning!",
                            "message": "Please fill Type."
                        });
                        toastEvent.fire();   
                    }
                        else {
                            action.setParams({ 
                                'c':component.get("v.cases")
                            });
                            
                            action.setCallback(this, function(response){
                                
                                alert('status of state'+response.getState());
                                var result=response.getReturnValue();
                                alert(JSON.stringify(result));
                                if(result==null){
                                    
                                    var action1=component.get("c.insertCase");
                                    action1.setParams({ 
                                        'c':component.get("v.cases")
                                    });
                                    action1.setCallback(this, function(response1){
                                        
                                        var state1=response1.getState();
                                        if(state1 === 'SUCCESS'){
                                            alert('inserted');
                                            var toastEvent = $A.get("e.force:showToast");
                                            toastEvent.setParams({
                                                "type":"success",
                                                "title": "Success!",
                                                "message": "Please fill Origin."
                                            });
                                            toastEvent.fire();
                                            var toggleText=component.find("fm");
                                            $A.util.toggleClass(toggleText,"toggle");
                                            component.set("v.cases",{'sobjectType':'Case',
                                                                     'Phone__c':'',
                                                                     'Subject':'',
                                                                     'Type':'',
                                                                     'Status':'',
                                                                     'Origin':''});
                                        }
                                    });
                                    $A.enqueueAction(action1);
                                    
                                }
                                else {
                                    var phoneid=component.find("casesContactPhone");
                                    phoneid.set("v.errors",[{message:"Phone number already exists"}]);
                                    
                                    //var result1=response.getReturnValue();
                                    
                                    component.set("v.show",true);
                                    component.set("v.caselist",result);
                                    
                                }
                                
                            });
                            $A.enqueueAction(action);
                        } 
    },
    handleCancle:function(component, event, helper){
        component.set("v.cases",{'sobjectType':'Case',
                                 'Phone__c': '',
                                 'Subject':'',
                                 'Type': '',
                                 'Status':'',
                                 'Origin':''});
        
    },
    showModal:function(component, event, helper){
        
        $A.util.removeClass(component.find('popUpId'), 'hideContent');
        $A.util.removeClass(component.find('popUpBackgroundId'), 'hideContent');
        
    },
    hidePopup : function(component){
        $A.util.addClass(component.find('popUpId'), 'hideContent');
        $A.util.addClass(component.find('popUpBackgroundId'), 'hideContent');
    }
    
})