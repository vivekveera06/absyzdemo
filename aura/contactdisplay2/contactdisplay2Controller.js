({
	myAction : function(component, event, helper) {
		var action=component.get("c.getContact");
        
        action.setCallback(this,function(data){
            
            component.set("v.contact",data.getReturnValue());
        
                          });
        		$A.enqueueAction(action);
        
	}
})