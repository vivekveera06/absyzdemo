({
	sendHelper: function(component, getEmail, getEmailbcc, getEmailcc, getSubject, getbody, getAttachment) {
        
        //alert(JSON.stringify(getmail));
        
        // call the server side controller method 	
        var action = component.get("c.sendMailMethod");
        
        // set the 3 params to sendMailMethod method 
        //alert('helper'+getEmail);
        
        action.setParams({
            'mMail': getEmail,
            'mMailcc': getEmailcc,
            'mMailbcc': getEmailbcc,
            'mSubject': getSubject,
            'mbody': getbody,
            'mAttachment':getAttachment
        });
       
        //alert("attachments"+getAttachment);
        action.setCallback(this, function(response) {
            //alert("inside call back");
            var state = response.getState();
            
            alert(state);
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                // if state of server response is comes "SUCCESS",
                // display the success message box by setting mailStatus attribute to true
                component.set("v.mailStatus", true);
                //alert(component.get("v.mailStatus"));
            }
            else{
                alert('failure');
            }
 
        });
        $A.enqueueAction(action);
    },
    checkMailFields: function(component) {
        // when user clicks on Send button 
        // First we get the email id list from all 3 fields 
        
        var toEmail=component.get("v.toEmailList");
        //alert('toEmail '+JSON.stringify(toEmail));
        var t=[];
        var nullEmails=[];
        for(var i=0;i<toEmail.length;i++){
            if(toEmail[i].subtitle == ""){
                nullEmails.push(toEmail[i].title);
            }
            else{
                t.push(toEmail[i].subtitle);
                var tString=t.join();
                component.set("v.email",tString); 
            }
        }
        
        var ccEmail=component.get("v.ccEmailList");
        //alert('ccEmail '+JSON.stringify(ccEmail));
        var c=[];
        
        for(var i=0;i<ccEmail.length;i++){
            if(ccEmail[i].subtitle == ""){
                nullEmails.push(ccEmail[i].title);
            }
            else{
                c.push(ccEmail[i].subtitle);
                var cString=c.join();
                component.set("v.emailcc",cString); 
            };
        }
        
        var bccEmail=component.get("v.bccEmailList");
        //alert('bccEmail '+JSON.stringify(bccEmail));
        var b=[];
        
        for(var i=0;i<bccEmail.length;i++){
            if(bccEmail[i].subtitle == ""){
                nullEmails.push(bccEmail[i].title);
            }
            else{
                b.push(bccEmail[i].subtitle);
                var bString=b.join();
                component.set("v.emailbcc",bString);
            };
        }
        
        //START--pass the parameters to the helper
        var getEmail = component.get("v.email");
        //alert('getEmail content:'+JSON.stringify(getEmail));
        //var getEmail=getEmail1.split("");
        //alert(JSON.stringify(getEmail));
        var getEmailbcc = component.get("v.emailbcc");
        var getEmailcc = component.get("v.emailcc");
        var getSubject = component.get("v.subject");
        var getbody = component.get("v.body");
        var getAttachment = component.get("v.contentVerIdList");
        
        if(toEmail.length == 0 && ccEmail.length == 0 && bccEmail.length == 0){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Email not sent !",
                "type":	"error",
                "message": "Please specify atleast one recipient !!"
            });
            toastEvent.fire();
            component.set("v.checkError",true); 
        }
        else if(nullEmails.length > 0){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Email not sent !",
                "type":	"error",
                "message": "One or more of the selected recipients do not have an email id associated with them !!"
            });
            toastEvent.fire();
            component.set("v.checkError",true);
        }
        else{
            component.set("v.checkError",false);
        }
        if((getbody == null || getSubject == null) && (nullEmails.length == 0) && (toEmail.length > 0 || ccEmail.length > 0 || bccEmail.length > 0)){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Warning!",
                "type":	"info",
                "message": "Either the Subject or Body is blank !! You might consider resending the email with a subject and body."
            });
            toastEvent.fire();
            //component.set("v.checkError",true);
            
        }
        if(component.get("v.checkError") == false){
            this.sendHelper(component, getEmail, getEmailbcc, getEmailcc, getSubject, getbody, getAttachment);
        }
        //END--
        
    },
    RemovePillToHelper : function(component,to) {
        
        var toList = component.get("v.toEmailList");
        for(var i=0;i<toList.length;i++){
            if(toList[i].value.includes(to.value))
            {
                //alert('true matching the id');
                toList.splice(i, 1);
            }
        }
        component.set("v.toEmailList",toList);
    },
    RemovePillCcHelper : function(component,cc) {
        
        var ccList = component.get("v.ccEmailList");
        for(var i=0;i<ccList.length;i++){
            if(ccList[i].value.includes(cc.value))
            {
                //alert('true matching the id');
                ccList.splice(i, 1);
            }
        }
        component.set("v.ccEmailList",ccList);
    },
    RemovePillBccHelper : function(component,bcc) {
        
        var bccList = component.get("v.bccEmailList");
        for(var i=0;i<bccList.length;i++){
            if(bccList[i].value.includes(bcc.value))
            {
                //alert('true matching the id');
                bccList.splice(i, 1);
            }
        }
        component.set("v.bccEmailList",bccList);
    },
})