({
    NavigateForTo : function(component, event, helper) {
        
        var to= event.getParam("navigateTo");
        //alert('id list'+JSON.stringify(to));
        component.set("v.toEmailList",to);
    },
    NavigateForCc : function(component, event, helper) {
        
        var cc= event.getParam("navigateCc");
        //alert('id list'+JSON.stringify(cc));
        component.set("v.ccEmailList",cc);
    },
    NavigateForBcc : function(component, event, helper) {
        
        var bcc= event.getParam("navigateBcc");
        //alert('id list'+JSON.stringify(bcc));
        component.set("v.bccEmailList",bcc);
    },
    sendMail: function(component, event, helper) {
        
        helper.checkMailFields(component);
    },
    hide : function(component, event, helper) {
        var x = component.find("check");
        //$A.util.removeClass(x, 'slds-show');
        //$A.util.toggleClass(x,'slds-hide');
        
        $A.util.removeClass(component.find('modaldialog'),'slds-fade-in-open');
        $A.util.addClass(component.find('modaldialog'),'slds-fade-in-hide');
        
        $A.util.removeClass(component.find('backdrop'),'slds-backdrop--open');
        $A.util.addClass(component.find('backdrop'),'slds-backdrop--hide');
    },
    check : function(component, event, helper) {
        component.set("v.checkError",false);
    },
    // when user click on the close buttton on message popup ,
    // hide the Message box by set the mailStatus attribute to false
    // and clear all values of input fields.   
    closeMessage: function(component, event, helper) {
        
        component.set("v.mailStatus", false);
        $A.get('e.force:refreshView').fire();
        
    },
    handleToSelect : function (component, event, helper) {
        //var selectedToValue = component.find("toSelection").get("v.value");
        var selectedToValue = event.getParam("value");
        component.set("v.toObject",selectedToValue);
        alert(selectedToValue+' Chosen');
    },
    handleCcSelect : function (component, event, helper) {
        //var selectedCcValue = component.find("ccSelection").get("v.value");
        var selectedCcValue = event.getParam("value");
        component.set("v.ccObject",selectedCcValue);
        alert(selectedCcValue+'Chosen');
    },
    handleBccSelect : function (component, event, helper) {
        //var selectedBccValue = component.find("bccSelection").get("v.value");
        var selectedBccValue = event.getParam("value");
        component.set("v.bccObject",selectedBccValue);
        alert(selectedBccValue+'Chosen');
    },
    RemovePillTo : function(component, event, helper) {
        var to= event.getParam("idTo");
        helper.RemovePillToHelper(component,to);
    },
    RemovePillCc : function(component, event, helper) {
        var cc= event.getParam("idCc");
        helper.RemovePillCcHelper(component,cc);
    },
    RemovePillBcc : function(component, event, helper) {
        var bcc= event.getParam("idBcc");
        helper.RemovePillBccHelper(component,bcc);
    },
    cancel : function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
    handleUploadFinished : function(component, event, helper) {
        
        // Get the list of uploaded files
        var uploadedFiles = event.getParam("files");
        var contentIds = [];
        for(var i=0;i<uploadedFiles.length;i++){
            contentIds.push(uploadedFiles[i].documentId);
        }
        
        //component.set("v.contentDocIdList",contentIds);
        
        alert('contentIds'+contentIds);
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "File "+uploadedFiles[0].name+" Uploaded successfully."
        });
        toastEvent.fire();
        
        var action = component.get("c.getContentVersion");
        
        action.setParams({
            'documentList': contentIds
        });
       
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            alert(state);
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                component.set("v.contentVerIdList", storeResponse);
            }
 
        });
        $A.enqueueAction(action);
        
    }
    
})