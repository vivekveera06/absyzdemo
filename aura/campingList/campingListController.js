({
    doInit :function(component, event, helper) {
        
        var action=component.get("c.getItems");
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                /*var expenses = component.get("v.expenses");
                expenses.push(response.getReturnValue());
                component.set("v.expenses", expenses);*/
                component.set("v.items", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
        
    },
    clickCreateItem  : function(component, event, helper) {
		
        var validExpense =component.find('expenseform').reduce(function (validSoFar, inputCmp){
            // Displays error messages for invalid fields
            inputCmp.showHelpMessageIfInvalid();
             return validSoFar && inputCmp.get('v.validity').valid;
        },true);
        if(validExpense){
            helper.createItem(component,event);

        
        }
	},
    handleAddItem: function(component, event, helper){
        
        var message = event.getParam("item");
        var action=component.get("c.saveItem");
        action.setParams({
            "c":message
            
        });
        action.setCallback(this,function(a){
            var state=a.getState();
            if(state==="SUCCESS"){
                var array = component.get("v.items");
                array.push(response.getReturnValue());
               component.set("v.items", array);
                  
            }
        });
        $A.enqueueAction(action);
    }
})