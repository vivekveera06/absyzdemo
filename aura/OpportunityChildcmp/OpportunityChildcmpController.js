({
	Button : function(component, event, helper) {
        var oppname = component.getEvent("oppEvent");
        var name = event.getSource().get("v.value");
        oppname.setParams({
            "message":name
        });
        oppname.fire();
	}
})