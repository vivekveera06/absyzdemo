({
    doInit : function(component, event, helper) {
        helper.displayRecord(component, event);
        
    },
    
    editMode: function(component, event, helper) {
        console.log('editMode-----');
        component.set("v.isEditMode",true);
        //  component.set("v.changedFields",oldValue);
        console.log(component.get("v.isEditMode"));
    },
    
    editModeParent: function(component, event, helper) {
        var params = event.getParam('arguments');
        if (params) {
            var title = params.lookupValueParent;
            console.log('title---',title);
        }
    },
    handleSave : function(component,event,helper) {
        helper.UpdateRecord(component, event);
    },
    
    handleGenericChildEvent : function(component, event) {
        var oldValue = component.get("v.changedFields");
        //var oldValue ;
        oldValue.push(event.getParam("changedValues"));
        component.set("v.changedFields",oldValue);
    },
    handleCustomLookupValue : function(component, event, helper) {
        console.log('in 1 parent change---');
        var oldValues = component.get("v.changedFields");
        oldValues.push(event.getParam("selectedRecord"));
        component.set("v.changedFields",oldValues);
        var params=event.getParam("selectedRecord");
        console.log('in 1 parent change---', params);
        console.log('get changed values'+component.get("v.changedFields"));
    },
    showSuccessToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Success Message',
            message: 'Mode is pester ,duration is 5sec and this is normal Message',
            messageTemplate: 'Record {0} created! See it {1}!',
            duration:' 5000',
            key: 'info_alt',
            type: 'success',
            mode: 'pester'
        });
        toastEvent.fire();
    },
})