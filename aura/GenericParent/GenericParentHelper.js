({
    
    callToServer :function(component,method,callback,params,storable){
        
        component.set("v.showSpinner", true);
        var action=component.get(method);
        if(storable){
          //  action.setStorable();
        }
        if(params){
            action.setParams(params);
        }
        console.log('after action----');
        action.setCallback(this,function(responsethis){
            component.set("v.showSpinner", false);
            console.log('In actiom.setCallback()-----'+responsethis.getState());
            var state = responsethis.getState();
            if (state === "SUCCESS") {
                console.log('inside success---'+responsethis.getReturnValue());
                callback.call(this, responsethis.getReturnValue());
            } 
            else if(state === "ERROR"){
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.alertMessage", "Error message: " + errors[0].message);
                        component.set("v.isAlert", true);
                    }
                }
                
            } else {
                component.set("v.alertMessage", "ERROR: Unknown Error");
                component.set("v.isAlert", true);
            }
        });
        $A.enqueueAction(action);
        
    },
    displayRecord : function(component, event) {
        var fieldList = [];
        fieldList.push("Name__c");
        fieldList.push("Category__c");
        fieldList.push("IsChinaUser__c");
        fieldList.push("Predefined_Approver_01__c");
        var empty = [];
        component.set("v.changedFields",empty);
        var self = this;
        self.callToServer(
            component,
            "c.getGenericFields",    /* server method */
            function(response) {
                
                component.set("v.objectName", response[0].objectName);
                component.set("v.fields",response);
                console.log('setting child***'+JSON.stringify(response));
                component.set("v.displayRecords", true);
            },
            {
                'fieldList' : fieldList,
                "SingleWorkflowrecord":component.get("v.SingleWorkflow")
            },
            true
        );
    },
    UpdateRecord :function(component, event){
        
        var fields = component.get("v.changedFields");
        console.log('fields--'+fields);
        component.set("v.isEditMode",false);
        var stringifiedFields=JSON.stringify(fields);
        console.log('stringifiedFields --'+stringifiedFields);
        var self=this;
        self.callToServer(
            component,
            "c.saveRecord",
            function(response){
                console.log('inside updateRecord Response --'+response);
            },
            {
                'recordId':component.get("v.SingleWorkflow").Id,
                'fields':stringifiedFields
            },
            true
            
        );
        
    }
})