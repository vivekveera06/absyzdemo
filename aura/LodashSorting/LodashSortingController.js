({
    doInit : function(component)
    {
        var action = component.get("c.getAccountItems");
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                var uniqueItems = _.orderBy(a.getReturnValue(), function(x){
                    return x.Name;
                    },['asc']);
                component.set("v.conItems", uniqueItems);
                }
            else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
                }
            });
        $A.enqueueAction(action);
        },
    
    sortByFirstName : function(component) {
        var items = component.get("v.conItems");
        if(component.get("v.sort1") === "down"){
            var uniqueItems = _.orderBy(items, function(x){
                return x.FirstName;
                },['desc']);
            component.set("v.conItems", uniqueItems);
            component.set("v.sort1", "up");
            } else {
                var uniqueItems = _.orderBy(items, function(x){
                    return x.FirstName;
                    },['asc']);
                component.set("v.conItems", uniqueItems);
                component.set("v.sort1", "down");
                }
        },
    
    sortByLastName : function(component) {
        var items = component.get("v.conItems");
        if(component.get("v.sort2") === "down"){
            var uniqueItems = _.orderBy(items, function(x){
                return x.LastName;
                },['desc']);
            component.set("v.conItems", uniqueItems);
            component.set("v.sort2", "up");
            } else {
                var uniqueItems = _.orderBy(items, function(x){
                    return x.LastName;
                    },['asc']);
                component.set("v.conItems", uniqueItems);
                component.set("v.sort2", "down");
                }
        },
    })