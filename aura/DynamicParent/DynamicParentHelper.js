({
    
    callToServer :function(component,method,callback,params,storable){
        
        component.set("v.showSpinner", true);
        var action=component.get(method);
        if(storable){
          //  action.setStorable();
        }
        if(params){
            action.setParams(params);
        }
        console.log('after action----');
        action.setCallback(this,function(responsethis){
            component.set("v.showSpinner", false);
            console.log('In actiom.setCallback()-----'+responsethis.getState());
            var state = responsethis.getState();
            if (state === "SUCCESS") {
                console.log('inside success---'+responsethis.getReturnValue());
                callback.call(this, responsethis.getReturnValue());
               
            } 
            else if(state === "ERROR"){
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.alertMessage", "Error message: " + errors[0].message);
                        component.set("v.isAlert", true);
                    }
                }
                
            } else {
                component.set("v.alertMessage", "ERROR: Unknown Error");
                component.set("v.isAlert", true);
            }
        });
        $A.enqueueAction(action);
        
    },
    displayRecord : function(component, event, helper) {
        console.log('SingleWorkflow'+component.get("v.SingleWorkflow").Name__c);
        var fieldList = [];
        fieldList.push("Name__c");
        fieldList.push("Category__c");
        fieldList.push("IsChinaUser__c");
        fieldList.push("Predefined_Approver_01__c");
        var empty = [];
        component.set("v.changedFields",empty);
        var self = this;
        self.callToServer(
            component,
            "c.getGenericFields",    /* server method */
            function(response) {
                
                component.set("v.objectName", response[0].objectName);
                console.log('response--->'+JSON.stringify(response));
                
                var section1List=[];
                var section2List=[];
                for(var i=0; i<response.length;i++){
                    
                    //console.log('response Inside loop'+JSON.stringify(response[i]));
                    var fieldSeparation = response[i];
                    console.log('0--'+fieldSeparation.section);
                    
                    if(fieldSeparation.section==1) {
                        section1List.push(response[i]);
                    } else if(fieldSeparation.section==2){
                        section2List.push(response[i]);
                    }
                    
                    console.log('section1List --->'+JSON.stringify(section1List));
                    console.log('section2List --->'+JSON.stringify(section2List));
                }
                
                component.set("v.sectionOneFields",section1List);
                component.set("v.SectionTwofields",section2List);
               
              console.log('SectionTwofields***'+JSON.stringify(component.get("v.SectionTwofields")));

              console.log('sectionOneFields***'+JSON.stringify(component.get("v.sectionOneFields")));

               // component.set("v.fields",response);
                console.log('setting child***'+JSON.stringify(response));
                component.set("v.displayRecords", true);
                component.set("v.showEditButton", true);
               	console.log("section2Fields"+component.get("v.SectionTwofields"));
            },
            {
                'fieldList' : fieldList,
                "SingleWorkflowrecord":component.get("v.SingleWorkflow")
            },
            true
        );
    },
    UpdateRecord :function(component, event,helper){
        
        var fields = component.get("v.changedFields");
        console.log('fields--'+fields);
        component.set("v.isEditMode",false);
        var stringifiedFields=JSON.stringify(fields);
        console.log('component.get("v.SingleWorkflow").Id --'+component.get("v.SingleWorkflow").Id);
        console.log('stringifiedFields --'+stringifiedFields);
        var self=this;
        self.callToServer(
            component,
            "c.saveRecord",
            function(response){
                console.log('inside updateRecord Response --'+response);
                component.set("v.isEditMode",false);
                //helper.displayRecord(component, event,helper);
                if(response==='true'){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'sticky',
                        message: 'Record successfully Updated',
                        type : 'success'
                    });
                    toastEvent.fire();
                }
                else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'sticky',
                        message: response,
                        type : 'ERROR'
                    });
                    toastEvent.fire();
                    
                }
                
            },
            {
                'recordId':component.get("v.SingleWorkflow").Id,
                'fields':stringifiedFields
            },
            true
            
        );
        
    }
})