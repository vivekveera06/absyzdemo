({
    doInit : function(component, event, helper) {
        helper.displayRecord(component, event);
        
    },
    editMode: function(component, event, helper) {
        console.log('editMode-----');
        component.set("v.isEditMode",true);
        component.set("v.showSaveButton",true);
        component.set("v.showEditButton",false);
        
       
        console.log(component.get("v.isEditMode"));
    },
    inlineEdit : function(component, event, helper){
        console.log('inside parent');
        component.set("v.isEditMode",true);
        component.set("v.showSaveButton",true);
        component.set("v.showEditButton",false);
        
    },
    Cancel : function(component, event, helper){
         $A.get('e.force:refreshView').fire();
    },
    hideSection1 : function(component, event, helper){
      console.log('inside icon');
       // var targetcomponent=component.find("section1Id");
       // $A.util.toggleClass(targetcomponent, 'slds-show');
    },
    handleSectionHeaderClick : function(component, event, helper) {
       
        var button = event.getSource();
        button.set('v.state', !button.get('v.state'));
		
        var whichButton = event.getSource().getLocalId();
        var sectionContainer ;
        if(whichButton==='Section1button'){
            console.log("inside button 1");
            sectionContainer = component.find('collapsibleSectionContainer1');
        }
        else{
             console.log("inside button 2");
            sectionContainer = component.find('collapsibleSectionContainer2');
        }
        $A.util.toggleClass(sectionContainer, "slds-is-open");
    },
    editModeParent : function(component, event, helper) {
        var params = event.getParam('arguments');
        if (params) {
            var title = params.lookupValueParent;
            console.log('title---',title);
        }
    },
    handleSave : function(component,event,helper) {
        helper.UpdateRecord(component, event);
         component.set("v.showSaveButton",false);
        component.set("v.showEditButton",true);
    },
    
    handleGenericChildEvent : function(component, event) {
        var oldValue = component.get("v.changedFields");
        //var oldValue ;
        console.log('oldValue****',oldValue);
        oldValue.push(event.getParam("changedValues"));
        component.set("v.changedFields",oldValue);
        console.log('checkbox----',component.get("v.changedFields"));
    },
    handleCustomLookupValue : function(component, event, helper) {
        console.log('in 1 parent change---');
        var oldValues = component.get("v.changedFields");
        oldValues.push(event.getParam("selectedRecord"));
        component.set("v.changedFields",oldValues);
        var params=event.getParam("selectedRecord");
        console.log('in 1 parent change---', params);
        console.log('get changed values'+component.get("v.changedFields"));
    },
    showSuccessToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Success Message',
            message: 'Mode is pester ,duration is 5sec and this is normal Message',
            messageTemplate: 'Record {0} created! See it {1}!',
            duration:' 5000',
            key: 'info_alt',
            type: 'success',
            mode: 'pester'
        });
        toastEvent.fire();
    },
    
    sectionOne : function(component, event, helper) {
        var acc = component.find("articleOne");
            $A.util.toggleClass(acc, 'slds-show');  
            $A.util.toggleClass(acc, 'slds-hide');  
    }
})