({
	doInit : function(component, event, helper) {
		var action = component.get("c.getLead");
        action.setCallback(this, function(a){
            var state = a.getState();
            alert(state);
            if(state == "SUCCESS"){
                component.set("v.leads",a.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    handleComponentEvent : function(component, event, helper) {
        var evtld = event.getParam("newlead");
        alert("created Lead is:"+JSON.stringify(evtld));
        var item = [];
        item = component.get("v.leads");
        item.push(evtld);
        component.set("v.leads",item);
    }
})