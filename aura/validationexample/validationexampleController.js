({
	validate: function(component, event, helper) {

        // Simplistic error checking
        var validExpense = true;

        // Name must not be blank
        var nameField = component.find("cname");
        var conname = nameField.get("v.value");
        if ($A.util.isEmpty(conname)){
            validExpense = false;
            nameField.set("v.errors", [{message:" name can't be blank."}]);
        }
        else {
            nameField.set("v.errors", null);
        }
        
        var lastField = component.find("lname");
        var conlastField = lastField.get("v.value");
        if ($A.util.isEmpty(conlastField)){
            validExpense = false;
            lastField.set("v.errors", [{message:" last Name can't be blank."}]);
        }
        else {
            lastField.set("v.errors", null);
        }

        var lastField = component.find("email");
        var conlastField = lastField.get("v.value");
        if ($A.util.isEmpty(conlastField)){
            validExpense = false;
            lastField.set("v.errors", [{message:" email can't be blank."}]);
        }
        else {
            lastField.set("v.errors", null);
        }
        
        var genderField = component.find("Phone");
        var conGenField = genderField.get("v.value");
        if ($A.util.isEmpty(conGenField)){
            validExpense = false;
            genderField.set("v.errors", [{message:" Gender can't be blank."}]);
        }
        else {
            genderField.set("v.errors", null);
        }
        
 
    }
    
})