({
	getContact : function(component, event, helper) {
		var accountId = $A.get("e.c:ContactEvent");
        var accid = event.getSource().get("v.value");
        accountId.setParams({
            "accountId":accid
        });
        accountId.fire();
	}
})