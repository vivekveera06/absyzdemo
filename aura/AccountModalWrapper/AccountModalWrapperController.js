({
	doInit : function(component, event, helper) {
		
        var action=component.get("c.fetchAccounts");
        action.setCallback(this,function(response){
            
            var state=response.getState();
            
            if(state==='SUCCESS'){
                component.set("v.AccountList",response.getReturnValue());
            }
            
        });
        $A.enqueueAction(action);
	},
    getAcc:function(component, event, helper){
        
        var ifchecked=event.getSource().get("v.checked");
        
        if(ifchecked==true){
        	var checkvalue=event.getSource().get("v.title");
        	component.set("v.AccountName",checkvalue.Name);
        	alert(component.get("v.AccountName"));
        	component.set("v.contacts.AccountId",checkvalue.Id);
        	alert(component.get("v.contacts.AccountId"));
         	$A.util.addClass(component.find('popUpId'), 'hideContent');
        	$A.util.addClass(component.find('popUpBackgroundId'), 'hideContent');
        }
        else{
            
        }
    },
    hidePopup:function(component, event, helper){
        
        $A.util.addClass(component.find('popUpId'), 'hideContent');
        $A.util.addClass(component.find('popUpBackgroundId'), 'hideContent');
	},
    getAccount:function(component, event, helper){
        

      	//component.find("check").set("v.checked",bool);
        $A.util.removeClass(component.find('popUpId'), 'hideContent');
        $A.util.removeClass(component.find('popUpBackgroundId'), 'hideContent');

    },
    createCon:function(component, event, helper){
        var action=component.get("c.insertcon");
        action.setParams({
            "c":component.get("v.contacts")
        });
        action.setCallback(this,function(response){
            var state=response.getState();
            	if(state==='SUCCESS'){
                    alert('contact inserted ');
                	/*var toastEvent = $A.get("e.force:showToast");
    				toastEvent.setParams({
       					 "title": "Success!",
        				 "message": "The record has been inserted successfully."
    				});
    				toastEvent.fire();*/
            	}
            	else {
                    alert('failed');
                   /* var toastEvent = $A.get("e.force:showToast");
    				toastEvent.setParams({
       					 "title": "error!",
        				 "message": "Some error."
    				});
    				toastEvent.fire();*/
                
                
            }
            
        });
        
        $A.enqueueAction(action);
    }
    
})