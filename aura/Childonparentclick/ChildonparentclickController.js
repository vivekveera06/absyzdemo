({
	getAllAccs : function(component, event, helper) {
		var action = component.get("c.getAccountDetails");
        action.setCallback(this,function(result){
            var state = result.getState();
            if (state === "SUCCESS") {
            alert("result "+result.getReturnValue());
            component.set("v.listAccs",result.getReturnValue()); 
            }
        });
        $A.enqueueAction(action);
	},
   /* handleSearchEvt : function(component, event, helper){
        var searchedString= event.getParam("searchString");
        alert("handler event fired"+event.getParam("searchString"));
         var action= component.get("c.getAccountDetailsByName");
        action.setParams({
            "AccName": searchedString
        });
        action.setCallback(this,function(response){
            component.set("v.listAccs",response.getReturnValue());
            alert("handledSearch result : "+response.getReturnValue());
        });
            $A.enqueueAction(action); 
            
    },*/
    ShowContacts : function(component, event, helper){
        		var src = event.getSource();
        var accountName = src.get("v.value");
    		var action = component.get("c.getcontactsofAccount");
        action.setParams({
            "acId": accountName
        });
        action.setCallback(this,function(result){
            var state = result.getState();
            if (state === "SUCCESS") {
            alert("result "+result.getReturnValue());
            component.set("v.listcons",result.getReturnValue()); 
            }
        });
        $A.enqueueAction(action);
   }
})