({
    doInit : function(component, event, helper) {
        
    },
    change :function(component, event, helper){
        console.log('change');
    },
    fieldChange:function(component, event, helper){
        console.log('fieldChange*****',event.getParam("value"));
        console.log('changevalues sfd------',JSON.stringify(component.get("v.field")));
        if(event.getParam("value")!==undefined){
            component.set("v.field.value", event.getParam("value"));
            var componentEvent = component.getEvent("ChangedValuesEvent");
            componentEvent.setParams({
                'changedValues':component.get("v.field")
            });
            componentEvent.fire();
        }
        else if(event.getParam("checked")!=undefined){
            console.log('event.getParam("checked")------',event.getParam("checked"));
            component.set("v.field.value", event.getParam("checked"));
            var componentEventCheckbox = component.getEvent("ChangedValuesEvent");
            componentEventCheckbox.setParams({
                'changedValues':component.get("v.field")
            });
            componentEventCheckbox.fire();
        }
    },
    SelectedRecord: function(component, event, helper){
    },
    editMode: function(component, event, helper){
        
        component.set("v.showSpinner",true);
        var cmpEvent = component.getEvent("cmpEvent");
        cmpEvent.fire();
        
    }
})