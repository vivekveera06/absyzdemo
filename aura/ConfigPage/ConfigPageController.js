({
	myAction : function(component, event, helper) {
		
	},
        save : function(component, event, helper) {
        var whichOne = event.getSource().get("v.value");
        alert('whichOne'+whichOne);
        helper.save(component);
    	
    },
    waiting: function(component, event, helper) {
    	$A.util.addClass(component.find("uploading").getElement(), "uploading");
    	$A.util.removeClass(component.find("uploading").getElement(), "notUploading");
    },
    doneWaiting: function(component, event, helper) {
    	$A.util.removeClass(component.find("uploading").getElement(), "uploading");
    	$A.util.addClass(component.find("uploading").getElement(), "notUploading");
    }
})