({
           doinit : function(component, event, helper) {
		var action=component.get("c.getaccounts");
        action.setCallback(this,function(response){
            var state=response.getState();
            alert('state'+state);
            if(state=="SUCCESS"){
            component.set("v.Accounts1",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);        
    },
	deleteacc : function(component, event, helper) {
		 var accdel=event.getParam("AccountId");
        var action=component.get("c.deleteaccount");
        action.setParams({
            "acc":accdel
        });
        action.setCallback(this,function(res){
            var state=res.getState();
            if(state=="SUCCESS"){                
                //component.set("v.Accounts1", res.getReturnValue());
                var acc = component.get("v.Accounts1");
                var items = [];
                for(var i=0; i<acc.length;i++){
                    if(acc[i].Id != accdel.Id){
                        items.push(acc[i]); 
                    }
                }
                component.set("v.Accounts1", items);
            }
            else 
                alert("ERROR");
        });
        $A.enqueueAction(action);
	}
})