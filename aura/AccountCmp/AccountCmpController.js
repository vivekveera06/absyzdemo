({
    insert : function(component, event, helper) {
        
        var action=component.get("c.InsertAcc");
        action.setParams({
            "ac":component.get("v.acc")
            
        });
        action.setCallback(this,function(a){
            
            var state=a.getState();
            if(state==='SUCCESS'){
                
                alert(state);
                var evt = $A.get("e.force:navigateToComponent");
                
                evt.setParams({
                    
                    componentDef :'c:AccordianCmp'
                });
                evt.fire();
                
            }
            else if(state==='ERROR'){
                
               alert('error'+state); 
            }
        });
        $A.enqueueAction(action);
    },
    options:function(component, event, helper){
        var evt = $A.get("e.force:navigateToComponent");
                
                evt.setParams({
                    
                    componentDef :'c:AccordianCmp'
                });
                evt.fire();
    }
})