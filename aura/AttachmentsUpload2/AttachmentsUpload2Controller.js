({
	doInit : function(component, event, helper) {
		
        var action=component.get("v.attachmentList");
        action.setParams({
            contactId:component.get("v.recordId")
		});
        action.setCallback(this,function(a){
            var state=a.getState();
            if (state === "SUCCESS"){
                console.log('return'+JSON.stringify(a.getReturnValue()));
                component.set("v.attachmentList",a.getReturnValue());
            }
		});
        $A.enqueueAction(action);
	}
    
})