({
    clear: function (component, event) {
        //Pushmitha: Child component controller to get the id of the pill that needs
        //to be removed.
        
        var x = component.get('v.type');
        //alert('value'+x);
        if (x == 'to') {
            var value = component.get('v.obj');
            //alert('check id child'+JSON.stringify(value));
            var toEvent = component.getEvent("idTo");
            toEvent.setParams({
                "idTo" : value
            });
            toEvent.fire();
            //alert("to event fired"+value);
            
            
            //alert('toValue'+JSON.stringify(value));
            //var idVal = toValue.value;
            
            var appEvent = $A.get("e.c:customStrikeAppEvent");
            appEvent.setParams({ "Id" : value});
            appEvent.fire();   
            //alert("to app event fired"+JSON.stringify(value));
        }
        else if(x == "cc"){
            var value = component.get('v.obj');
            //alert('check id child'+JSON.stringify(value));
            var ccEvent = component.getEvent("idCc");
            ccEvent.setParams({
                "idCc" : value
            });
            ccEvent.fire();
            //alert("cc event fired");
            
            var appEvent = $A.get("e.c:customStrikeAppEvent");
            appEvent.setParams({ "Id" : value});
            appEvent.fire();   
            //alert("cc app event fired"+JSON.stringify(value));
        }
            else if(x == "bcc"){
                var value = component.get('v.obj');
                //alert('check id child'+JSON.stringify(value));
                var bccEvent = component.getEvent("idBcc");
                bccEvent.setParams({
                    "idBcc" : value
                });
                bccEvent.fire();
                //alert("bcc event fired");
                
                var appEvent = $A.get("e.c:customStrikeAppEvent");
                appEvent.setParams({ "Id" : value});
                appEvent.fire();   
                //alert("bcc app event fired"+JSON.stringify(value));
            }
        
    },
})