({
    doInit : function(component, event, helper) {
        component.set('v.Column', [
            {label: 'Id', fieldName: 'Id', type: 'text'},
            {label: 'Employee Name', fieldName: 'Name', type: 'text'},
            {label: 'Date of Birth', fieldName: 'Date_Of_Birth__c', type: 'date'}
        ]);
        
        if(sessionStorage.getItem('employee7'))
        {
            alert("Fetching employeeList from custom cache");
            component.set("v.EmployeeList",JSON.parse(sessionStorage.getItem('employee7')));
        }
        
        else{
            var action = component.get("c.getEmployeeDetails");
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") { 
                    var values = response.getReturnValue();
                    component.set("v.EmployeeList",values); 
                    sessionStorage.setItem('employee7', component.get("v.EmployeeList"));
                    alert("Fetching employeeList from server");
                }
            });
            $A.enqueueAction(action);
        }
        
        
    }
})