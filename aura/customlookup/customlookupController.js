({
    doInit:function(component,event,helper){
        console.log('selectedRecord -- value on load--'+component.get("v.selectedRecord"));
        if(component.get("v.selectedRecord")!=='undefined'){
            console.log('selectedRecord inside condition '+component.get("v.selectedRecord"));
            component.set("v.selectedRecord",component.get("v.selectedRecord"));
            console.log('selectedRecord inside condition '+component.get("v.selectedRecord"));
            
            var forclose = component.find("lookup-pill");
            $A.util.addClass(forclose, 'slds-show');
            $A.util.removeClass(forclose, 'slds-hide');
            
            var forclose = component.find("searchRes");
            $A.util.addClass(forclose, 'slds-is-close');
            //$A.util.removeClass(forclose, 'slds-is-open');
            
            var lookUpTarget = component.find("lookupField");
            $A.util.addClass(lookUpTarget, 'slds-hide');
        }
        
    },
    onfocus : function(component,event,helper){
        console.log('selectedRecord -'+JSON.stringify(selectedRecord));
        //alert('JSON.stringify(selectedRecord)'+JSON.stringify(selectedRecord))
        //component.set("v.SearchKeyWord",component.get("!v.selectedRecord.name"));
        $A.util.addClass(component.find("mySpinner"), "slds-show");
        var forOpen = component.find("searchRes");
        $A.util.addClass(forOpen, 'slds-is-open');
        $A.util.removeClass(forOpen, 'slds-is-close');
        // Get Default 5 Records order by createdDate DESC  
        var getInputkeyWord = '';
        helper.searchHelper(component,event,getInputkeyWord);
    },
    onblur : function(component,event,helper){       
        component.set("v.listOfSearchRecords", null );
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
    },
    keyPressController : function(component, event, helper) {
        // get the search Input keyword   
        var getInputkeyWord = component.get("v.SearchKeyWord");
        console.log('getInputkeyWord--'+getInputkeyWord);
        // check if getInputKeyWord size id more then 0 then open the lookup result List and 
        // call the helper 
        // else close the lookup result List part.   
        if( getInputkeyWord.length > 0 ){
            var forOpen = component.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');
            helper.searchHelper(component,event,getInputkeyWord);
        }
        else{  
            component.set("v.listOfSearchRecords", null ); 
            var forclose = component.find("searchRes");
            $A.util.addClass(forclose, 'slds-is-close');
            $A.util.removeClass(forclose, 'slds-is-open');
        }
    },
    
    // function for clear the Record Selaction 
    clear :function(component,event,heplper){
        var pillTarget = component.find("lookup-pill");
        var lookUpTarget = component.find("lookupField"); 
        
        $A.util.addClass(pillTarget, 'slds-hide');
        $A.util.removeClass(pillTarget, 'slds-show');
        
        $A.util.addClass(lookUpTarget, 'slds-show');
        $A.util.removeClass(lookUpTarget, 'slds-hide');
        
        component.set("v.SearchKeyWord",null);
        component.set("v.listOfSearchRecords", null );
        component.set("v.selectedRecord", {} );   
    },
    
    // This function call when the end User Select any record from the result list.   
    handleComponentEvent : function(component, event, helper) {
        // get the selected Account record from the COMPONETN event 	 
        var selectedRecordGetFromEvent = event.getParam("recordByEvent");
        console.log('selectedAccountGetFromEvent--'+selectedRecordGetFromEvent.Name)
        console.log('selectedAccountGetFromEvent--',selectedRecordGetFromEvent);

        component.set("v.selectedRecord" , selectedRecordGetFromEvent); 
        //component.set("v.selectedRecordevt" , selectedAccountGetFromEvent);
        var forclose = component.find("lookup-pill");
       	$A.util.addClass(forclose, 'slds-show');
        $A.util.removeClass(forclose, 'slds-hide');
        
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
        
        var lookUpTarget = component.find("lookupField");
        $A.util.addClass(lookUpTarget, 'slds-hide');
        $A.util.removeClass(lookUpTarget, 'slds-show'); 
        console.log('component.get("v.APIName")'+component.get("v.APIName"));
        var cmpEvent=component.getEvent("customLookupValue");
        var json={};
        json='{'+'"name":"",'+'"type":"text",'+'"objectName":"",'+'"apiname":'+'"'+component.get("v.APIName")+'",'+'"value":'+'"'+selectedRecordGetFromEvent.Name+'"' +'}';
        
        //cmpEvent.setParams({'selectedRecord':event.getParam("recordByEvent")});
       	cmpEvent.setParams({'selectedRecord':JSON.parse(json)});
        cmpEvent.fire(); 
       /*
        * This is Aura method 
        *  var attributeTag = component.get("v.parent2");
        console.log('attributevalue---'+attributeTag);
    	attributeTag.parentMethod2(selectedRecordGetFromEvent);*/
    }
})