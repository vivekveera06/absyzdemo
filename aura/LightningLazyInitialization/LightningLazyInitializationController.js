({
    openModal : function(component, event, helper) {
        $A.createComponent(
            "c:ModalWindow",
            {
            },
            function(cmp, status) {
                if (status === "SUCCESS") {
                    component.set("v.editBody", cmp);
                } else {
                    console.error("ERROR: Input Component error");
                }
            });
    }
})