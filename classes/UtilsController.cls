public class UtilsController {

    public static List<sObject> getSobjectList(String sObjectName, String fieldList) {
        List<sObject> record = new List<sObject>();
        String query = 'SELECT id' + fieldList + ' FROM ' + sObjectName + '  WHERE id = :requestId';
        
        try {
            record = Database.query(query);
        } catch(Exception e) {
            System.debug('Error :' + e);
        }
        return record;
    }
}