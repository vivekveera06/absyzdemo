@isTest
public class UpdatingRatingTest {
		
    public static testmethod void testmethod1()
    {
        Account acc=new Account(name='Test account',industry='Agriculture');// constructor is used to initialize values.
        test.startTest();
        insert acc;
        acc.industry='consulting';
        update acc;
        acc.industry='null';
        update acc;
         acc.industry='consulting';
        
        update acc;
         acc.industry='electronics';
        update acc;
        test.stopTest();
    }
    
}