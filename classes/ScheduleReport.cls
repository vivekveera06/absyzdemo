public class ScheduleReport {
    public ScheduleReport(){
        list<report> reportList=[select id, name from report where name='TypeOpportunity'];
        string  reportId=(string) reportList.get(0).get('Id');
        reports.ReportResults reportResult=reports.ReportManager.runReport(reportId, true);
        Reports.ReportInstance instance = Reports.ReportManager.runAsyncReport(reportId, true);
		System.debug('Asynchronous instance: ' + instance);
		system.debug('reportResult--->'+reportResult);
    }
}