public class ShowDetalRecords {

    @AuraEnabled
    public static list<AdditinalDetails__c> getRecords(id parentid){
        list<AdditinalDetails__c> childlist=[select id,name,Current_Employer__c,Current_Company__c,Current_Location__c from AdditinalDetails__c where Opportunity__c=:parentid];
        system.debug('childlist -->'+childlist);
        return childlist;
    }
}