public class AccountProcessor {
	 static integer count=0;
    
    @future
    public static void countContacts(List<id> accid){
        
        List<Contact> con=[select id,name from Contact where Accountid in :accid];
        for(Contact c:con){
            count= count+1;
          
        }
        list<Account> acc=[select id,name from Account  where id in :accid];
        for(Account a:acc){
            a.Number_of_Contacts__c=count;
        }
        update acc;
   
        }
}