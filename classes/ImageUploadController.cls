public class ImageUploadController {

    
    
    @AuraEnabled
    public static boolean uploadFile(String fileName,string cid,string base64Data,string contentType){
        
        base64Data=EncodingUtil.urlDecode(base64Data, 'UTF-8');
        
        //contact c=[select id,name,ImageFile__c from contact where id=:cid];
        
        //c.ImageFile__c='vivek';
        //c.ImageFile__c=EncodingUtil.base64Decode(base64Data).toString();
       
        Attachment a=new Attachment();
        
        a.parentId =cid;
        a.Body = EncodingUtil.base64Decode(base64Data);
        a.Name = fileName;
        a.ContentType = contentType; 
        Database.SaveResult scr=DataBase.insert(a,false);
        
        if(scr.isSuccess()){
            
             return true;
        }
        else
            return false;
        
       
        
    }
}