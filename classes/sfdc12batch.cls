global class sfdc12batch implements Database.Batchable<sObject> {

    global database.QueryLocator start(Database.BatchableContext BC){
         string  query='select id,name from Account';
         return Database.getQueryLocator(query);
        
    }
    global void execute(Database.BatchableContext BC,list<Account> scope){
        //public list<Account> aa=new list<Account>();
        for(Account a:scope){
            
            a.name=+a.name+'q';
           // aa.add(a);
        }
        update scope;
        
    }
     global void finish(Database.BatchableContext BC){}    
}