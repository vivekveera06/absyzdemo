public class authentationClass {
    
    public string accessid{get;set;}
    public static string clientid{get;set;}
    public static string redirecturl{get;set;}
    public static  string clientsecret{get;set;}
    public string refreshtoken{get;set;}
    public boolean step2 {get;set;}
    
    public authentationClass(){
         accessid=apexpages.currentpage().getparameters().get('code');
         if(accessid!=null){
          step2=true;
         }
    }
    
   	public PageReference call(){
        PageReference pageRef = new PageReference('https://test.salesforce.com/services/oauth2/authorize?response_type=code&client_id='+clientid+'&redirect_uri='+redirecturl);
   		pageRef.setRedirect(true);
        return pageRef;
                
     }
    
    
    public void callout(){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://test.salesforce.com/services/oauth2/token?grant_type=authorization_code&client_id='+clientid+'&code='+accessid+'&client_secret='+clientsecret+'&redirect_uri='+redirecturl);
        req.setMethod('POST');
       	req.setBody('grant_type=authorization_code&client_id='+clientid+'&code='+accessid+'&client_secret='+clientsecret+'&redirect_uri='+redirecturl); 
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
     	req.setHeader('Content-Length','10240');
        HttpResponse res = h.send(req);
        system.debug('responsebody'+res.getbody());
        refreshtoken = res.getbody();
       }
}