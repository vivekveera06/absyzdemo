global class sfdcbatchinsert implements Database.Batchable<sObject> {

    global database.QueryLocator start(Database.BatchableContext BC){
         string  query ='select id,name from Account';
         return Database.getQueryLocator(query);
        
    }
    global void execute(Database.BatchableContext BC,list<Account> scope){
        list<Account> aa=new list<Account>();
        for(Account a:scope){   
            Account r=new Account();
            r.name=a.name+'hi';
           aa.add(r);
        }
        insert aa;      
    }
     global void finish(Database.BatchableContext BC){}
    
}