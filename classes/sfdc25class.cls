public class sfdc25class {


    public String Sobjects { get; set; }  
    public List<SelectOption> selectFields {get; set; }
    public String Fields { get; set; } 
    public list<SelectOption> selectRecords { get; set; }
    public String Records { get; set; }
    map<String, Schema.SobjectType> so = Schema.getGlobalDescribe();
    
    public List<SelectOption> getSelectObjects() {
        List<String> S = new List<String>(so.keyset());
        List<SelectOption> op = new List<SelectOption>();
        for(String S1:S){
            op.add(new SelectOption(S1,S1));
            op.sort();
        }
        return op;
        
    }

    public PageReference selFields() {
        map<String,Schema.SObjectField> sf = so.get(Sobjects).getDescribe().fields.getMap();
        List<String> S2 = new List<String>(sf.keyset());
        selectFields= new List<SelectOption>();
       for(String S3:S2)
           selectFields.add(new SelectOption(S3,S3));
        return null;
    }
    
     public PageReference selRecords() {
        String str='select '+Fields+' from '+Sobjects;
        List<SObject> sob=Database.query(str);
        selectRecords=new list<SelectOption>();
        for(SObject gf:sob){
            if(gf.get(Fields)!=NULL)
                selectRecords.add(new SelectOption(String.valueOf(gf.get(Fields)),String.valueOf(gf.get(Fields))));
        }       
        return null;
    }   
}