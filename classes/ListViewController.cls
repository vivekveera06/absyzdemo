public with sharing class ListViewController {
    
    /* fetch all the Workflow records for the listview */
    @AuraEnabled
    public static List<Workflow1__c> getWorkflowRecords(){
        
        list<String> profile_List=new list<String>();
        
        /* Custom Setting Record to get All profiles for which all workflows to be displayed. */
        string profileList=system.Label.Profile_Categorization;
        String query = 'select Name__c,Category__c,Application_Key__c,Description__c,IsChinaUser__c,Predefined_Approver_01__c from Workflow1__c';
        
        system.debug('profileList --'+profileList);
        
        for(string s : profileList.split(',')){
            profile_List.add(s);
        }
        
        system.debug('profile_List--'+profile_List);
        string profileName = [select name from profile where id=:userInfo.getProfileId()].Name;
        system.debug('p-->'+profileName);
        
        
        
        if(profile_List.contains(profileName)){
            
            list<Sobject> workflowlist = database.query(query);
            system.debug('workflowList ------'+workflowList);
            return workflowList;       
        }
        else{
            
            map<String,set<String>> keyMap = new map<String,set<String>>();
            map<Boolean,set<String>> BooleanMap = new map<Boolean,set<String>>();
            
            user loggedInUser = [select name,Location__c,Department__c from user where Id=:userinfo.getuserId()];
            list<Profile_Categorization__mdt> metadata_list = [select id,name__c,Department__c,Location__c,User__c,
                                                               General_Workflow__c, General__c from
                                                               Profile_Categorization__mdt];
            
            string loggedInUserName = loggedInUser.name;
            string loggedInUserLocation = loggedInUser.Location__c;
            string loggedInUserDepartment = loggedInUser.Department__c;
            
            system.debug('loggedInUser---'+loggedInUser.Location__c);
            system.debug('loggedInUser.name -->'+loggedInUser.name);
            system.debug('loggedInUser.name -->'+loggedInUser.Department__c);
            
            
            for(Profile_Categorization__mdt key:metadata_list){
                
                /* For Checking users */
                if(keyMap.containsKey(key.User__c)){
                    set<string> temppoints = keyMap.get(key.User__c);
                    temppoints.add(key.name__c);
                    keyMap.put(key.User__c, temppoints);
                }else{
                    if( key.User__c != null && key.User__c.contains(',') && key.User__c.contains(loggedInUserName)) {
                        string dummystring = key.User__c;
                        for(string currentUserName : dummystring.split(',')){ 
                            if(currentUserName.contains(loggedInUserName)){
                                set<string> temppointsnew= new set<string>();
                                temppointsnew.add(key.name__c);
                                keyMap.put(currentUserName, temppointsnew);
                            }
                        }
                    }
                    else{
                        if( key.User__c != null && key.User__c.contains(loggedInUserName)) {
                            set<string> temppointsnew= new set<string>();
                            temppointsnew.add(key.name__c);
                            keyMap.put(key.User__c, temppointsnew);
                        }
                    }
                }
                /* For Checking Department type of workflows */
                if(keyMap.containsKey(key.Department__c)) {
                    set<string> temppoints = keyMap.get(key.Department__c);
                    temppoints.add(key.name__c);
                    keyMap.put(key.Department__c, temppoints);                
                }else{
                    if( key.Department__c != null && key.Department__c.contains(',') && key.Department__c.contains(loggedInUserDepartment)){
                        system.debug('key.Department__c-->'+key.Department__c);
                        string dummystring = key.Department__c;
                        for(string currentDepartment : dummystring.split(',')){
                            if(currentDepartment.contains(loggedInUserDepartment)) {
                                set<string> temppointsnew= new set<string>();
                                temppointsnew.add(key.name__c);
                                keyMap.put(currentDepartment, temppointsnew); 
                            }
                        }
                    }
                    else{
                        if( key.Department__c != null && key.Department__c.contains(loggedInUserDepartment)){
                            set<string> temppointsnew= new set<string>();
                            temppointsnew.add(key.name__c);
                            keyMap.put(key.Department__c, temppointsnew);
                        }
                    }
                }
                /* For Checking Location type of workflows */
                if(keyMap.containsKey(key.Location__c)) {
                    set<string> temppoints = keyMap.get(key.Location__c);
                    temppoints.add(key.name__c);
                    keyMap.put(key.Location__c, temppoints);                
                } else{
                    if(key.Location__c != null && key.Location__c.contains(',') && key.Location__c.contains(loggedInUserLocation)) {
                        system.debug('key.Location__c-->'+key.Location__c);
                        string dummystring = key.Location__c;
                        for(string currentnlocation : dummystring.split(',')){
                            if(currentnlocation.contains(loggedInUserLocation)){
                                set<string> temppointsnew= new set<string>();
                                temppointsnew.add(key.name__c);
                                keyMap.put(currentnlocation, temppointsnew);
                            }
                        }
                    }
                    else{
                        if( key.Location__c != null && key.Location__c.contains(loggedInUserLocation)) {
                            set<string> temppointsnew= new set<string>();
                            temppointsnew.add(key.name__c);
                            keyMap.put(key.Location__c, temppointsnew);
                        }
                    }
                }
                /* For Checking General type of workflows */
                if(BooleanMAp.containsKey(key.General_Workflow__c)) {
                    set<string> temppoints = BooleanMAp.get(key.General_Workflow__c);
                    temppoints.add(key.name__c);
                    BooleanMAp.put(key.General_Workflow__c, temppoints); 
                }
                else {
                    if(key.General_Workflow__c != null && key.General_Workflow__c==true) {
                        set<string> temppointsnew= new set<string>();
                        temppointsnew.add(key.name__c);
                        BooleanMAp.put(key.General_Workflow__c, temppointsnew);
                    }
                }
            }
            system.debug('keyMap --->'+keyMap);
            system.debug('BooleanMAp-->'+BooleanMAp);
            set<String> mergedSet = new Set<String>();
            
            for(string currentKeyValue : keyMap.keySet()){
                system.debug('currentKeyValue-->'+currentKeyValue);
                if(keyMap.containsKey(loggedInUserName)) {
                    mergedSet.addAll(keyMap.get(currentKeyValue)); 
                }
                else if(keyMap.containsKey(loggedInUserLocation)) {
                    mergedSet.addAll(keyMap.get(currentKeyValue));
                }
                else if(keyMap.containsKey(loggedInUserDepartment)) {
                    mergedSet.addAll(keyMap.get(currentKeyValue));
                }
            }
            
            system.debug('BooleanMAp.values() -->'+BooleanMAp);
            
            for(boolean currentKey : BooleanMAp.keySet()){
                system.debug('currentKey -->'+currentKey);
                mergedSet.addAll(BooleanMAp.get(currentKey));
            }
            
            system.debug('BooleanMAp.values() -->'+BooleanMAp.values());
            system.debug('mergedSet -->'+mergedSet);
            query = query+' where name__c IN : mergedSet';
            system.debug('query -->'+query); 
            list<Sobject> workflowlist = database.query(query);
            system.debug('workflowList ------'+workflowList);
            return workflowlist;
            
        }
        
    }
}