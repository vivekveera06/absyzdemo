public with sharing class Comp {
public string searchname{get;set;}
public string csearchstrength{get;set;}
public string csearchweakness{get;set;}
public list<Competitor__c> com{get;set;}

public String selected { get; set; }    

    public Comp(ApexPages.StandardController controller)
    {

    
    }
     public pagereference scom()
    {
           
            
           if((searchname<>null)&&(csearchstrength<>null)&&(csearchweakness<>null)){
           
             com=[select name,Competitors_Strength__c,Competitors_Weakness__c from Competitor__c where (name like: '%'+searchname+'%') and (Competitors_Strength__c like: '%'+csearchstrength+'%' )and(Competitors_Weakness__c like: '%'+csearchweakness+'%') ];
            
            }
            else if((searchname<>null)&&(csearchstrength==null)&&(csearchweakness==null)){
           
            com=[select name,Competitors_Strength__c,Competitors_Weakness__c from Competitor__c where (name like: '%'+searchname+'%') ];
            
            
            }
            else if((searchname<>null)&&(csearchstrength==null)&&(csearchweakness<>null)){
           com=[select name,Competitors_Strength__c,Competitors_Weakness__c from Competitor__c where (name like: '%'+searchname+'%') and(competitors_weakness__c like: '%'+csearchweakness+'%') ];
            }           
     return null;
    }
}