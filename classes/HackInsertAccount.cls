public with sharing class HackInsertAccount {
@auraEnabled
    public static Account newAccount(Account acc){       
        insert acc;
        return acc;        
    } 
    
        
}