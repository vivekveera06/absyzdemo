public class AccountsControllerDelete {

    @AuraEnabled
   public static List<Account> getAccountsList() {
       return [SELECT Id, name, industry, Type, NumberOfEmployees, TickerSymbol, Phone
       FROM Account ORDER BY createdDate DESC LIMIT 10];
   }   
     @AuraEnabled
    public static list<Account> setAccount(ID acc) {
       account ac=[select id from account where id=:acc];
        delete ac;
       return [SELECT Id, name, industry, Type, NumberOfEmployees, TickerSymbol, Phone
       FROM Account ORDER BY createdDate DESC LIMIT 10];
   }
}