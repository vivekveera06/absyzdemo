public class LeadConvertClass {
    public list<lead> leadgroup;
    
    public static void convertChildleads(List<lead> leads)
    {
        string teamid; // group of leads team id
        string lid;	// lead id on which convert button is clicked.
        string accid;	// converted account id
        map<string,string> leadid_accid=new map<string,string>();
        map<string,string> leadid_teamid=new map<string,string>();
        
        for(lead l:leads)
        {
            leadid_accid.put(l.id,l.Team_id__c);
            leadid_teamid.put(l.Id,l.ConvertedAccountId);
            //teamid=l.Team_id__c;
            //accid=l.convertedAccountId;
            //lid=l.Id;
        }
        
        list<lead> leadgroup=[select id,name,team_id__c,User_id__c from lead where team_id__c=:teamid and id!=:lid  and IsConverted=:false];
        system.debug('list of child leads'+leadgroup);
        if(leadgroup.size()>0)
        {
            for(lead l:leadgroup)
            {
                Database.LeadConvert lc = new Database.LeadConvert();
                lc.setLeadId(l.Id);
                lc.setAccountId(accid);
                lc.setDoNotCreateOpportunity(true);
                LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
                lc.setConvertedStatus(convertStatus.MasterLabel);
                database.LeadConvertResult lcr=database.convertLead(lc);
            }
        }
        
    }   
}