public class sfdc18class {
	
    public list<lead> l{get;set;} 
    public string inputlastname{get;set;}
    public string inputcompany{get;set;}
    public string inputLeadSource{get;set;}
    public sfdc18class(Apexpages.StandardController contr){}
    public pageReference save(){
        lead ld=new lead();
        ld.LastName=inputlastname;
        ld.Company=inputcompany;
        ld.Status=inputLeadSource;
        insert ld;
        PageReference page = new PageReference('/apex/sfdc18link');
        page.setRedirect(true);
        return page;
    }
    
    
}