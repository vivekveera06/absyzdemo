public class InsertCon {

    @AuraEnabled
    public static boolean Insertcont(contact cont,string accid){
        
        cont.accountid=accid;
        system.debug('accid'+accid);
        Database.SaveResult scr=Database.insert(cont, false);
        
        if(scr.isSuccess()){
            
            return true;
        }
        else
            return false;
    }
}