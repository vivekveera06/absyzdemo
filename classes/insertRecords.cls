public class insertRecords {
	@AuraEnabled
    public static void insertRecords(account ac,contact co,opportunity op){
        
        Database.SaveResult srList = Database.insert(ac, false);
       
            
            if (srList.isSuccess()){
                
                //if operation is successful we will add the assign account record id to contact lookup.
                co.AccountId=srList.getId();
                system.debug('account id'+srList.getId());
                insert co;
                op.AccountId=srList.getId();
                system.debug('contact id'+srList.getId());
                op.StageName='Prospecting';
                insert op;
            }
            else {
                
                for(Database.Error err: srList.getErrors()){
                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    
                }
            }
       

        
    }
}