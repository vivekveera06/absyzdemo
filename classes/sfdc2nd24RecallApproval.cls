global class sfdc2nd24RecallApproval {

    webservice static void methodrecallApproval(Id recordId)    
    {        
        List<ProcessInstanceWorkitem> piwi = [SELECT Id, ProcessInstanceId, ProcessInstance.TargetObjectId FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId =: recordId];
        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
        req.setAction('Removed');        
        req.setWorkitemId(piwi.get(0).Id);
   
        Approval.process(req,false);
    }
    
    
}