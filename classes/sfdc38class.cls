public class sfdc38class {

    public void email( ){
        list<b__c> email1=[select email__C from b__c];
        for(b__c b:email1){
            string email=b.email__C;
        customEmail(email);
    }
        
    }
    
    
    public void customEmail(string email){
    	Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
		EmailTemplate et=[Select id from EmailTemplate where name = 'Communities: New Member Welcome Email' limit 1];
 
			message.toAddresses = new String[] { email, email };
			message.optOutPolicy = 'FILTER';
			message.subject = 'template Out Test Message';
         	 message.setTemplateId(et.id);
			Messaging.SingleEmailMessage[] messages = 
   				 new List<Messaging.SingleEmailMessage> {message};
        	 Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
			if (results[0].success) {
   			 System.debug('The email was sent successfully.');
		}
        else 
        {
    		System.debug('The email failed to send: '+ results[0].errors[0].message);
				}
       
   	 }
}