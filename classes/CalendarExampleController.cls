public class CalendarExampleController {
/*    
    public Boolean includeMyEvents {get;set;}
    public string currentRecordId;
    public boolean displayPopUp{get;set;}
    public list<calEvent> events {get;set;}
    public list<TimeSlots> timeslotwrapper{get;set;}
    public date data{get;set;}
    public map<date,list<TimeSlots>> slots_map;
    public Preffered__c preferedslot{get;set;}
    public boolean errBoolean1{get;set;}
    public string Message{get;set;}  
    public integer rowToDelete{get;set;}
    public static  integer i=1;
    //The calendar plugin is expecting dates is a certain format. We can use this string to get it formated correctly
    
    String dtFormat = 'EEE, d MMM yyyy HH:mm:ss ';
    
    //constructor
    public CalendarExampleController() {
        
        //Default showing my events to on
        
        includeMyEvents = true;
        currentRecordId=Apexpages.currentPage().getParameters().get('id');
        displayPopUp=false;
        timeslotwrapper= new list<TimeSlots>();
        slots_map= new map<date,list<TimeSlots>>();
        errBoolean1=false;
        
    }
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('1 AM','1 AM'));
        options.add(new SelectOption('2 AM','2 AM'));
        options.add(new SelectOption('3 AM','3 AM'));
        options.add(new SelectOption('4 AM','4 AM'));
        options.add(new SelectOption('5 AM','5 AM'));
        options.add(new SelectOption('6 AM','6 AM'));
        options.add(new SelectOption('7 AM','7 AM'));
        options.add(new SelectOption('8 AM','8 AM'));
        options.add(new SelectOption('9 AM','9 AM'));
        options.add(new SelectOption('10 AM','10 AM'));
        options.add(new SelectOption('11 AM','11 AM'));
        options.add(new SelectOption('12 PM','12 PM'));
        options.add(new SelectOption('1 PM','1 PM'));
        options.add(new SelectOption('2 PM','2 PM'));
        options.add(new SelectOption('3 PM','3 PM'));
        options.add(new SelectOption('4 PM','4 PM'));
        options.add(new SelectOption('5 PM','5 PM'));
        options.add(new SelectOption('6 PM','6 PM'));
        options.add(new SelectOption('7 PM','7 PM'));
        options.add(new SelectOption('8 PM','8 PM'));
        options.add(new SelectOption('9 PM','9 PM'));
        options.add(new SelectOption('10 PM','10 PM'));
        options.add(new SelectOption('11 PM','11 PM'));
        options.add(new SelectOption('12 AM','12 AM'));
         return options;
    }      
    
    
    public PageReference pageLoad() {
        
        events = new list<calEvent>();
        
        list<Preffered__c> meets=new list<Preffered__c>();
        meets=[select id,Name,End_Date_Time__c,Start_Date_Time__c,Booked_Slots__c,Employee__c,Available_Slots__c from Preffered__c where Employee__c=:currentRecordId ];
        system.debug('meets -->'+meets);
        
        for(Preffered__c currentslot:meets){
            system.debug('currentslot -->'+currentslot);
            if(currentslot.Available_Slots__c!=null){
                system.debug('available slots -->'+currentslot.Available_Slots__c);
                list<String> splittime=currentslot.Available_Slots__c.split(';');
                for( string s:splittime){
                    DateTime startDT;
                    DateTime endDT;
                    list<String> substringlist=s.split('-');
                    for(String currenttime:substringlist){
                        integer value1;
                        integer i=1;
                        string currentvalue;
                        if(currenttime.contains('PM')){
                            currentvalue=currenttime.removeEnd(' PM');
                            value1=Integer.valueOf(currentvalue)+12;
                        }
                        else{
                            currentvalue=currenttime.removeEnd(' AM');
                            value1=Integer.valueOf(currentvalue);
                        }
                        if(i==1){
                            time t=time.newInstance(00,00,00,00).addhours(value1);
                            system.debug('t ->'+t);
                            system.debug('t hour -->'+t.hour());
                            system.debug('1st time -->'+value1);
                            startDT = datetime.newInstance(currentslot.Start_Date_Time__c.Year(),currentslot.Start_Date_Time__c.Month(), currentslot.Start_Date_Time__c.Day(),t.hour(),t.minute(),00);
                            system.debug('startdt -->'+startDT);
                        }
                        else{
                            time t=time.newInstance(00,00,00,00).addhours(value1);
                            system.debug('t ->'+t);
                            system.debug('t hour -->'+t.hour());
                            system.debug('2nd time -->'+value1);
                            endDT= datetime.newInstance(currentslot.End_Date_Time__c.Year(),currentslot.End_Date_Time__c.Month(), currentslot.End_Date_Time__c.Day(),t.hour(),t.minute(),00);
                            system.debug('endDT -->'+endDT);
                        }
                        i++;
                    }
                    //DateTime startDT = datetime.newInstance(currentslot.Start_Date_Time__c.Year(),currentslot.Start_Date_Time__c.Month(), currentslot.Start_Date_Time__c.Day(),time.newInstance(00, 01, 00,00).addHours(s),00,00);
                    //DateTime endDT= datetime.newInstance(currentslot.End_Date_Time__c.Year(),currentslot.End_Date_Time__c.Month(), currentslot.End_Date_Time__c.Day(),currentslot.End_Date_Time__c.hour(),currentslot.End_Date_Time__c.minute(),00);
                    calEvent slot = new calEvent();
                    slot.title = currentslot.Name + ' ';
                    slot.allDay = false;
                    slot.startString = startDT.format(dtFormat,'Asia/Kolkata');
                    //slot.endString = endDT.format(dtFormat,'Asia/Kolkata');
                    //slot.url=URL.getSalesforceBaseUrl().toExternalForm() + '/apex/CalendarPopup?id=' + currentslot.Id;
                    //slot.url='/'+currentslot.id;
                    slot.className='Prefered-slot';
                    system.debug('slots -->'+slot);
                    events.add(slot);
                }
                
            }
        }
        //Get my Events if we have selected the correct option
        system.debug('events -->'+events);
        if(includeMyEvents){}
        return null;
        
    }
    public void ShowModal(){
        displayPopUp=true;
        TimeSlots t= new TimeSlots();
        t.index=1;
        
        timeslotwrapper.add(t);
        system.debug('timeslotwrapper  -->'+timeslotwrapper);
    }
    public void close(){
        displayPopUp=false;
        timeslotwrapper= new list<TimeSlots>();
        errBoolean1=false;
    }
    public void AddRow(){
        system.debug('timeslotwrapper -->'+timeslotwrapper);
       	TimeSlots t= new TimeSlots();
        t.index= timeslotwrapper.size()+1;
        timeslotwrapper.add(t);
    }
    public void save(){
        list<Preffered__c> Preffered_list=new list<Preffered__c>();
        system.debug('timeslotwrapper -->'+timeslotwrapper);
        system.debug('Wrapper size -->'+timeslotwrapper.size());
        boolean insertrecords=true;
        list<TimeSlots> TimeSlotsList = new list<TimeSlots>();
        for(TimeSlots currentslot: timeslotwrapper){
            if(currentslot.startdate==null || currentslot.enddate==null || currentslot.startdate<system.today() || currentslot.enddate<system.today()){
                displayPopUp=true;
                insertrecords=false;
                errBoolean1=true; 
                Message='Invalid Date Range, Please check date Range';
            }
            else{
            integer days = currentslot.startdate.daysBetween(currentslot.enddate);
            string start_timeString=currentslot.starttime;
            string end_timeString=currentslot.endtime;
            time start_time;
            time end_time;
            if(start_timeString.contains(' AM')){
                 start_timeString=start_timeString.substring(0,start_timeString.length()-3);
                 start_time=time.newInstance(00,00,00,00).addhours(Integer.valueOf(start_timeString));
            }
            else{
                start_timeString=start_timeString.substring(0,start_timeString.length()-3);
                start_time=time.newInstance(00,00,00,00).addhours(Integer.valueOf(start_timeString)+12);
			}
            if(end_timeString.contains(' AM')){
                 end_timeString=end_timeString.substring(0,end_timeString.length()-3);
                 end_time=time.newInstance(00,00,00,00).addhours(Integer.valueOf(end_timeString));
            }
            else{
                end_timeString=end_timeString.substring(0,end_timeString.length()-3);
                end_time=time.newInstance(00,00,00,00).addhours(Integer.valueOf(end_timeString)+12);
            }
			if(days<0){
                displayPopUp=true;
                insertrecords=false;
                errBoolean1=true; 
                Message='Invalid Date Range, Please check date Range';
            } 
            else if(start_time.hour()>=end_time.hour()){
                 displayPopUp=true;
                 insertrecords=false;
                 errBoolean1=true;
                 Message='Invalid Time Range, Please check Time Range';

            }
            else if(days>0){
                system.debug('days -->'+days);
                date sdt = currentslot.startdate;
                for(integer i=0; i<days; i++){
                    TimeSlots ts = new TimeSlots();
                    ts.startdate = sdt;
                    ts.enddate = sdt;
                    ts.starttime = currentslot.starttime;
                    ts.endtime = currentslot.endtime;
                    
                    TimeSlotsList.add(ts);
                    sdt=sdt+1;
                    system.debug('TimeSlotsList -->'+TimeSlotsList);
                }
            }
            else{
                TimeSlotsList.add(currentslot);
            }
            }

        }
        system.debug('totall'+TimeSlotsList);
        if(insertrecords==true){
            list<Preffered__c> Preffered_lst= new list<Preffered__c>();
            map<Date, Preffered__c> datemap = new map<Date, Preffered__c>();
            for(TimeSlots currentslot:TimeSlotsList){
                if(datemap!=NULL && datemap.size()>0 && datemap.containsKey(currentslot.startdate)){
                    Preffered__c prefer = datemap.get(currentslot.startdate);
                    prefer.Available_Slots__c = prefer.Available_Slots__c+';'+currentslot.starttime+'-'+currentslot.endtime;
                    datemap.put(currentslot.startdate, prefer);
                }
                else{
                    Preffered__c s=new Preffered__c();
                    datetime startdttime = datetime.newInstance(currentslot.startdate, time.newInstance(00, 01, 00,00));
                    datetime enddttime = datetime.newInstance(currentslot.enddate, time.newInstance(23, 59, 00,00));
                    s.Start_Date_Time__c =startdttime;
                    s.End_Date_Time__c=enddttime;
                    s.Available_Slots__c=currentslot.starttime+'-'+currentslot.endtime;
                    s.Employee__c=currentRecordId;
                    datemap.put(currentslot.startdate, s);
                }
            }
            for(Preffered__c pf: datemap.values())
            {
                Preffered_lst.add(pf);
            }
            system.debug('Preffered_lst -->'+Preffered_lst);
            if(Preffered_lst.size()>0){
                displayPopUp=false;
                errBoolean1=false;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter value'));

                database.insert(Preffered_lst, false);
            }
        }
        else{
            displayPopUp=true;
        }
        
    }
    public void DeleteRow(){
      // integer index= timeslotwrapper.indexOf(rowToDelete);
      list<TimeSlots> dummylist=new list<TimeSlots>();
        for(TimeSlots currentwrapper:timeslotwrapper){
            
            if(currentwrapper.index!=rowToDelete){
                currentwrapper.index=i;
                dummylist.add(currentwrapper);
                i++;
            }
            
        }
        timeslotwrapper.clear();
        
        timeslotwrapper.addAll(dummylist);
        system.debug('updated list-->'+timeslotwrapper);
        
    }
    public class TimeSlots{
        public integer index{get;set;}
        public date startdate{get;set;}
        public date enddate{get;set;}
        public String starttime{get;set;}
        public String endtime{get;set;}
        
    }
    //Class to hold calendar event data
    public class calEvent{
        
        public String title {get;set;}
        
        public Boolean allDay {get;set;}
        
        public String startString {get;private set;}
        
        public String endString {get;private set;}
        
        public String url {get;set;}
        
        public String className {get;set;}
        
    }*/
    
}