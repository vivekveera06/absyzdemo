global class LeadProcessor implements Database.Batchable<sObject> {

    global database.QueryLocator start(Database.BatchableContext BC){
        
        string query='select id,name from lead';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC,List<lead> scope){
        for(lead l:scope){
            l.LeadSource='Dreamforce';
        }
         update scope;   
        
    }
    global void finish(Database.BatchableContext BC){
        
    }
    
    
    
    
    
}