public class sfdc52Aclass {
  public Document attachment {
  get {
      if (attachment == null)
        attachment = new Document();
      return attachment;
    }
  set;
  }

  public PageReference upload() {

    attachment.AuthorId = UserInfo.getUserId();
    attachment.FolderId = '0037000000lFxcw'; // the record the file is attached to
    //attachment.IsPrivate = true;

    try {
      insert attachment;
    } catch (DMLException e) {
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
      return null;
    } finally {
      attachment = new Document(); 
    }

    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
    return null;
  }

}