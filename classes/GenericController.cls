public with sharing class GenericController {

    @AuraEnabled
    public static accountContactWrapper getAccountContactDetails() {
        
        String accountList = createQuery('Account');
        String contactList = createQuery('Contact');
        accountContactWrapper wrapperList = new accountContactWrapper(UtilsController.getSobjectList('Account', accountList),UtilsController.getSobjectList('Contact', contactList));
        return wrapperList;
    }
    
    public static String createQuery(String sObjectName) {
        Map<String, Schema.SObjectField> sObjectFieldMap = Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap();
        String fieldList = ' ';
        if(sObjectFieldMap != null) {
            for (String fieldName: sObjectFieldMap.keySet()) {
                if(!sObjectFieldMap.get(fieldName).getDescribe().getName().EqualsIgnoreCase('ID')){
                    fieldList = fieldList + ' , ' + sObjectFieldMap.get(fieldName).getDescribe().getName();
                }
            }
        }
        return fieldList;
    }
    
    public class accountContactWrapper {
        
        public List<Account> accountList {
            get;
            set;
        }
        public List<Contact> contactList {
            get;
            set;
        }
        public accountContactWrapper(List<Account> accList, List<Contact> conList) {
            this.accountList = accList;
            this.contactList = conList;
        }
        public accountContactWrapper() {
            
        }
    }
}