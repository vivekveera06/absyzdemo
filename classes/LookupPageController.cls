public with sharing class LookupPageController {
 
public Boolean render1 { get; set; }
    //public id Aid{get;set;}
    //public string AName{get;set;}
 
List<A__c> records=new List<A__c>();
 
public String searchvalue { get; set; }
 
public LookupPageController()
{
try
{
searchvalue=ApexPages.currentPage().getParameters().get('parentname');
String id=ApexPages.currentPage().getParameters().get('parentid');
 
if(String.IsNotBlank(searchvalue)){
render1=true;
records=[Select Name,fee__c,sampletext__c from A__c where Name like :+searchvalue+'%' order by Name asc];
}else
{
render1=true;
records=[Select Name,fee__c,sampletext__c from A__c order by Name asc];
 
}
}catch(Exception e)
{
}
}
 
public List<A__c> getRecords() {
if(records.size()!=0)
{
return records;
}else
{
return null;
}
}
 
public PageReference onkeyupAction() {
searchAction();
return null;
}
 
public PageReference searchAction() {
render1=true;
records=[Select Name,fee__c,sampletext__c from A__c where Name like :+searchvalue+'%' order by Name asc];
if(records.isEmpty())
{
ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error,'No Records Found'));
}
return null;
}
 
}