public class GoogleDriveController {
    private String code;
    
    public boolean val {
        get;
        set;
    }
    
    public blob file {
        get;
        set;
    }
    
    public String filetype {
        get;
        set;
    }
    public string refreshtoken;
    public String filename {get;set;}
    private string key = '1014215371149-g37digj8fckntvoajakuqgtbdbkd58kt.apps.googleusercontent.com';
    private string secret = '7-fI6WfOpZIRoigz9rbhY6z2';
    private string redirect_uri = 'https://vivekabsyz-dev-ed--c.ap5.visual.force.com/apex/GoogleDrive2'; 
    private String accesstoken;
    private Integer expiresIn;
    private String tokentype;
    public GoogleDriveController() {
        code = ApexPages.currentPage().getParameters().get('code');
        //Get the access token once we have code
        if (code != '' & code != null) {
            AccessToken();
        }
    }
    public PageReference DriveAuth() {
        //Authenticating
        PageReference pg = new PageReference(GoogleDriveAuthUri(key, redirect_uri));
        return pg;
    }
    
    public String GoogleDriveAuthUri(String Clientkey, String redirect_uri) {
        String key = EncodingUtil.urlEncode(Clientkey, 'UTF-8');
        String uri = EncodingUtil.urlEncode(redirect_uri, 'UTF-8');
        String authuri = '';
        authuri = 'https://accounts.google.com/o/oauth2/auth?' +
            'client_id=' + key +
            '&response_type=code' +
            '&scope=https://www.googleapis.com/auth/drive' +
            '&redirect_uri=' + uri +
            '&state=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx' +
            +
            'access_type=offline';
        return authuri;
    }
    public void UploadFile() {
        system.debug('filetype'+filetype);
		system.debug('filename-->'+filename);
        String boundary = '----------9889464542212';
        String delimiter = '\r\n--' + boundary + '\r\n';
        String close_delim = '\r\n--' + boundary + '--';
        system.debug('file-->'+file);
        String bodyEncoded = EncodingUtil.base64Encode(file);
        system.debug('bodyEncoded-->'+bodyEncoded);
        string folderid='1G8FcPXJnaoqqNlMp3JuWQtpeEKFJBdUA'; // Folder id of drive 
        //String body = delimiter + 'Content-Type: application/json\r\n\r\n' + '{ "title" : "' + filename + '",' + ' "mimeType" : "' + filetype + '" }' + delimiter + 'Content-Type: ' + filetype + '"parents":[{"id":"'+ folderid +'"}] }'+'\r\n' + 'Content-Transfer-Encoding: base64\r\n' + '\r\n' + bodyEncoded + close_delim;
        String body = delimiter+'Content-Type: application/json\r\n\r\n'+'{ "title" : "'+ filename+'",'+' "mimeType" : "'+ filetype+ '", "parents" : [{"id" : "'+folderId+'"}]}'+delimiter+'Content-Type: ' + filetype + '\r\n'+'Content-Transfer-Encoding: base64\r\n'+'\r\n'+bodyEncoded+close_delim;
       // String body = delimiter+'Content-Type: application/json\r\n\r\n'+'{ "parents" : [{"id" : "'+folderId+'"}]}'+delimiter+'\r\n'+'Content-Transfer-Encoding: base64\r\n'+'\r\n'+bodyEncoded+close_delim;
        System.debug('body' + body);
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://www.googleapis.com/upload/drive/v2/files?uploadType=multipart');
        req.setHeader('Authorization', 'Bearer ' + accessToken);
        req.setHeader('Content-Type', 'multipart/mixed; boundary="' + boundary + '"');
        req.setHeader('Content-length', String.valueOf(body.length()));
        req.setBody(body);
        req.setMethod('POST');
        req.setTimeout(60 * 1000);
        HttpResponse resp = http.send(req);
        string webContentLink;
        system.debug('resp final'+resp.getBody());
        string response=resp.getBody();
       	JSONParser parser = JSON.createParser(response);
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)) {
                String fieldName = parser.getText();
                system.debug('fieldName'+fieldName);
                parser.nextToken();
                if (fieldName == 'webContentLink') {
                    webContentLink = parser.getText();
                    
                }
            }
        } 
        system.debug('webContentLink-->'+webContentLink);
        file = null;
        filetype = '';
        filename = '';
        //change();
    }
    public void change(){
        
        Http http=new http();
        HttpRequest req = new HttpRequest();
         String boundary = '----------9889464542212';
        String delimiter = '\r\n--' + boundary + '\r\n';
        //STring endpoint='https://www.googleapis.com/drive/v2/files/'+'1G8FcPXJnaoqqNlMp3JuWQtpeEKFJBdUA'+'/permissions/vivek.kothalanka@absyz.com';
  		//String endpoint='https://www.googleapis.com/drive/v2/permissionIds/'+'vivekveera666@gmail.com';
       	string endpoint='https://www.googleapis.com/drive/v3/files/1INJtzisTd5fPgP-IIXFQNsMOsj3WCeVb8hnnoCgTWtI/permissions/17692751145983707565';
        req.setEndpoint(endpoint);
        req.setHeader('Authorization', 'Bearer ' + accessToken);
        req.setHeader('Content-Type', 'multipart/mixed; boundary="' + boundary + '"');
        string body='{"role": "reader"}';
        req.setBody(body);
        //req.setHeader('X-HTTP-Method-Override','PATCH');
       	// req.setMethod('PATCH');
        //req.setHeader('X-HTTP-Method-Override','PATCH');
		req.setMethod('POST');
        HttpResponse resp = http.send(req);
        string response=resp.getBody();
        system.debug('response-->'+response);
        //req.setBody(body);
    	
    }
    // To get Access token.
    /*    public static void renew_accesswith_refresh(){ 
        //renew access with refresh token every time a event is about to be created
        System.debug('inside renewing');
        String errorMessage ='';
        // String consumerKey='199214975867-n3hr994b3edfh94o6t5vb1ldnu698e9h.apps.googleusercontent.com'; 
        List<eventCreationClass__c> eventcreation = new List<eventCreationClass__c>();
        eventcreation = eventCreationClass__c.getAll().values();
        
        //finale String consumerKey='1085104463653-aadd4qq3d01m5kjgqosdecn9pj1r28uh.apps.googleusercontent.com';    
        String consumerKey=eventcreation[0].consumerKey__c;
        String clientSecret=eventcreation[0].clientSecret__c;
        //finale String clientSecret='tum44e_4vuqNGj7056LMzOkQ';
        //String clientSecret='Qwo31MBqbEGNMJ0hYTjrm_Xj'; 
        //client:983402858120-sneq9643vtfhdhns1iloo1bvb3q3ndn0.apps.googleusercontent.com
        //secret key:HYWtM9xTG0p3iPoFVXxtC7mi
        //uri to redirect after retrieving refresh token
        //String redirect_uri='https://absyzconnect--calender--c.cs72.visual.force.com/apex/Meetingvf?sfdc.tabName=01r5D0000004P0V';
        //finale  String redirect_uri='https://absyzconnect--preprod--c.cs57.visual.force.com/apex/Meetingvf?sfdc.tabName=01r0k0000004bLQ';
        String redirect_uri=eventcreation[0].redirect_uri__c;
        Http http = new Http();
        HttpRequest httpReq = new HttpRequest();
        HttpResponse httpRes = new HttpResponse();
        httpReq.setEndpoint('https://www.googleapis.com/oauth2/v4/token');
        httpReq.setMethod('POST');
        httpReq.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        
        List<GoogleCalenderInfo__c> googleSettingInfoList = new List<GoogleCalenderInfo__c>();
        googleSettingInfoList = GoogleCalenderInfo__c.getAll().values();
        String refreshToken;
        if(googleSettingInfoList.size() > 0 )
            refreshToken = googleSettingInfoList[0].Refresh_Token__c;
        //get refresh token from custom setting and use it to send http request
        System.debug('#### refreshToken '+refreshToken);
        String refreshTokenBody = 'client_id='+consumerKey+'&client_secret='+clientSecret+'&refresh_token='+refreshToken
            +'&grant_type=refresh_token';
        System.debug('#### refreshTokenBody '+refreshTokenBody);
        
        httpReq.setBody(refreshTokenBody);
        
        try{
            
            /*if (!Test.isRunningTest()){
                // resp = http.send(req);
                httpRes = http.send(httpReq);                    
            }
            else if(Test.isRunningTest()){
                MockHttpResponseGenerator res = new MockHttpResponseGenerator();
                httpRes=res.respond(httpReq);
            }
            
            // httpRes = http.send(httpReq); 
            if(httpRes.getStatusCode() == 200){              //200 status code is successful status
                System.debug('inside 200status of renewing');
                Map<String,object> TokenInfo = (Map<String,object>)JSON.deserializeUntyped(httpRes.getBody());
                GoogleCalenderInfo__c googleSettingInfo = googleSettingInfoList[0];
                googleSettingInfo.Access_Token__c = String.valueOf(TokenInfo.get('access_token'));
                googleSettingInfo.Expire_In_seconds__c = Double.valueOf(1.0);
                
                System.debug('do Refresh Token '+googleSettingInfo);
                update new List<GoogleCalenderInfo__c>{googleSettingInfo};
                    
                    }else{
                        errorMessage = 'Unexpected Error while communicating with Google Calendar API. '
                            +'Status '+httpRes.getStatus()+' and Status Code '+httpRes.getStatuscode();
                    }
        }catch(System.Exception e){
            
            System.debug('#### Exception Executed '+e.getStackTraceString() + ' '+e.getMessage());
            if(String.valueOf(e.getMessage()).startsWith('Unauthorized endpoint')){
                errorMessage = 'Unauthorize endpoint: An Administer must go to Setup -> Administer -> Security Control ->'
                    +' Remote Site Setting and add '+' '+ 'https://www.googleapis.com/oauth2/v4/token' +' Endpoint';
            }else{
                errorMessage = 'Unexpected Error while communicating with Google Calendar API. '
                    +'Status '+httpRes.getStatus()+' and Status Code '+httpRes.getStatuscode();
            }
        }
    }*/
    //To get Refresh Token.
    public void AccessToken() {
        //Getting access token from google
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint('https://accounts.google.com/o/oauth2/token');
        req.setHeader('content-type', 'application/x-www-form-urlencoded');
        String messageBody = 'code=' + code + '&client_id=' + key + '&client_secret=' + secret + '&redirect_uri=' + redirect_uri + '&grant_type=authorization_code';
        req.setHeader('Content-length', String.valueOf(messageBody.length()));
        req.setBody(messageBody);
        req.setTimeout(60 * 1000);
        Http h = new Http();
        String resp;
        HttpResponse res = h.send(req);
        resp = res.getBody();
        JSONParser parser = JSON.createParser(resp);
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)) {
                String fieldName = parser.getText();
                system.debug('fieldName'+fieldName);
                parser.nextToken();
                if (fieldName == 'access_token') {
                    accesstoken = parser.getText();
                }
                if(fieldName == 'refresh_token'){
                      refreshtoken=parser.getText();  
                }    
                else if (fieldName == 'expires_in') {
                    expiresIn = parser.getIntegerValue();
                } else if (fieldname == 'token_type') {
                    tokentype = parser.getText();
                }
            } }
        System.debug(' You can parse the response to get the access token ::: ' + resp);
    }
    
}