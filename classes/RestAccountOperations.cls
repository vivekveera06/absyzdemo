// This is a class for inserting/returning/deleting records from work bench or form any other org as well.
@RestResource(urlMapping='/RestAccountOperations')
Global class RestAccountOperations {
	@HTTPPost
    Global static id createAccount(string accname){
        
        Account a=new Account();
        a.name=accname;
        insert a;
        system.debug('inseted record'+a);
        return a.Id;
    }
    /*@HTTPGET
    Global static Account getAccount(){
        id accid=RestContext.request.params.get('accId');
        return[select id,name from Account where id=:accid];
    }*/
    @HTTPGET
    Global static list<Account> getAccount(){
        string searchstr=RestContext.request.params.get('str');
        //return[select id,name from Account where name like 'searchstr%'];
       	system.debug('database.query(searchstr)'+database.query(searchstr));
        return database.query(searchstr);
    }
    @HTTPDELETE
    Global static boolean DeleteAccount(){
        id accid=RestContext.request.params.get('accId');
        try{
            account a=[select id,name from Account where id=:accid];
			delete a;
			return true;            
        }
        catch(Exception e){
            return false;
        }
        
    }
    
}