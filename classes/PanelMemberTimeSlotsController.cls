/********************************************************************
* Company: Absyz
* Developer: Vivek
* Created Date: 22/06/2018
* Description: Accepting timeslots from the Interviewer.
* Test Class: PanelMemberTimeSlotsControllerTest
********************************************************************/
public class PanelMemberTimeSlotsController {
    
    public Boolean includeMyEvents {get;set;}
    public string currentRecordId;                  // This variable is used to get employee id from url.
    public boolean displayPopUp{get;set;}              // This variable is used to show or hide modal on button click.
    public list<calEvent> events {get;set;}              // calEvent Wrapper class variable.
    public list<TimeSlots> timeslotwrapper{get;set;}        // timeslotwrapper wrapper class variable.
    ///public date data{get;set;}
    public list<TimeSlots> timeslotswrapperedit{get;set;}		// to delete or edit list.
    public Preffered__c preferedslot{get;set;}
    public boolean errBoolean1{get;set;}              // This variable  is used show or hide modal.
    public string Message{get;set;}                  // This variable is used to diaplay error message in modal.
    public integer rowToDelete{get;set;}              // This variable is used hold rowindex of the row to be deleted.
    public static  integer i=1;                    // This variable is used to show row index in timeslots table.
    String dtFormat = 'EEE, d MMM yyyy HH:mm:ss ';          // This variable is used to dsiplay time in calender. 
    public boolean insertrecords;
    public boolean editDifferentDate{get;set;}		// newly added.
    public date startdate{get;set;}				// newly added
    public date enddate{get;set;}				// newly added
    public boolean emptylist{get;set;}			// newly added
    public boolean isnew{get;set;}				// newly added
    public string SaveLabel{get;set;}
    //constructor
    public PanelMemberTimeSlotsController() {
        
        //Default showing my events to on
        includeMyEvents = true;
        currentRecordId=Apexpages.currentPage().getParameters().get('id');
        displayPopUp=false;
        timeslotwrapper= new list<TimeSlots>();
        errBoolean1=false;
        editDifferentDate=false;
        isnew=false;
        SaveLabel='Save';
    }
    //*******************************************************************************************
    //Created by      : Vivek
    //Overall Purpose : This method is used for showing time so that interviewer can select.
    //InputParams     : NA
    //OutputParams    : List of times
    //*******************************************************************************************   
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('1 AM','1:00 AM'));
        options.add(new SelectOption('1:30 AM','1:30 AM'));
        options.add(new SelectOption('2 AM','2:00 AM'));
        options.add(new SelectOption('2:30 AM','2:30 AM'));
        options.add(new SelectOption('3 AM','3:00 AM'));
        options.add(new SelectOption('3:30 AM','3:30 AM'));
        options.add(new SelectOption('4 AM','4:00 AM'));
        options.add(new SelectOption('4:30 AM','4:30 AM'));
        options.add(new SelectOption('5 AM','5:00 AM'));
        options.add(new SelectOption('5:30 AM','5:30 AM'));
        options.add(new SelectOption('6 AM','6:00 AM'));
        options.add(new SelectOption('6:30 AM','6:30 AM'));
        options.add(new SelectOption('7 AM','7:00 AM'));
        options.add(new SelectOption('7:30 AM','7:30 AM'));
        options.add(new SelectOption('8 AM','8:00 AM'));
        options.add(new SelectOption('8:30 AM','8:30 AM'));
        options.add(new SelectOption('9 AM','9:00 AM'));
        options.add(new SelectOption('9:30 AM','9:30 AM'));
        options.add(new SelectOption('10 AM','10:00 AM'));
        options.add(new SelectOption('10:30 AM','10:30 AM'));
        options.add(new SelectOption('11 AM','11:00 AM'));
        options.add(new SelectOption('11:30 AM','11:30 AM'));
        options.add(new SelectOption('12 PM','12:00 PM'));
        options.add(new SelectOption('12:30 PM','12:30 PM'));
        options.add(new SelectOption('1 PM','1:00 PM'));
        options.add(new SelectOption('1:30 PM','1:30 PM'));
        options.add(new SelectOption('2 PM','2:00 PM'));
        options.add(new SelectOption('2:30 PM','2:30 PM'));
        options.add(new SelectOption('3 PM','3:00 PM'));
        options.add(new SelectOption('3:30 PM','3:30 PM'));
        options.add(new SelectOption('4 PM','4:00 PM'));
        options.add(new SelectOption('4:30 PM','4:30 PM'));
        options.add(new SelectOption('5 PM','5:00 PM'));
        options.add(new SelectOption('5:30 PM','5:30 PM'));
        options.add(new SelectOption('6 PM','6:00 PM'));
        options.add(new SelectOption('6:30 PM','6:30 PM'));
        options.add(new SelectOption('7 PM','7:00 PM'));
        options.add(new SelectOption('7:30 PM','7:30 PM'));
        options.add(new SelectOption('8 PM','8:00 PM'));
        options.add(new SelectOption('8:30 PM','8:30 PM'));
        options.add(new SelectOption('9 PM','9:00 PM'));
        options.add(new SelectOption('9:30 PM','9:30 PM'));
        options.add(new SelectOption('10 PM','10:00 PM'));
        options.add(new SelectOption('10:30 PM','10:30 PM'));
        options.add(new SelectOption('11 PM','11:00 PM'));
        options.add(new SelectOption('11:30 PM','11:30 PM'));
        options.add(new SelectOption('12 AM','12:00 AM'));
        options.add(new SelectOption('12:30 AM','12:30 AM'));
        return options;
    }      
    //*******************************************************************************************
    //Created by      : Vivek
    //Overall Purpose : This method is used for showing timeslots for selected Employee.
    //InputParams     : NA
    //OutputParams    : NA
    //*******************************************************************************************   
    public PageReference pageLoad() {
        
        events = new list<calEvent>();
        
        list<Preffered__c> meets=new list<Preffered__c>();
        meets=[select id,Name,End_Date_Time__c,Start_Date_Time__c,Employee__c,Available_Slots__c from Preffered__c where Employee__c=:currentRecordId ];
        system.debug('meets -->'+meets);
        
        for(Preffered__c currentslot:meets){
            system.debug('currentslot -->'+currentslot);
            if(currentslot.Available_Slots__c!=null){
                system.debug('available slots -->'+currentslot.Available_Slots__c);
                list<String> splittime=currentslot.Available_Slots__c.split(';');
                for( string s:splittime){
                    DateTime startDT;
                    DateTime endDT;
                    list<String> substringlist=s.split('-');
                    for(String currenttime:substringlist){
                        system.debug('currenttime -->'+currenttime);
                        integer value1;
                        integer i=1;
                        string currentvalue;
                        time temp_time;
                        if(currenttime.contains('PM')){
                            currentvalue=currenttime.removeEnd(' PM');
                            system.debug('currentvalue -->'+currentvalue);
                            if(currentvalue.contains(':')){
                                temp_time=time.newInstance(00,00,00,00).addhours(Integer.valueOf(currentvalue.left(1))+12).addminutes(Integer.valueOf(currentvalue.right(2)));
                            }
                            else{
                                temp_time =time.newInstance(00,00,00,00).addhours(Integer.valueOf(currentvalue)+12); 
                            }
                        }
                        else{
                            currentvalue=currenttime.removeEnd(' AM');
                            system.debug('currentvalue -->'+currentvalue);
                            if(currentvalue.contains(':')){
                                temp_time=time.newInstance(00,00,00,00).addhours(Integer.valueOf(currentvalue.left(1))).addminutes(Integer.valueOf(currentvalue.right(2)));
                            }
                            else{
                                temp_time=time.newInstance(00,00,00,00).addhours(Integer.valueOf(currentvalue));
                            }
                        }
                        if(i==1){								// to know whether we are at start string or end string
                            //time t=time.newInstance(00,00,00,00).addhours(value1);
                            time t=temp_time;
                            system.debug('t ->'+t);
                            startDT = datetime.newInstance(currentslot.Start_Date_Time__c.Year(),currentslot.Start_Date_Time__c.Month(), currentslot.Start_Date_Time__c.Day(),t.hour(),t.minute(),00);
                            system.debug('startdt -->'+startDT);
                            
                        }
                        if(i==2){
                            //time t=time.newInstance(00,00,00,00).addhours(value1);
                            time t=temp_time;
                            system.debug('t ->'+t);
                            endDT= datetime.newInstance(currentslot.End_Date_Time__c.Year(),currentslot.End_Date_Time__c.Month(), currentslot.End_Date_Time__c.Day(),t.hour(),t.minute(),00);
                            system.debug('endDT -->'+endDT);
                        }
                        i++;
                        system.debug('i -->'+i);
                    }
                    system.debug('end dt -->'+endDT);
                    calEvent slot = new calEvent();
                    slot.title = currentslot.Name + ' ';
                    slot.allDay = false;
                    slot.startString = startDT.format(dtFormat,'Asia/Kolkata');
                    //slot.endString = endDT.format(dtFormat,'Asia/Kolkata');
                    //slot.url=URL.getSalesforceBaseUrl().toExternalForm() + '/apex/CalendarPopup?id=' + currentslot.Id;
                    slot.url='/'+currentslot.id;
                    slot.className='Prefered-slot';
                    system.debug('slots -->'+slot);
                    events.add(slot);
                }
            }
        }
        //Get my Events if we have selected the correct option
        system.debug('events -->'+events);
        if(includeMyEvents){}
        return null;
    }
    //*******************************************************************************************
    //Created by      : Vivek
    //Overall Purpose : This method is used for showing modal on button click for Selecting slots.
    //InputParams     : NA
    //OutputParams    : NA
    //*******************************************************************************************   
    public void ShowModal(){
        displayPopUp=true;
        editDifferentDate=false;
        emptylist=false;
        SaveLabel='Save';
        isnew=true;
        TimeSlots t= new TimeSlots();
        t.index=1;
        
        timeslotwrapper.add(t);
        system.debug('timeslotwrapper  -->'+timeslotwrapper);
    }
    //*******************************************************************************************
    //Created by      : Vivek
    //Overall Purpose : This method is used for closing the modal on button click in modal.
    //InputParams     : NA
    //OutputParams    : NA
    //*******************************************************************************************   
    public void close(){
        displayPopUp=false;
        timeslotwrapper= new list<TimeSlots>();
        errBoolean1=false;
    }
    //*******************************************************************************************
    //Created by      : Vivek
    //Overall Purpose : This method is used for Adding new row on button click in modal.
    //InputParams     : NA
    //OutputParams    : NA
    //*******************************************************************************************   
    public void AddRow(){
        system.debug('timeslotwrapper -->'+timeslotwrapper);
        TimeSlots t= new TimeSlots();
        t.index= timeslotwrapper.size()+1;
        timeslotwrapper.add(t);
    }
    //*******************************************************************************************
    //Created by      : Vivek
    //Overall Purpose : This method is used saving timeslots on button click in modal.
    //InputParams     : NA
    //OutputParams    : NA	
    //*******************************************************************************************   
    public void save(){
        list<Preffered__c> Preffered_list=new list<Preffered__c>();
        system.debug('timeslotwrapper -->'+timeslotwrapper);
        system.debug('Wrapper size -->'+timeslotwrapper.size());
        insertrecords=true;
        list<TimeSlots> TimeSlotsList = new list<TimeSlots>();
        system.debug('timeslotwrapper -->'+timeslotwrapper);
        for(TimeSlots currentslot: timeslotwrapper){
            if(currentslot.startdate==null || currentslot.enddate==null || currentslot.startdate<system.today() || currentslot.enddate<system.today()){
                displayPopUp=true;
                insertrecords=false;
                errBoolean1=true; 
                Message='Invalid Date Range, Please check date Range';
            }
            else{
                integer days = currentslot.startdate.daysBetween(currentslot.enddate);
                system.debug('currentslot.startdate '+currentslot.startdate+'currentslot.enddate -->'+currentslot.enddate);
                system.debug('days -->'+days);
                string start_timeString=currentslot.starttime;
                string end_timeString=currentslot.endtime;
                time start_time;
                time end_time;
                if(start_timeString.contains(' AM')){
                    start_timeString=start_timeString.substring(0,start_timeString.length()-3);
                    if(start_timeString.contains(':')){
                        start_time=time.newInstance(00,00,00,00).addhours(Integer.valueOf(start_timeString.left(1))).addminutes(Integer.valueOf(start_timeString.right(2)));
                        system.debug('end_time -->'+end_time);
                    }
                    else{
                        start_time=time.newInstance(00,00,00,00).addhours(Integer.valueOf(start_timeString));
                        
                    }
                }
                else{
                    start_timeString=start_timeString.substring(0,start_timeString.length()-3);
                    if(start_timeString.contains(':')){
                        start_time=time.newInstance(00,00,00,00).addhours(Integer.valueOf(start_timeString.left(1))+12).addminutes(Integer.valueOf(start_timeString.right(2)));
                    }
                    else{
                        start_time=time.newInstance(00,00,00,00).addhours(Integer.valueOf(start_timeString)+12);
                    }
                }
                if(end_timeString.contains(' AM')){
                    end_timeString=end_timeString.substring(0,end_timeString.length()-3);
                    system.debug('end_timeString -->'+end_timeString);
                    if(end_timeString.contains(':')){
                        end_time=time.newInstance(00,00,00,00).addhours(Integer.valueOf(end_timeString.left(1))).addminutes(Integer.valueOf(end_timeString.right(2)));
                        system.debug('end_time -->'+end_time);
                    }
                    else{
                        end_time=time.newInstance(00,00,00,00).addhours(Integer.valueOf(end_timeString));
                    }
                }
                else{
                    end_timeString=end_timeString.substring(0,end_timeString.length()-3);
                    if(end_timeString.contains(':')){
                        end_time=time.newInstance(00,00,00,00).addhours(Integer.valueOf(end_timeString.left(1))+12).addminutes(Integer.valueOf(end_timeString.right(2)));
                    }
                    else{
                        end_time=time.newInstance(00,00,00,00).addhours(Integer.valueOf(end_timeString)+12);
                    }
                }
                system.debug('start_time -->'+start_time);
                system.debug('end time -->'+end_time);
                if(days<0){
                    displayPopUp=true;
                    insertrecords=false;
                    errBoolean1=true; 
                    Message='Invalid Date Range, Please check date Range';
                } 
                else if(start_time.hour()>=end_time.hour() && start_time.minute()>=end_time.minute()){
                    displayPopUp=true;
                    insertrecords=false;
                    errBoolean1=true;
                    Message='Invalid Time Range, Please check Time Range';
                }
                else if(days>0){
                    system.debug('days -->'+days);
                    date sdt = currentslot.startdate;
                    for(integer i=0; i<days; i++){
                        TimeSlots ts = new TimeSlots();
                        ts.startdate = sdt;
                        ts.enddate = sdt;
                        ts.starttime = currentslot.starttime;
                        ts.endtime = currentslot.endtime;
                        
                        TimeSlotsList.add(ts);
                        sdt=sdt+1;
                    }
                }
                else{
                    TimeSlotsList.add(currentslot);
                    system.debug('currentslot -->'+currentslot);
                    insertrecords=true;
                }
            }
        }
        system.debug('insertrecords -->'+insertrecords);
        if(insertrecords==true){
            list<Preffered__c> Preffered_lst= new list<Preffered__c>();
            map<Date, Preffered__c> datemap = new map<Date, Preffered__c>();
            for(TimeSlots currentslot:TimeSlotsList){
                if(currentslot.recordid==null){
                    if(datemap!=NULL && datemap.size()>0 && datemap.containsKey(currentslot.startdate)){
                        Preffered__c prefer = datemap.get(currentslot.startdate);
                        prefer.Available_Slots__c = prefer.Available_Slots__c+';'+currentslot.starttime+'-'+currentslot.endtime;
                        datemap.put(currentslot.startdate, prefer);
                    }
                    else{
                        Preffered__c newrecord=new Preffered__c();
                        date startdttime = currentslot.startdate;
                        date enddttime = currentslot.enddate;
                        newrecord.Start_Date_Time__c =startdttime;
                        newrecord.End_Date_Time__c=enddttime;
                        newrecord.Available_Slots__c=currentslot.starttime+'-'+currentslot.endtime;
                        newrecord.Employee__c=currentRecordId;
                        datemap.put(currentslot.startdate, newrecord);
                    }
                }
                else{
                    Preffered__c oldrecord= new Preffered__c();
                    oldrecord.id=currentslot.recordid;
                    date startdttime = currentslot.startdate;
                    date enddttime = currentslot.enddate;
                    oldrecord.Start_Date_Time__c =startdttime;
                    oldrecord.End_Date_Time__c=enddttime;
                    oldrecord.Available_Slots__c=currentslot.starttime+'-'+currentslot.endtime;
                    oldrecord.Employee__c=currentRecordId;
                    Preffered_lst.add(oldrecord);
                }
            }
            for(Preffered__c pf: datemap.values())
            {
                Preffered_lst.add(pf);
            }
            system.debug('Preffered_lst -->'+Preffered_lst);
            if(Preffered_lst.size()>0 && Preffered_lst!=null){
                displayPopUp=false;
                errBoolean1=false;
                //database.insert(Preffered_lst, false);
                database.upsert(Preffered_lst, false);
                
                system.debug('Preffered_lst -->'+Preffered_lst);
                
            }
        }
        else{
            displayPopUp=true;
        }
        
    }
    //*******************************************************************************************
    //Created by      : Vivek
    //Overall Purpose : This method is used deleting one row on button click in modal.
    //InputParams     : NA
    //OutputParams    : NA
    //*******************************************************************************************   
    public void DeleteRow(){
        list<TimeSlots> Temporarylist=new list<TimeSlots>();
        list<string> Deletelist=new list<string>();
        for(TimeSlots currentwrapper:timeslotwrapper){
            
            if(currentwrapper.index!=rowToDelete){
                currentwrapper.index=i;
                Temporarylist.add(currentwrapper);
                i++;
            }
            else if(currentwrapper.index==rowToDelete && currentwrapper.recordid!=null){
                Deletelist.add(currentwrapper.recordid);
            }
        }
        timeslotwrapper.clear();
        timeslotwrapper.addAll(Temporarylist);
        system.debug('Deletelist -->'+Deletelist);
        if(Deletelist.size()>0){
            database.delete(Deletelist,false);
        }
        system.debug('Deletelist -->'+Deletelist);
        system.debug('updated list-->'+timeslotwrapper);
    }
    public pagereference EditRecords(){
        SaveLabel='Update';
        list<Preffered__c> preferredslots;
        if(startdate!=null && enddate!=null){
            system.debug('startdate -->'+startdate);
            system.debug('enddate -->'+enddate);
            
            preferredslots=[select id,name,Employee__c,Available_Slots__c,Start_Date_Time__c,End_Date_Time__c from  Preffered__c where Employee__c=:currentRecordId  and Start_Date_Time__c>=:startdate and End_Date_Time__c<=:enddate];
            timeslotwrapper.clear();
            list<TimeSlots> timeslotwrapper=new list<TimeSlots>();
        }
        else{
            datetime endtime = datetime.newInstance(system.today(), time.newInstance(23, 59, 00,00));
            preferredslots=[select id,name,Employee__c,Available_Slots__c,Start_Date_Time__c,End_Date_Time__c from  Preffered__c where Employee__c=:currentRecordId and Start_Date_Time__c>=:system.today() and End_Date_Time__c<: system.today()+1];
        }
        integer i=1;
        system.debug('preferredslots -->'+preferredslots);
        for(Preffered__c currentslot:preferredslots){
            integer count=currentslot.Available_Slots__c.countMatches(';');
            if(count==0){
                TimeSlots t=new TimeSlots();
                t.index=i;
                t.startdate=currentslot.Start_Date_Time__c;
                t.enddate=currentslot.End_Date_Time__c;
                list<string> lst=currentslot.Available_Slots__c.split('-');
                for(string str:lst){
                    t.starttime=currentslot.Available_Slots__c.substringBefore('-');
                    t.endtime=currentslot.Available_Slots__c.substringAfter('-');
                }
                t.recordid=currentslot.Id;
                timeslotwrapper.add(t);
                i++;
            }
            else{
                list<string> individualslots=currentslot.Available_Slots__c.split(';');
                system.debug('individualslots -->'+individualslots);
                for(string current:individualslots){
                    TimeSlots t=new TimeSlots();
                    t.index=i;
                    t.startdate=date.newInstance(currentslot.Start_Date_Time__c.year(),currentslot.Start_Date_Time__c.month(),currentslot.Start_Date_Time__c.day());
                    t.enddate=date.newInstance(currentslot.End_Date_Time__c.year(),currentslot.End_Date_Time__c.month(),currentslot.End_Date_Time__c.day());
                    list<string> lst=currentslot.Available_Slots__c.split('-');
                    for(string str:lst){
                        t.starttime=currentslot.Available_Slots__c.substringBefore('-');
                        t.endtime=currentslot.Available_Slots__c.substringAfter('-');
                    }
                    /*t.starttime=current.left(4);
					t.endtime=current.right(4);*/
                    t.recordid=currentslot.Id;
                    timeslotwrapper.add(t);
                    i++;
                }
            }
            system.debug('timeslotwrapper -->'+timeslotwrapper);
            system.debug('timeslotwrapper -->'+timeslotwrapper.size());
        }
        if(timeslotwrapper.size()==0){
            emptylist=true;
        }
        isnew=false;
        displayPopUp=true;
        editDifferentDate=true;
        return null;
    }
    //*******************************************************************************************
    //Created by      : Vivek
    //Overall Purpose : This is wrapper class for taking inputs for timeslots.
    //*******************************************************************************************   
    public class TimeSlots{
        public integer index{get;set;}
        public date startdate{get;set;}
        public date enddate{get;set;}
        public String starttime{get;set;}
        public String endtime{get;set;}
        public string recordid{get;set;}
    }
    //*******************************************************************************************
    //Created by      : Vivek
    //Overall Purpose : This is wrapper class for showing timeslots on calender.
    //*******************************************************************************************   
    public class calEvent{
        
        public String title {get;set;}
        public Boolean allDay {get;set;}
        public String startString {get;private set;}
        public String endString {get;private set;}
        public String url {get;set;}
        public String className {get;set;}
    }
    
}