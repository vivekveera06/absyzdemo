global class SamplePptBatch implements Database.Batchable<sObject> {
    global DataBase.QueryLocator start(Database.BatchableContext BC){
    String query='select id,name from Account limit 5';
    return Database.getQueryLocator(query);
	}
	global void execute(Database.BatchableContext BC,List<Account>scope){
    	for(Account a:scope)
        {
            a.name=a.name+'Updated';
        }
        update scope;
    
	}
	global void finish(Database.BatchableContext BC){}
    	
}