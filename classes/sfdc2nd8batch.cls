global class sfdc2nd8batch implements Database.Batchable<sObject>{
    
    global database.QueryLocator start(database.BatchableContext bc){
   string query ='select id,name form Account order by createddate desc limit 5';
        return database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC,list<Account> scope){
        
        for(Account a:scope){
            a.rating='hot';
            update scope;
        }
    }
    global void finish(Database.BatchableContext bc){}
    
    

}