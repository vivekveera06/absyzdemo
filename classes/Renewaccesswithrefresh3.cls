global with sharing class Renewaccesswithrefresh3 {
    @remoteaction
    global static String renew(){ 
        //renew access with refresh token every time an attachment is about to be created
        System.debug('inside renewing');
        String errorMessage ='';
        String accesstoken='';
        List<Renewaccess__c> renewal = new List<Renewaccess__c>();
        renewal = Renewaccess__c.getAll().values();
        /*String consumerKey=renewal[0].consumerKey__c;
        String clientSecret=renewal[0].clientSecret__c;
        String refreshToken=renewal[0].refreshToken__c;
        String redirect_uri=renewal[0].redirecturi__c;*/
        
        String consumerKey='8519216663-lbmtq76hmiunr52ifj5gbqagccrgc61c.apps.googleusercontent.com';
        String clientSecret='JwN1wu6Df8Oj3GUmfHaai4mN';
        String refreshToken='1/u3q6pTcxAQWMlx_5XnkaFku047zrcmDLbDp_93QZJBM';
        String redirect_uri='https://vivekabsyz-dev-ed--c.ap5.visual.force.com/apex/GoogleDriveIntegration2';
        
        
        //uri to redirect after retrieving refresh token
        Http http = new Http();
        HttpRequest httpReq = new HttpRequest();
        HttpResponse httpRes = new HttpResponse();
        httpReq.setEndpoint('https://www.googleapis.com/oauth2/v4/token');
        httpReq.setMethod('POST');
        httpReq.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        //get refresh token from custom setting and use it to send http request
        System.debug('#### refreshToken '+refreshToken);
        String refreshTokenBody = 'client_id='+consumerKey+'&client_secret='+clientSecret+'&refresh_token='+refreshToken
            +'&grant_type=refresh_token';
        System.debug('#### refreshTokenBody '+refreshTokenBody);
        
        httpReq.setBody(refreshTokenBody);
        
        
        if (!Test.isRunningTest()){
            // resp = http.send(req);
            httpRes = http.send(httpReq);                    
        }
        
        //  httpRes = http.send(httpReq); 
        if(httpRes.getStatusCode() == 200){              //200 status code is successful status
            System.debug('inside 200status of renewing');
            Map<String,object> TokenInfo = (Map<String,object>)JSON.deserializeUntyped(httpRes.getBody());
            
            accesstoken=String.valueOf(TokenInfo.get('access_token'));
            
            
        }
        return accesstoken;
    }
}