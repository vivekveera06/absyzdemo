public with sharing class tstpopup {

    public Account ll { get; set; }

 public Boolean display { get; set; }
  public List<Account> Account { get; set; }
    public tstpopup(){
       Account=new List<Account>();  
       ll=new Account();
       Account=[select Name from Account];
    }

    public PageReference closePopup() {
        display=false;
        return null;
    }
    
  public PageReference pop() {
    string ids=apexpages.currentpage().getparameters().get('up');
    ll =[select Name,type,industry from Account  where id=:ids];
       display=true;
        return null;
    }

}