public class AccountListController {

    @AuraEnabled
  public static List<Account> getAccountDetails() {
     return [SELECT Id, Name,Phone,BillingStreet,Rating,AnnualRevenue,Type FROM Account ORDER BY createdDate desc limit 10];
  }
    @AuraEnabled
  public static List<Account> getAccountDetailsByName(string AccName) {
     String searchName='%'+AccName+'%';
     return [SELECT Id, Name,Phone,BillingStreet,Rating,AnnualRevenue,Type FROM Account where name like :searchName ORDER BY createdDate desc limit 10];
  }
    @AuraEnabled
    public static Account createAcc(string name, string state, string city, String type){
        Account a= new  account();
        a.Name= name;
        a.BillingState= state;
        a.BillingCity= city;
        a.Type=type;
        insert a;
        return a;
    }
    @AuraEnabled
    Public static list<string> accTypePicklist(){
        list<string> typepicklistvalues= new list<string>();
        schema.SObjectType objType= Account.getsobjecttype();
        schema.DescribeSObjectResult objDescribe= objType.getdescribe();
        map<string, schema.SObjectField> fieldMap= objDescribe.fields.getmap();
        list<schema.picklistentry> values= fieldMap.get('Type').getdescribe().getPickListValues();
        for(schema.picklistentry picklistval: values){
            typepicklistvalues.add(picklistval.getvalue());
        }
        return typepicklistvalues; 
    }
            @AuraEnabled
    public static list<Account> DeleteAccount(String acId){
        System.debug('acId '+acId);
        delete [select id from account where id=:acId];
        return [SELECT Id,Name,Industry FROM ACCOUNT  LIMIT 10];
    }
      @AuraEnabled
    public static list<contact> getcontactsofAccount(String acId){
        System.debug('acId '+acId);
        return [select id, name from contact where Accountid=:acId limit 5];
    }
}