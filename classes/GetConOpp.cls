public  with sharing class GetConOpp {
	@auraEnabled
    
    public static list<contact> getCon(string idcon){
        system.debug(idcon);
        return [select id,AccountId,LastName from contact where AccountId=:idcon ];
    }
    public static list<Opportunity> getOpp(string idopp){
        system.debug(idopp);
        return [select id,AccountId,Name from Opportunity where AccountId=:idopp];
    }
    
    
    
}