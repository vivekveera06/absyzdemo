public class Hack3Class {

    public list<sStudent> studentlist{get;set;}
    public id id1{get;set;}
       public Student__c  dep{get;set;}
    public List<Student__c> student2{get;set;}
    
    public Hack3Class(ApexPages.StandardController controller){
        student2 =new List<Student__c>();
        dep =new Student__c ();
        id1=apexPages.currentpage().getParameters().get('id');
        
    }
    public list<sStudent> getstudents(){
        if(studentlist==null)
        {
            studentlist=new list<sStudent>();
            for(Student__c ss:[select id,name,age__c,Department__c  from Student__c where Department__c=:id1]) 
                studentlist.add(new sStudent(ss));
            
        }
        
        return studentlist;
    }
    public PageReference processSelected(){
        
        List<Student__c> selectedstudents =new List<Student__c>();
        for(sStudent cCon:getstudents()){
            if(cCon.selected == true){
                
                selectedstudents.add(cCon.con);
            }
            
        }
        for(Student__c cc:selectedstudents){
        cc.Department__c=dep.Department__c;
        student2.add(cc);
        
   			 }
        update student2 ;
        
        return null;
    }
    
    
    
    
    public class sStudent
    {
        //public Student__c con{get;set;}
        public Student__c con{get;set;}
        public  Boolean selected{get;set;}
        
        public sStudent(Student__c c){
            
            con=c;
            selected=false;
        }
    }
}