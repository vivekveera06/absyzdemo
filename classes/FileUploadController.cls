public class FileUploadController {
    
    @AuraEnabled
    public static List<ContentDocumentLink>getAttachment(String ContactId) { 
        List<ContentDocumentLink> Documents =[SELECT Id, LinkedEntityId, ContentDocumentId, Visibility, IsDeleted,
                                              ContentDocument.Title, ContentDocument.FileType,ContentDocument.Description,
                                              ContentDocument.ContentSize
                                              FROM ContentDocumentLink 
                                              WHERE LinkedEntityId = :ContactId AND ContentDocument.FileType !='SNOTE'];
        return Documents;
    }
    
    @AuraEnabled
    public static boolean ImageDelete(String ContactId) { 
        ContentDocumentLink Documents =[SELECT Id, ContentDocumentId
                                              FROM ContentDocumentLink 
                                              WHERE LinkedEntityId = :ContactId AND ContentDocument.FileType !='SNOTE' 
                                        order by systemModstamp desc limit 1];
        list<ContentDocumentLink> doclist=[select id,ContentDocumentId 
                                           from ContentDocumentLink 
                                          where LinkedEntityId = :ContactId AND ContentDocument.FileType !='SNOTE' and id!=: Documents.id];
        //ContentDocument  content=[select id,Title from ContentDocument where id = :ContactId];
        
        Delete doclist;
        return true;
    }
    
    @AuraEnabled
    public static boolean AttachmentDelete(String AttachmentId) { 
        ContentDocument  content=[select id,Title from ContentDocument where id = :AttachmentId];
        Delete content;
        return true;
    }
    
    @AuraEnabled
    public static boolean AttachmentEdit(String AttachmentId,String fileTitle, String fileDiscription) { 
        ContentDocument  content=[select id,Title,Description from ContentDocument where id = :AttachmentId];
        content.Description = fileDiscription;
        content.Title = fileTitle;
        Update content;
        return true;
    }
    
    
}