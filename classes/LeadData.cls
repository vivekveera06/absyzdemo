public class LeadData {
	@AuraEnabled
    public static List<Lead> getLead(){
        list<lead> led = [select id,FirstName,LastName,Phone,Email,Company from Lead limit 10];
        return led;
    }
}