public class Deleteacc2 {
@AuraEnabled
    public static list<Account> getaccounts(){
        return[select id,Name,Type from Account where createddate=THIS_MONTH];
    }
@AuraEnabled
public static list<Account> deleteaccount(Account acc) {
    // Perform isDeletable() check here 
    delete acc;
   return[select id,Name,Type from Account where createddate=THIS_MONTH];
}

}