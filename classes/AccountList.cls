public class AccountList {

     
@AuraEnabled
  public static List<Account> getAccountDetails() {
     return [SELECT Id, Name,Phone,BillingStreet,Rating,AnnualRevenue,Type FROM Account ORDER BY createddate desc limit 10];
  }
@AuraEnabled
 public static List<contact> getAccountItems() {
    return [SELECT Id, FirstName, LastName FROM Contact limit 10];
 }

}