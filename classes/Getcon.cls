public with sharing class Getcon {
	@auraEnabled
    public static List<contact> getcontacts() {
     return [SELECT Id,FirstName,LastName FROM contact ORDER BY LastModifiedDate  desc limit 9];
  }
    
}