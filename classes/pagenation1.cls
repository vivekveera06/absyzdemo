public class pagenation1 {

     public String step1 {get;set;} 
    public String step2 {get;set; }
    public String step3 {get;set;}
    public String currentStep { get;private set;}
    
    public pagenation1() {
        this.currentStep = 'step1';
    }
    
    public PageReference didStep1() {
        this.currentStep = 'step2';
        return null;
    }
    
    public PageReference didStep2() {
        this.currentStep = 'step3';
        return null;
    }
    
    public PageReference didStep3() {
        this.currentStep = 'step4';
        return null;
    }
    
    public PageReference saveSteps() {
        // Do stuff to create object
        return new PageReference('/');
    }
}