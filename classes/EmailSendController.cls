public class EmailSendController {
    
    
    
    @AuraEnabled 
    public static void sendMailMethod(String mMail ,String mMailcc ,String mMailbcc ,String mSubject ,String mbody ,List<Id> mAttachment){
        
       // contact  contact1=[select id,email from contact limit 1];
        
        system.debug('inside method');

        
        List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();     
        
        // Step 1: Create a new Email
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        // Step 2: Set list of people who should get the email
        List<String> sendTo = new List<String>();
        if(mMail != null){
            for(string eachemail:mMail.split(','))
            {
                sendTo.add(eachemail);
            }
        }
        system.debug('Email Ids in To field: '+sendTo);
        //sendTo.add(mMail);
     //   mail.setTreatTargetObjectAsRecipient(false); 
       // mail.setTargetObjectId(contact1.Id);
        //mail.setSaveAsActivity(false);
        //mail.setWhatId(contact1.Id);
        
        mail.setToAddresses(sendTo);
        // Step 2a: Set list of people in bcc who should get the email
        List<String> sendTobcc = new List<String>();
        if(mMailbcc != null){
            for(string eachemailbcc:mMailbcc.split(','))
            {
                sendTobcc.add(eachemailbcc);
            }
        }
        system.debug('Email Ids in Bcc field: '+sendTobcc);
        mail.setBccAddresses(sendTobcc);
        
        // Step 2b: Set list of people in cc who should get the email
        List<String> sendTocc = new List<String>();
        if(mMailcc != null){
            for(string eachemailcc:mMailcc.split(','))
            {
                sendTocc.add(eachemailcc);
            }
        }
        system.debug('Email Ids in Cc field: '+sendTocc);
        mail.setCcAddresses(sendTocc);
        
        // Step 3: Set who the email is sent from
        mail.setReplyTo('vivek.kothalanka@absyz.com'); // sender's mail address.
        mail.setSenderDisplayName('vivek'); // sender's name. 
        
        // Step 4. Set email contents
        mail.setSubject(mSubject);
        mail.setHtmlBody(mbody);
        
        if(mbody == null){
            mail.setPlainTextBody(' ');
        }
        
        mail.setEntityAttachments(mAttachment);
        // Step 5. Add your email to the master list
        mails.add(mail);
        
        system.debug('sent'+mails);
        system.debug('attachments'+mAttachment);
        // Step 6: Send all emails in the master list
        Messaging.sendEmail(mails);
    }   
    
    /*
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Debasmita Rawooth          Dec-2017            Added method "getContentVersion" to fetch 
												the ContentVersionIds from database.
*/
    
    @AuraEnabled 
    public static List<Id> getContentVersion(List<Id> documentList){
        
        //database query to get the ContentVersionIds of the attachments
        List<ContentVersion> idList = [Select id from ContentVersion Where ContentDocumentId in :documentList];
        List<Id> docList = new List<Id>();
        For(ContentVersion c:idList){
            docList.add(c.Id);
        }
        system.debug('List'+docList);
        
        //return list of ContentVersionIds
        return docList;
    }
}