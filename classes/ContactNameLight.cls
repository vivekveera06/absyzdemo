public class ContactNameLight {
@AuraEnabled
    public static list<Contact> getContact(String n){
        //string a=n;
        return[select id,name,Email from contact where name=:n];
    }
}