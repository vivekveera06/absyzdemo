public class InsertAccount {

    
    @AuraEnabled
  public static List<Account> getAccountDetails() {
     return [SELECT Id, Name,Phone,BillingStreet,Rating,AnnualRevenue,Type FROM Account ORDER BY createdDate desc limit 10];
  }
    
    @AuraEnabled
    public static void contactRecord (Contact cont){
        
        try{
            System.debug('ContactController::createRecord::Contact'+cont);
            
            if(cont != null){
                insert cont;
            }
            
        } catch (Exception ex){
            
          }       
    }
    
    @AuraEnabled
    public static void createAccount (Account acc){
        
        try{            
            
            if(acc != null){
                insert acc;
            }
            
        } catch (Exception ex){
            
          }       
    }
    
    @AuraEnabled
    public static Account getAccountAndRelatedRecord(String acId){
        System.debug('acId '+acId);
        return [SELECT Id,Name,Industry,(select id,name from Opportunities),(select id,name from Contacts) FROM ACCOUNT where id=:acId LIMIT 1];
    }
    @AuraEnabled
    public static List<String> getAccountType(){
	List<String> options = new List<String>();
	Schema.DescribeFieldResult fieldResult = Account.Type.getDescribe();
	List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
	for (Schema.PicklistEntry f: ple) {
    options.add(f.getLabel());
	}

	return options;
    }
}