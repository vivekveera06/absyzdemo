@istest
public class LeadProcessorTest {

    public static testmethod void m(){
        list<lead> l=new list<lead>();
        for(integer i=0;i<200;i++){
            lead ls=new lead(firstname='a'+i,lastname='ai',company='absyz');
            l.add(ls);
        }
        
        
        insert l;
        test.startTest();
        LeadProcessor ld= new LeadProcessor();
        Database.executeBatch(ld);
        test.stopTest();
    }
    
}