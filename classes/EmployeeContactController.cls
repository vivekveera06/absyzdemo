public class EmployeeContactController {
    
    @AuraEnabled
    public static employeeContactWrapper getEmployeeContactDetails() {
        List<Employee__c> employee = new List<Employee__c>();
        List<Contact> contact = new List<Contact>();
        
        employee = [SELECT id, name FROM Employee__c LIMIT 1]; 
        contact = [SELECT id, name FROM Contact LIMIT 1];
        //hi team test
        employeeContactWrapper wrapperList = new employeeContactWrapper(employee[0], contact[0]);
        system.debug('wrapperList'+ wrapperList);
        return wrapperList;
    }
    
     @AuraEnabled
    public static List<Employee__c> getEmployeeDetails() {
        List<Employee__c> employeeList = new List<Employee__c>();
        
        employeeList = [SELECT id, name FROM Employee__c LIMIT 10];
        
        system.debug('employeeList'+ employeeList);
        return employeeList;
    }

    public class employeeContactWrapper {
        
        @AuraEnabled
        public Employee__c employee {
            get;
            set;
        }
        @AuraEnabled
        public Contact contact {
            get;
            set;
        }
        public employeeContactWrapper(Employee__c employee, Contact contact) {
            this.employee = employee;
            this.contact = contact;
        }
    }
}