@isTest
global class AccountProcessorTest {

    global static testmethod void AccountProcessorTest(){
        
       Account a=new Account();
        a.name='trail1';
        insert a;
        contact con=new Contact();
        con.lastname='lname';
        con.AccountId=a.id;
        insert con;
        list<id> accid=new list<id>();
        accid.add(a.Id);
        test.startTest();
        AccountProcessor.countContacts(accid);
        test.stopTest();
        
        Account acc=[select Number_of_Contacts__c  from Account where id=:a.Id limit 1];
        System.assertEquals(Integer.valueOf(acc.Number_of_Contacts__c),1);
    }
}