/*
* Reason: 		 To Connect to Drive.
* VF Page Name: GoogleDriveIntegration.
*/
public class DriveConnect {
    
    public string cid;
    public string authorizationcode;
    public string state;
    private final string googleClientID='8519216663-lbmtq76hmiunr52ifj5gbqagccrgc61c.apps.googleusercontent.com';
    private final string googleSecretCode='JwN1wu6Df8Oj3GUmfHaai4mN';
    private final string redirectURI='https://vivekabsyz-dev-ed--c.ap5.visual.force.com/apex/GoogleDriveIntegration';
    public string accessToken{get;set;}
    public string refreshToken{get;set;}
    public string acctoken { get; set;} 		// y?
    public boolean accheck {get; set;}			// y?
    private string ExpiresIn;
    private string tokenType;
    
    public DriveConnect(){
        accheck=false;
        cid=system.currentPageReference().getparameters().get('cid');
        system.debug('cid==>'+cid);
        authorizationcode=system.currentPageReference().getParameters().get('code');
        system.debug('authorizationcode==>'+authorizationcode);
        if(authorizationcode!=null){
            state=system.currentPageReference().getParameters().get('state');
            system.debug('state -->'+state);
            accesstoken='';
            getAccesToken();
            
        }
    }
    
    public pageReference doConnectIntegration(){
        
        PageReference pr = new PageReference('https://accounts.google.com/o/oauth2/auth' +
            '?response_type=code' +
            '&client_id=' + googleClientID +
            '&redirect_uri=' + redirectURI +
            '&scope=https://www.googleapis.com/auth/drive' +
            '&state=security_token%3D138r5719ru3e1%26url%3Dhttps://oa2cb.example.com/myHome&' +
            '&access_type=offline' +
            '&login_hint=xxxx@example.com');
            System.debug(pr);
        return pr;
    }
    private void getAccesToken(){
        
        http h =new http();
        
        httpRequest req=new httpRequest();
        string endpointvalue='https://accounts.google.com/o/oauth2/token';
        req.setEndpoint(endpointvalue);
        
        string bodyRequest='';
        bodyRequest='code='+EncodingUtil.urlEncode(authorizationCode, 'UTF-8');
        bodyRequest+='&client_id='+ EncodingUtil.urlEncode(googleClientID, 'UTF-8');
        bodyRequest+='&client_secret='+EncodingUtil.urlEncode(googleSecretCode, 'UTF-8');
        bodyRequest += '&redirect_uri=' + EncodingUtil.urlEncode(redirectURI, 'UTF-8');
        bodyRequest += '&grant_type=authorization_code';
        req.setBody(bodyRequest);
        req.setHeader('Content-length', string.ValueOf(bodyRequest.length()));
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        req.setMethod('POST');
        req.setTimeout(60*1000);
        HttpResponse res=h.send(req);
        map<string, string> jsonValues = new map<string, string>();
        
        System.debug('Response Value:'+res.getBody());
        jsonValues = parseJSONToMap(res.getBody());
        
        if(jsonValues.containsKey('Error')){
            
        }
        else{
            accesstoken=jsonValues.get('access_token');
            system.debug('accesstoken '+accesstoken);
            refreshToken =jsonValues.get('refresh_token');
            system.debug('refreshToken ->'+refreshToken);
            acctoken = refreshToken;
            if(accesstoken!=null)
                accheck=true;
            ExpiresIn=jsonValues.get('expires_in');
            tokenType = jsonValues.get('token_type'); 
            system.debug('refreshToken ->'+refreshToken);
            
        }
        
        
    }
    private map<String,string> parseJSONToMap(string jsonvalues){
        
        JSONParser parser=	JSON.createParser(jsonvalues);
        Map<string,string> jsonmap=new map<string,string>();
        string keyvalue='';
        string tempvalue='';
        while(parser.nextToken()!=null){
            if(parser.getCurrentToken()==JSONToken.FIELD_NAME){
                keyvalue=parser.getText();
                parser.nextToken();
                tempvalue=parser.getText();
                jsonmap.put(keyvalue,tempvalue);
            }
        }
        return jsonmap;
    }
    
}