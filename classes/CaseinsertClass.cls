public class CaseinsertClass {
    
    @AuraEnabled
    public static list<string> getCaseSubject(){
        
       List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = case.Subject.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry f: ple) {
            options.add(f.getLabel());
        }
        return options;
    }
    @AuraEnabled
    public static list<case> checkCase(case c){
        
        list<case> c1=[select id,Phone__c,Subject,Type,Status,Origin from case where Phone__c=:c.Phone__c limit 1];
        
        if(c1.size()>0){
            return c1;
        }
        else {
            return null;
            
        }
        

    }
    @AuraEnabled
    public static boolean  insertCase( case c){
        
        Database.SaveResult srList = Database.insert(c, false);
        
      
            if (srList.isSuccess()){
                return true;
            }
        else
            return false;
    }
    
}