public class StrikeContacts {

    @AuraEnabled
	public static list<Id> getContacts(){
        list<id> conids =new list<id>();
        list<contact> contacts=[select id,name from contact limit 15];
        for(Contact c:contacts){
            conids.add(c.Id);
        }
        return conids;
    }
}