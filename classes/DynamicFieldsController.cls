public class DynamicFieldsController {
    
    @AuraEnabled
    public static FieldSetForm getForm(Id recordId,List<String> fieldList) {
        String objectName = recordId.getsObjectType().getDescribe().getName();
        FieldSetForm form = new FieldSetForm();
        
        form.Fields = getFields(recordId, objectName,fieldList);
        //form.Fields = getFields(recordId, objectName, fieldSetName);
        form.Record = getRecord(recordId, objectName, form.Fields);
        
        return form;
    }
    @AuraEnabled
    public static void upsertRecord(SObject recordToUpsert) {
        upsert recordToUpsert;
    }
    private static List<Field> getFields(Id recordId, String objectName,List<String> fieldList ) {
       /* Schema.SObjectType objectType = null;
        
        if (recordId != null) {
            objectType = recordId.getSobjectType();
        }
        else if (String.isNotBlank(objectName)) {
            objectType = Schema.getGlobalDescribe().get(objectName);
        }*/
        
        /* Schema.DescribeSObjectResult objectDescribe = objectType.getDescribe();
		Map<String, Schema.FieldSet> fieldSetMap = objectDescribe.fieldSets.getMap();
		//Schema.FieldSet fieldSet = fieldSetMap.get(fieldSetName);
		List<Schema.FieldSetMember> fieldSetMembers = fieldSet.getFields();*/
        
        Map<String, Schema.SObjectType> global_describe = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> object_fields_map = global_describe.get(objectName).getDescribe().fields.getMap();
        
        list<string> fieldsApiNameList= new list<string>();
        list<string> fieldsNameList = new list<string>();
        list<Schema.DisplayType> fieldsTypeList = new list<Schema.DisplayType>();
        
        for (String fieldName: object_fields_map.keySet()) {
            if(fieldList.contains(object_fields_map.get(fieldName).getDescribe().getName())) {
                String apiName = object_fields_map.get(fieldName).getDescribe().getName();
                fieldsApiNameList.add(apiName);
                String label = object_fields_map.get(fieldName).getDescribe().getLabel();
                fieldsNameList.add(label);
                Schema.DisplayType type = object_fields_map.get(fieldName).getDescribe().getType();
                fieldsTypeList.add(type);
            } 
        }   
        
        List<Field> fields = new List<Field>();
        
        for(Integer i=0;i<fieldsNameList.size();i++){
            Field fieldInstance = new Field();
            fieldInstance.APIName=fieldsApiNameList[i];
            string strCheckboxType = string.valueOf(fieldsTypeList[i]);
            if(strCheckboxType == 'Boolean'){
                fieldInstance.type = 'checkbox';
            }
            else if(fieldsApiNameList[i].contains('Approver') || fieldsApiNameList[i].contains('Predefined_Approver')){
                fieldInstance.type = 'lookup';
            }
            else{
                fieldInstance.type = strCheckboxType;   
            }
            fieldInstance.Label= fieldsNameList[i];
            fields.add(fieldInstance);
        }
       
        /*for (Schema.FieldSetMember fsm : fieldSetMembers) {
			Field f = new Field(fsm);

			fields.add(f);
		}*/
        system.debug('fields---'+fields);
        
        return fields;
    }
    
    private static SObject getRecord(Id recordId, String objectName, List<Field> fields) {
        if (recordId == null) {
            Schema.SObjectType objectType = Schema.getGlobalDescribe().get(objectName);
            return objectType.newSObject();
        }
        
        List<String> fieldsToQuery = new List<String>();
        for (Field f : fields) {
            fieldsToQuery.add(f.APIName);
        }
        
        Schema.SObjectType objectType = recordId.getSobjectType();
        Schema.DescribeSObjectResult objectDescribe = objectType.getDescribe();
        String objectAPIName = objectDescribe.getName();
        
        String recordSOQL = 'SELECT ' + String.join(fieldsToQuery, ',') +
            '  FROM ' + objectAPIName +
            ' WHERE Id = :recordId';
        
        SObject record = Database.query(recordSOQL);
        
        system.debug('fields---'+record);
        
        return record;
    }
    
    public class FieldSetForm { 
        @AuraEnabled
        public List<Field> Fields { get; set; }
        
        @AuraEnabled
        public SObject Record { get; set; }
        
        public FieldSetForm() {
            Fields = new List<Field>();
        }
    }
}