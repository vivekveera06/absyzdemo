public class AccList {
@auraEnabled
    public static list<Account> getAccount(){
        
        List<Account> a=[select id,name,phone,industry,BillingCity,BillingState,BillingPostalCode,BillingCountry,owner.name from Account];
        //String Owner = [select name from user where id =: Userinfo.getUserId()].Name;
       //return Owner;
        return a;
    }
    @AuraEnabled 
    public static list<Account> getAcc(){
        
        return[select id,name,phone from Account order by createddate desc  limit 10];
    }
    @auraEnabled
    public static boolean InsertAcc(Account ac){
        
        Database.SaveResult srList = Database.insert(ac, false);
        if (srList.isSuccess()){
            
         return true;   
        }
        else{
            
            return false;
        }

    }
    @AuraEnabled
    public static boolean InsertCon(Contact cc){
        
        Database.SaveResult srList = Database.insert(cc, false);
        if (srList.isSuccess()){
            
         return true;   
        }
        else{
            
            return false;
        }
        
    }
    @AuraEnabled
    public static list<Account> getFiveRecords(){
        
        return [select id,name from Account order by createddate desc limit 5];
    }
     @AuraEnabled
    public static list<opportunity>  getRecords(){
         
        list<opportunity> opplist=[select AccountId,Account.name,Account.Industry,Account.NumberOfEmployees from opportunity where AccountId!=null];
        return opplist;
        
    }
   
    
       
}