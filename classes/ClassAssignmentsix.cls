public class ClassAssignmentsix {
	public map<decimal,Merchandise__c> getRecords()
    {
        
        map<decimal,Merchandise__c>  mapcollection=new map<decimal,Merchandise__c> ();
        for(Merchandise__c merRec:[select id,name,Quantity__c,Price__c from Merchandise__c])
        {
            mapcollection.put(merRec.Quantity__c,merRec);
        }
        return mapcollection;
    }
    public void updateMerRecords(map<decimal,Merchandise__c> mapcollection)
    {
        List<Merchandise__c> lstRec=new List<Merchandise__c>();
        for(Merchandise__c mer:mapcollection.values())
        {
            lstRec.add(mer);
        }
        List<Merchandise__c> updateRecs=new  List<Merchandise__c>();
        for(integer i=0;i<lstRec.size();i++)
        {
				Merchandise__c merobj=lstRec[i];
            
            if(math.mod(i, 2)==0)
            {
                merobj.Quantity__c=100;
            }
            else{
                merobj.Quantity__c=200;
        }
        updateRecs.add(merobj);
        
        
        
        
    }
    update updateRecs;
    
}
}