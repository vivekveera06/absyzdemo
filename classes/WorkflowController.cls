public class WorkflowController {
    
    @AuraEnabled(cacheable=true)
    public static list<Workflow1__c> getCustomSettinglist(){
        return [select id,Name__c,Category__c,IsChinaUser__c,Favourites__c,Number_Field__c,Start_From__c,Predefined_Approver_01__c,Predefined_Approver_01_Role__c from Workflow1__c  ];
    }
    
    //lookup controller
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String ObjectName) {
        system.debug('ObjectName-->' + ObjectName);
        String searchKey = searchKeyWord + '%';
        
        List < sObject > returnList = new List < sObject > ();
        
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  'select id, Name from ' +ObjectName + ' where Name LIKE: searchKey order by createdDate DESC limit 5';
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
    
    @AuraEnabled
    public static list<WorkFlowWrapper> getGenericFields(List<String> fieldList,sObject SingleWorkflowrecord ){
        
        /*  Code From Here.*/
        String objectName = SingleWorkflowrecord.Id.getsObjectType().getDescribe().getName();
        system.debug('fieldList--'+fieldList);
        
        /* Maps and lists used */
        Map<String, Schema.SObjectType> global_describe = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> object_fields_map = global_describe.get(objectName).getDescribe().fields.getMap();
        system.debug('object_fields_map'+object_fields_map);
        Map<String,String> valueMap = new Map<String,String>();
        
        list<string> fieldsApiNameList= new list<string>();
        list<string> fieldsNameList = new list<string>();
        list<Schema.DisplayType> fieldsTypeList = new list<Schema.DisplayType>();
        list<String> matadataList = new list<String>();
        
        system.debug('SingleWorkflowrecord-->'+SingleWorkflowrecord.get('Name__c'));
        
        list<CustomSettingFields__mdt> MetadatafieldsList = [select id,FieldApiName__c,Required__c,MetadataForCustomSetting__c,Section__c from CustomSettingFields__mdt where MetadataForCustomSetting__r.WorkflowName__c=:(String)SingleWorkflowrecord.get('Name__c')];
        
        system.debug('fieldsList--->'+MetadatafieldsList);
        system.debug('matadataList --->'+matadataList);
        system.debug('object_fields_map-->'+object_fields_map);
        
        
        Map<String, Object> fieldsToValue = SingleWorkflowrecord.getPopulatedFieldsAsMap();
        system.debug('fieldsToValue---'+fieldsToValue);
        for (String fieldName : fieldsToValue.keySet()){
            //System.debug('field name is ' + fieldName + ', value is ' +
            //                         fieldsToValue.get(fieldName));
            valueMap.put(fieldName,string.valueOf(fieldsToValue.get(fieldName)));
        }
        system.debug('valueMap--'+valueMap);
        
        
        list<WorkFlowWrapper> WorkFlowWrapperList = new List<WorkFlowWrapper>();
        /* For Creating Wrapper Class*/
        for(CustomSettingFields__mdt metadata: MetadatafieldsList){
            
            
            system.debug('split****'+metadata.FieldApiName__c.split(','));
            for(string objApiName : metadata.FieldApiName__c.split(',')){
                WorkFlowWrapper wrapperInstance = new WorkFlowWrapper();
                wrapperInstance.apiName = objApiName;
                wrapperInstance.required = metadata.Required__c;
                wrapperInstance.section = metadata.Section__c;
                wrapperInstance.objectName = objectName ;
                system.debug('objApiName-----'+objApiName);
                if(object_fields_map.containsKey(objApiName)){
                    wrapperInstance.value = valueMap.get(objApiName); 
                    if(wrapperInstance.value=='true'){
                        wrapperInstance.booleabattribute=true;
                    }
                    else{
                        wrapperInstance.booleabattribute=false; 
                    }
                }
                wrapperInstance.label = object_fields_map.get(objApiName).getDescribe().getLabel();
                wrapperInstance.type = String.valueOf(object_fields_map.get(objApiName).getDescribe().getType());
                
                system.debug(' wrapperInstance.label  ---'+ wrapperInstance.label );
                if( wrapperInstance.type == 'Boolean'){
                    wrapperInstance.type = 'checkbox';
                }
                else if( wrapperInstance.type == 'DOUBLE'){
                    wrapperInstance.type = 'number';
                }
                if(wrapperInstance.label.contains('Approver') || wrapperInstance.label.contains('Predefined_Approver')){
                    wrapperInstance.type = 'lookup';
                }
                system.debug('wrapperInstance---'+wrapperInstance);
                WorkFlowWrapperList.add(wrapperInstance);
            }
        }
        system.debug('WorkFlowWrapperList--->'+WorkFlowWrapperList);
        
        return WorkFlowWrapperList;    
    }
    
    @AuraEnabled
    public static string saveRecord(id recordId, string fields){
        system.debug('fields'+fields);
        system.debug('fields'+recordId);
        String objectName = recordId.getsObjectType().getDescribe().getName();
        list<WorkFlowWrapper> record = new list<WorkFlowWrapper>();
        try{
            record = (list<WorkFlowWrapper>)System.JSON.deserialize(fields, list<WorkFlowWrapper>.class);
        } catch(Exception e) {
            WorkFlowWrapper warp = (WorkFlowWrapper)JSON.deserialize(fields, WorkFlowWrapper.class);
            record.add(warp);
        }
        system.debug('record--'+record);
        sObject workflowObject = recordId.getSObjectType().newSObject(recordId) ;
        system.debug('workflowObject--'+workflowObject);
        for(integer i = 0;i<record.size();i++){
            system.debug('record[i].type--'+record[i].type);
            if(record[i].type == 'DATE') {
                Date startDate = Date.valueOf(record[i].value);
                system.debug('startDate****'+startDate);
                system.debug('api name****'+record[i].apiName);
                workflowObject.put( record[i].apiName, startDate) ;
            }
            else if(record[i].type == 'checkbox') {
                Boolean booleanvalue = Boolean.valueOf(record[i].value);
                system.debug('booleanvalue--'+booleanvalue);
                workflowObject.put( record[i].apiName, booleanvalue) ;
            }
            else if(record[i].type == 'number') {
                integer Decimalvalue = integer.valueOf(record[i].value);
                system.debug('Decimalvalue--'+Decimalvalue);
                workflowObject.put( record[i].apiName, Decimalvalue) ;
            }
            else {
                workflowObject.put( record[i].apiName, record[i].value);
            }
            
        }
        system.debug('workflowObject--'+workflowObject);
       // update workflowObject ;
        Database.SaveResult result = Database.update(workflowObject, false);
        system.debug('result -->'+result.isSuccess());
        if(result.isSuccess()){
           
            return 'true';
        }
        else{
            for(Database.Error error : result.getErrors()){
                return error.getMessage();
            }
            return 'false';
        }        
    }
    
    public class fieldWrapper{
        
        @AuraEnabled
        public string name{ get;set;}
        @AuraEnabled
        public string apiName{ get;set;}
        @AuraEnabled
        public string value{get;set;}
        @AuraEnabled
        public String type{get;set;}
        @AuraEnabled
        public string objectName{ get;set;}
        public fieldWrapper(String name, String apiname,String value, String stringType, String objectName){
            this.name=name;
            this.apiName=apiname;
            this.value=value;
            this.type=stringType;
            this.objectName=objectName;
            
        }
        
        public fieldWrapper() {}
    }
    
    
    Public class WorkFlowWrapper{
        
        @AuraEnabled public String label{get;set;}
        @AuraEnabled public String apiName{get;set;}
        @AuraEnabled public boolean required{get;set;} 
        @AuraEnabled public String section{get;set;} 
        @AuraEnabled public String type{get;set;} 
        @AuraEnabled public boolean booleabattribute{get;set;}
        @AuraEnabled public String objectName{get;set;} 
        @AuraEnabled public String value{get;set;} 
        
        public WorkFlowWrapper(String label, String apiName, boolean required, String section, String type,string objectName, String value){
            this.label = label;
            this.apiName = apiName;
            this.required = required;
            this.section = section;
            this.type = type;
            this.objectName=objectName;
            this.value=value;
        }
        
        public  WorkFlowWrapper() {}
    }
}