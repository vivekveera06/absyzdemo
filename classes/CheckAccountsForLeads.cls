public class CheckAccountsForLeads {

    public static void getAccounts(list<lead> leads){
        list<Account> acc;
        string teamid;
        string leadid;
        for(lead l:leads){
            teamid=l.Team_id__c;
            leadid=l.Id;
        }
        if(teamid!=null){
            acc=[select id,Team_idA__c from Account where Team_idA__c=:teamid];
         }
       
        if(acc.size()>0){
            for(Account a:acc){
            Database.LeadConvert lc = new Database.LeadConvert();
            lc.setLeadId(leadid);
			lc.setAccountId(a.id);
            lc.setDoNotCreateOpportunity(true);
            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
			lc.setConvertedStatus(convertStatus.MasterLabel);
            database.LeadConvertResult lcr=database.convertLead(lc);
            
        	}
          }
       }    
}