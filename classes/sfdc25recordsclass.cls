public class sfdc25recordsclass {
	public String Sobjects { get; set; } 
    public list<SelectOption> selectRecords { get; set; }
    public String Records { get; set; }
    map<String, Schema.SobjectType> so = Schema.getGlobalDescribe();
     public List<SelectOption> getSelectObjects() {
        List<String> S = new List<String>(so.keyset());
        List<SelectOption> op = new List<SelectOption>();
        for(String S1:S){
            op.add(new SelectOption(S1,S1));
            op.sort();
        }
        return op;
        
    }
     public PageReference selRecords() {
        String str='select '+'name'+' from '+Sobjects;
        List<SObject> sob=Database.query(str);
        selectRecords=new list<SelectOption>();
        for(SObject gf:sob){
            if(gf.get('name')!=NULL)
                selectRecords.add(new SelectOption(String.valueOf(gf.get('name')),String.valueOf(gf.get('name'))));
        }       
        return null;
    
    
}
}