global class ClassBatchOne implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        String query='select id,name from Account';
        return Database.getQueryLocator(query);
    }
	global void execute(Database.BatchableContext bc,List<Account> scope)
    {
        for(Account a:scope)
        {
            a.name='ABSYZ'+a.name;
        }
        update scope;
    }
    global void finish(Database.BatchableContext bc)
    {
        
    }
}
/*global class EveryDayTen implements schedulable
{
    global void execute(Schedulable Context sc)
    {
        ClassBatchOne b=new ClassBatchOne();
        String sch='0 20 6';
        String jobid=system.schedule('schedule at ten',sch,b);
        Datebase.executeBatch(b,2);
    }
    
}*/