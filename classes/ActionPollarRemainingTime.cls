public class ActionPollarRemainingTime {

    // property to hold number of time refreshed
    public Integer timesRefreshed { get; set; }

    // constructor
    public ActionPollarRemainingTime() {
        timesRefreshed = 0;
    }

    // incrementing the counter
    public void incrementTimesRefreshed() {
        
        timesRefreshed++;
        system.debug(timesRefreshed);
    }
}