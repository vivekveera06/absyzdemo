public class Deleteacc {
	  @AuraEnabled
    public static list<Account> getaccounts(){
        return[select id,Name,Type from Account limit 10 ];
    }
@AuraEnabled
public static Account deleteaccount(Account acc) {
    // Perform isDeletable() check here 
    delete acc;
    return acc;
}

}