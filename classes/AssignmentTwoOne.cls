public class AssignmentTwoOne {
	public Map<Merchandise__c,list<Line_Items__c>> method()
    {
        Map<Merchandise__c,list<Line_Items__c>> mapobj=new Map<Merchandise__c,list<Line_Items__c>>();
        for(Merchandise__c mer:[select id,name,(select id,name from Line_Item__r) from Merchandise__c] )
        {
            mapobj.put(mer,mer.Line_Item__r);
        }
        System.debug(mapobj);
        return mapobj;
        
    }
}