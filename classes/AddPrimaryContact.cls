public class AddPrimaryContact  implements Queueable {
    
    public contact con;
    public string state;
    public AddPrimaryContact(contact con,string state){
        
        this.con=con;
       this.state=state;
    }
    public void execute(QueueableContext context)
    {
        List<Account> ListAccount = [SELECT ID, Name ,(Select id,FirstName,LastName from contacts ) FROM ACCOUNT WHERE BillingState = :state LIMIT 200];
        
        list<contact> lcon=new list<contact>();
        for(Account a:ListAccount){
            contact cont=con.clone(false,false,false,false);
            cont.AccountId=a.id;
            lcon.add(cont);
        }
        if(lcon.size()>0)
            insert lcon;
        
    }

}