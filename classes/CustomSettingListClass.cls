public class CustomSettingListClass {

    @AuraEnabled(cacheable=true)
    public static list<Workflow1__c> CustomSettinglist(){
        return [select id,Name__c,Category__c,IsChinaUser__c from Workflow1__c  ];
    }
}