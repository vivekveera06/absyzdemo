public class InsertMultipleAcc {
    @auraEnabled
    public static list<account> saveaccounts(list<account> acclist){
        insert acclist;
        return acclist;
    }
}