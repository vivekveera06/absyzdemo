public class Wrapperclass {

    @AuraEnabled
    public static list<wrapperClass1> getdata(string contactid){
        
        //list<wrapperClass1> returnwrapperlist= new list<wrapperClass1>();
        list<Attachment> attlist=[select id,name from Attachment where ParentID=:contactid];
        list<Note> notelist=[select id,title from note where ParentId=:contactid];
        
        // to get all except for notes 
        list<ContentDocumentLink> attchments=[select id,LinkedEntityId,ContentDocumentId,
                                             Visibility,IsDeleted,ContentDocument.Title,
                                             ContentDocument.FileType,ContentDocument.Description,
                                             ContentDocument.ContentSize
                                             from ContentDocumentLink where LinkedEntityId =:contactid  and ContentDocument.FileType !='SNOTE'];
        list<ContentDocumentLink> lnotes=[select id,LinkedEntityId,ContentDocumentId,
                                         	Visibility,IsDeleted,ContentDocument.Title,
                                          	ContentDocument.FileType,ContentDocument.Description,
                                            ContentDocument.ContentSize
                                            from ContentDocumentLink where LinkedEntityId=:contactid and ContentDocument.FileType='SNOTE'
                                          ];
        
        list<wrapperClass1> wraps=new list<wrapperClass1>();
        
        for(ContentDocumentLink d:attchments){
            wrapperClass1 newclass=new wrapperClass1();
            newclass.id1=d.Id;
            newclass.type='attl';
            newclass.name=d.ContentDocument.title;
            newclass.button=false;
            wraps.add(newclass);
            
        }
        for(ContentDocumentLink n:lnotes){
             wrapperClass1 newclass=new wrapperClass1();
             newclass.id1=n.Id;
            newclass.type='notel';
            newclass.name=n.ContentDocument.title;
            newclass.button=false;
            wraps.add(newclass);
        }
        for(Attachment at:attlist){
            wrapperClass1 newclass=new wrapperClass1();
            newclass.id1=at.Id;
            newclass.type='att';
            newclass.name=at.name;
            newclass.button=false;
            wraps.add(newclass);
            
        }
         for(Note nt:notelist){
            wrapperClass1 newclass=new wrapperClass1();
            newclass.id1=nt.Id;
            newclass.type='note';
            newclass.name=nt.title;
            newclass.button=false;
            wraps.add(newclass);
            
        }
        
        return wraps;
    } 
    
    @AuraEnabled
    public static boolean delete1( string rid,string Typevalue){


		system.debug('rid'+rid+'TypeValue'+Typevalue);        
        if(Typevalue=='attl' || Typevalue=='notl'){
            list<ContentDocumentLink> d=[select id from ContentDocumentLink where id=:rid];
            system.debug(d);
            delete d;
            return true;
        }
        else if(Typevalue=='att'){
            list<Attachment> d=[select id from Attachment where id=:rid];
            system.debug(d);
            delete d;
            return true;
        }
        else {
            list<Note> d=[select id from note where id=:rid];
            delete d;
            system.debug(d);
            return true;
        }
          
    }
    public class wrapperClass1{
        
        @AuraEnabled public string id1{get;set;}
        @AuraEnabled public string type{get;set;}
        @AuraEnabled public string name{get;set;}
        @AuraEnabled public boolean button{get;set;}
        
        public wrapperClass1(){
            
        }
        
    }
}