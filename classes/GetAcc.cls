public with sharing  class GetAcc {

    @AuraEnabled
  public static List<Account> getAccountDetails() {
     return [SELECT Id, Name,Phone,BillingStreet,Rating,AnnualRevenue,Type FROM Account ORDER BY createddate desc limit 10];
  }
}