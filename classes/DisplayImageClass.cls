public class DisplayImageClass {

    /*@AuraEnabled
    public static string getPhoto(string cid){
        
        ContentDocumentLink doc=[select id,LinkedEntityId from ContentDocumentLink 
                                  where LinkedEntityId = :cid and ContentDocument.FileType !='SNOTE' limit 1];
        
        
        ContentDocument dc=[select id from ContentDocument where id=:doc.ContentDocumentId];
       	string image=dc.id;
        system.debug('image'+dc);
        return image;
        
    }*/
    @AuraEnabled
	public static string getImage(String ContactId) { 
        system.debug('Contact Id'+ContactId);
        ContentDocumentLink Documents =[SELECT Id, LinkedEntityId, ContentDocumentId, Visibility, IsDeleted,
                                              ContentDocument.Title, ContentDocument.FileType,ContentDocument.Description,
                                              ContentDocument.ContentSize
                                              FROM ContentDocumentLink 
                                              WHERE LinkedEntityId = :ContactId AND ContentDocument.FileType !='SNOTE' limit 1];
        ContentDocument dc = [select id from ContentDocument where id =:Documents.ContentDocumentId];
        system.debug('Document'+dc);
        string imageid = dc.id;
        return imageid;
    }
}