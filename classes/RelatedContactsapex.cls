public class RelatedContactsapex {
	@AuraEnabled
    public Static List<Contact> getContacts(String acctId){
        system.debug('accountid id'+acctId);
        List<Contact> con = [select Id, LastName,AccountId from Contact where AccountID=:acctId];
        return con;
    }
}