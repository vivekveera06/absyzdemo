/********************************************************************
* Company: Absyz
* Developer: Vivek
* Created Date: 22/06/2018
* Class Name: PanelMemberTimeSlotsController. 
********************************************************************/
@isTest
public class PanelMemberTimeSlotsControllerTest {
   /* 
    Public static testmethod void showModalTestMethod(){
        PanelMemberTimeSlotsController testinstance =new PanelMemberTimeSlotsController();
        testinstance.getItems();
        testinstance.ShowModal();
        testinstance.close();
        testinstance.AddRow();
        testinstance.DeleteRow();
        testinstance.save();
        PanelMemberTimeSlotsController.TimeSlots  timeslotsInstance =new PanelMemberTimeSlotsController.TimeSlots(); 
        timeslotsInstance.enddate=system.today();
        timeslotsInstance.startdate=system.today();
        timeslotsInstance.starttime='';
        timeslotsInstance.endtime='';
        PanelMemberTimeSlotsController.calEvent callEventInstance =new  PanelMemberTimeSlotsController.calEvent();
        callEventInstance.allDay=true;
        callEventInstance.title='';
        callEventInstance.className='';
        callEventInstance.title='';
        callEventInstance.url='';
    }
    Public static testmethod void insertSlotsDifferentDaysMethod(){
        PanelMemberTimeSlotsController testinstance =new PanelMemberTimeSlotsController();
        PanelMemberTimeSlotsController.calEvent callEventInstance =new  PanelMemberTimeSlotsController.calEvent();
        callEventInstance.allDay=true;
        callEventInstance.title='';
        callEventInstance.className='';
        callEventInstance.title='';
        callEventInstance.url='';
        
        PanelMemberTimeSlotsController.TimeSlots t =new PanelMemberTimeSlotsController.TimeSlots();
        t.startdate=system.today().adddays(1);
        t.enddate=system.today().adddays(3);
        t.starttime='7 PM';
        t.endtime='11 PM';
        testinstance.timeslotwrapper.add(t);
        system.debug('  testinstance.timeslotwrapper -->'+ testinstance.timeslotwrapper);
        testinstance.ShowModal();
        testinstance.save();
    }
    Public static testmethod void insertSlotsSameDayMethodTest1(){
        PanelMemberTimeSlotsController testinstance =new PanelMemberTimeSlotsController();
        PanelMemberTimeSlotsController.calEvent callEventInstance =new  PanelMemberTimeSlotsController.calEvent();
        callEventInstance.allDay=true;
        callEventInstance.title='';
        callEventInstance.className='';
        callEventInstance.title='';
        callEventInstance.url='';
        
        PanelMemberTimeSlotsController.TimeSlots t =new PanelMemberTimeSlotsController.TimeSlots();
        t.startdate=system.today().adddays(1);
        t.enddate=system.today().adddays(3);
        t.starttime='1 AM';
        t.endtime='11 PM';
        testinstance.timeslotwrapper.add(t);
        system.debug('  testinstance.timeslotwrapper -->'+ testinstance.timeslotwrapper);
        testinstance.ShowModal();
        testinstance.save();
    }
    Public static testmethod void insertSlotsSameDayMethodTest2(){
        PanelMemberTimeSlotsController testinstance =new PanelMemberTimeSlotsController();
        PanelMemberTimeSlotsController.calEvent callEventInstance =new  PanelMemberTimeSlotsController.calEvent();
        callEventInstance.allDay=true;
        callEventInstance.title='';
        callEventInstance.className='';
        callEventInstance.title='';
        callEventInstance.url='';
        
        PanelMemberTimeSlotsController.TimeSlots t =new PanelMemberTimeSlotsController.TimeSlots();
        t.startdate=system.today().adddays(1);
        t.enddate=system.today().adddays(3);
        t.starttime='1 AM';
        t.endtime='11 AM';
        testinstance.timeslotwrapper.add(t);
        system.debug('  testinstance.timeslotwrapper -->'+ testinstance.timeslotwrapper);
        testinstance.ShowModal();
        testinstance.save();
    }
    Public static testmethod void insertSlotsSameDayMethodTest3(){
        PanelMemberTimeSlotsController testinstance =new PanelMemberTimeSlotsController();
        PanelMemberTimeSlotsController.calEvent callEventInstance =new  PanelMemberTimeSlotsController.calEvent();
        callEventInstance.allDay=true;
        callEventInstance.title='';
        callEventInstance.className='';
        callEventInstance.title='';
        callEventInstance.url='';
        
        PanelMemberTimeSlotsController.TimeSlots t =new PanelMemberTimeSlotsController.TimeSlots();
        t.startdate=system.today().adddays(1);
        t.enddate=system.today().adddays(1);
        t.starttime='9 AM';
        t.endtime='11 AM';
        testinstance.timeslotwrapper.add(t);
        system.debug('testinstance.timeslotwrapper -->'+ testinstance.timeslotwrapper);
        testinstance.ShowModal();
       // testinstance.insertrecords=true;
        testinstance.save();
        
    }
    Public static testmethod void insertSlotsErrorDayTest(){
        PanelMemberTimeSlotsController testinstance =new PanelMemberTimeSlotsController();
        PanelMemberTimeSlotsController.calEvent callEventInstance =new  PanelMemberTimeSlotsController.calEvent();
        callEventInstance.allDay=true;
        callEventInstance.title='';
        callEventInstance.className='';
        callEventInstance.title='';
        callEventInstance.url='';
        
        PanelMemberTimeSlotsController.TimeSlots t =new PanelMemberTimeSlotsController.TimeSlots();
        t.startdate=system.today().adddays(2);
        t.enddate=system.today();
        t.starttime='1 AM';
        t.endtime='11 AM';
        testinstance.timeslotwrapper.add(t);
        system.debug('  testinstance.timeslotwrapper -->'+ testinstance.timeslotwrapper);
        testinstance.ShowModal();
        testinstance.insertrecords=true;
        testinstance.save();
        
    }
        Public static testmethod void insertSlotsErrorTimimgTest(){
        PanelMemberTimeSlotsController testinstance =new PanelMemberTimeSlotsController();
        PanelMemberTimeSlotsController.calEvent callEventInstance =new  PanelMemberTimeSlotsController.calEvent();
        callEventInstance.allDay=true;
        callEventInstance.title='';
        callEventInstance.className='';
        callEventInstance.title='';
        callEventInstance.url='';
        
        PanelMemberTimeSlotsController.TimeSlots t =new PanelMemberTimeSlotsController.TimeSlots();
        t.startdate=system.today();
        t.enddate=system.today();
        t.starttime='11 AM';
        t.endtime='7 AM';
        testinstance.timeslotwrapper.add(t);
        system.debug('  testinstance.timeslotwrapper -->'+ testinstance.timeslotwrapper);
        testinstance.ShowModal();
        testinstance.insertrecords=true;
        testinstance.save();
        testinstance.insertrecords=true;      
    }
    Public static testmethod void pageLoadMethodTest(){
        Employee__c testemployee=new Employee__c();
        testemployee.Name='Test Employee';
        testemployee.Interview_Level__c='1';
        //testemployee.Gender__c='Male';
        insert testemployee;
        
        Preffered__c s=new Preffered__c();
        s.Start_Date_Time__c=system.today();
        s.End_Date_Time__c=system.today().adddays(3);
        s.Available_Slots__c='11 AM-2 PM';
        s.Employee__c= testemployee.Id;
        insert s;
        system.debug('s -->'+s);
        
        apexpages.currentPage().getParameters().put('id', testemployee.id);
        
        PanelMemberTimeSlotsController testinstance =new PanelMemberTimeSlotsController();
        testinstance.pageLoad();
        
        PanelMemberTimeSlotsController.calEvent callEventInstance =new  PanelMemberTimeSlotsController.calEvent();
        callEventInstance.allDay=true;
        callEventInstance.title='';
        callEventInstance.className='';
        callEventInstance.title='';
        callEventInstance.url='';
        
        PanelMemberTimeSlotsController.TimeSlots t =new PanelMemberTimeSlotsController.TimeSlots();
        t.startdate=system.today();
        t.enddate=system.today();
        t.starttime='1 AM';
        t.endtime='11 AM';
        testinstance.timeslotwrapper.add(t);
        system.debug('  testinstance.timeslotwrapper -->'+ testinstance.timeslotwrapper);
        testinstance.ShowModal();    
    }
    */
    
}