public class UpdateOpportunitiesWrapper {

    @AuraEnabled
    public static list<wrapperClass> getOpp(){
        list<wrapperClass> returnwrapperClass=new list<wrapperClass>();
        
        list<opportunity> oppList=[select id,name,stagename from opportunity where stageName!='Closed Won' limit 10 ];
        for(opportunity o:oppList){
            
            system.debug('current opportunity'+o);
            wrapperClass newwrapper=new wrapperClass(o);
            returnwrapperClass.add(newwrapper);
            
        }
        system.debug(returnwrapperClass+'returned wrapper class');
        return returnwrapperClass;
         
    }
    @AuraEnabled
    public static boolean updateList(list<String> opplist){
        
        list<Opportunity> opp=[select id,name,StageName from opportunity where id in:opplist];
        
        for(opportunity o:opp){
            o.StageName='Closed Won';
            update opp;
        }
        return true;
    }
    public class wrapperClass{
        
        @AuraEnabled public Opportunity opp{get;set;}
        @AuraEnabled public boolean checkbox{get;set;}
        
        public wrapperClass(Opportunity o){
            opp=o;
            checkbox=false;
        }
    } 
}