public class GetAccounts {
    @AuraEnabled
    public static list<Account> getAccountDetails(){
        return [select id,name,phone from account limit 10];
    }

}