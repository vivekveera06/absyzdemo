public class sfdc29class {
	
    public List<cContact> contactList {get; set;}
  public  List<Contact> selectedContacts{get;set;}

public List<cContact> getContacts() {
    if(contactList == null) {
        contactList = new List<cContact>();
        for(Contact c: [select Id, Name, Email, Phone from Contact limit 10]) {

            contactList.add(new cContact(c));
        }
    }
    return contactList;
}
 
 
public PageReference  selectedlist() {
 
    selectedContacts = new List<Contact>();
 
 for(cContact cCon: getContacts()) {
        if(cCon.selected == true) {
            selectedContacts.add(cCon.con);
        }
    }
 
    System.debug('These are the selected Contacts...');
    for(Contact con: selectedContacts) {
        system.debug(con);
    }
    delete selectedContacts;
        pagereference page=new pagereference('/003/o');
    page.setRedirect(true);
    return null;

     
}
 
 
public class cContact {
    public Contact con {get; set;}
    public Boolean selected {get; set;}
 
       public cContact(Contact c) {
        con = c;
        selected = false;
    }
}
    
    
    
}