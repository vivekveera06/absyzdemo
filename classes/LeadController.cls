public class LeadController {
	@auraEnabled
    public static list<Lead> saveLead(Lead listLead){
        upsert listLead;
        return  [select id , name,Phone, Status, NumberOfEmployees from lead order by createddate desc limit 5];
    }
    @auraEnabled
    public static list<lead> getleads(){
        return [select id , name,Phone, Status, NumberOfEmployees from lead order by createddate desc limit 5];
    }
    
}