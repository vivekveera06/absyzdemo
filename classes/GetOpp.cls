public class GetOpp {

    @AuraEnabled
    public static List<Opportunity> getOppMethod(string accid){
        
        list<opportunity> opplist=[select id,name from opportunity where AccountId=:accid];
        return opplist;
        
    }
}