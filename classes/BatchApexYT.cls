global class BatchApexYT implements Database.Batchable<sObject> {
    
    public final string  query;
     public final string sobj;
     public final  string field;
     public final string field_value;
   global BatchApexYT(string q,string s,string f,string v){
        query=q;
        sobj=s;
        field=f;
        field_value=v;
        
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC,list<sObject> scope)
    {
        for(sObject a:scope)
        {
            a.put(field,field_value);
        }
        update scope;
        
    }
    global void finish(Database.BatchableContext BC){}
}