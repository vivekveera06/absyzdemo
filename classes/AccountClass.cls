public class AccountClass {

    @AuraEnabled
    public static list<Opportunity> getOpp(string aid){
        return[select id,Name,Amount,CloseDate from Opportunity where AccountId=:aid];
        
    }
}