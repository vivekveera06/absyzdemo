global with sharing class RenewAccessWithRefresh {
   	public static string code;
    public RenewAccessWithRefresh(){
        code = ApexPages.currentPage().getParameters().get('code');
    }
    
   /* @remoteaction
    global static String renew(){ 
        //renew access with refresh token every time an attachment is about to be created
        System.debug('inside renewing');
        String errorMessage ='';
        String accesstoken='';
        List<Renewaccess__c> renewal = new List<Renewaccess__c>();
        renewal = Renewaccess__c.getAll().values();
        String consumerKey=renewal[0].consumerKey__c;
        String clientSecret=renewal[0].clientSecret__c;
        String refreshToken=renewal[0].refreshToken__c;
        String redirect_uri=renewal[0].redirecturi__c;
        
        
        //uri to redirect after retrieving refresh token
        Http http = new Http();
        HttpRequest httpReq = new HttpRequest();
        HttpResponse httpRes = new HttpResponse();
        httpReq.setEndpoint('https://www.googleapis.com/oauth2/v4/token');
        httpReq.setMethod('POST');
        httpReq.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        //get refresh token from custom setting and use it to send http request
        System.debug('#### refreshToken '+refreshToken);
        String refreshTokenBody = 'client_id='+consumerKey+'&client_secret='+clientSecret+'&refresh_token='+refreshToken
            +'&grant_type=refresh_token';
        System.debug('#### refreshTokenBody '+refreshTokenBody);
        
        httpReq.setBody(refreshTokenBody);
        
        
        if (!Test.isRunningTest()){
            // resp = http.send(req);
            httpRes = http.send(httpReq);                    
        }
        /*else if(Test.isRunningTest()){
			MockHttpResponseGenerator res = new MockHttpResponseGenerator();
			httpRes=res.respond(httpReq);
		}
        
        //  httpRes = http.send(httpReq); 
        if(httpRes.getStatusCode() == 200){              //200 status code is successful status
            System.debug('inside 200status of renewing');
            Map<String,object> TokenInfo = (Map<String,object>)JSON.deserializeUntyped(httpRes.getBody());
            
            accesstoken=String.valueOf(TokenInfo.get('access_token'));
            system.debug('accesstoken ->'+accesstoken);
            
        }
        return accesstoken;
        
    }*/
   
    @remoteaction
    global static String renew(){ 
        String accesstoken;
        Integer expiresIn;
        String tokentype;
        List<Renewaccess__c> renewal = new List<Renewaccess__c>();
        renewal = Renewaccess__c.getAll().values();
        String Key=renewal[0].consumerKey__c;
        String secret=renewal[0].clientSecret__c;
        String redirect_uri=renewal[0].redirecturi__c;
        system.debug('key'+key);
        //Getting access token from google
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint('https://accounts.google.com/o/oauth2/token');
        req.setHeader('content-type', 'application/x-www-form-urlencoded');
        String messageBody = 'code=' + code + '&client_id=' + key + '&client_secret=' + secret + '&redirect_uri=' + redirect_uri + '&grant_type=authorization_code';
        req.setHeader('Content-length', String.valueOf(messageBody.length()));
        req.setBody(messageBody);
        req.setTimeout(60000);
        Http h = new Http();
        String resp;
        HttpResponse res = h.send(req);
        resp = res.getBody();
        JSONParser parser = JSON.createParser(resp);
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)) {
                String fieldName = parser.getText();
                parser.nextToken();
                if (fieldName == 'access_token') {
                    accesstoken = parser.getText();
                } else if (fieldName == 'expires_in') {
                    expiresIn = parser.getIntegerValue();
                } else if (fieldname == 'token_type') {
                    tokentype = parser.getText();
                }
            } }
        System.debug(' You can parse the response to get the access token ::: ' + resp);
        system.debug('accesstoken '+accesstoken);
        return accesstoken;
    }
}