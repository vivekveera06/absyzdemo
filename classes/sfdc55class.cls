public class sfdc55class {

    public void apexApproval(){
        Account a=[select id,name from Account order by createddate desc limit 1];
        user u=[select id from user where Alias='pvive'];
        //creating approval request
        Approval.ProcessSubmitRequest re= new Approval.ProcessSubmitRequest();
       // re.setComments('submitting for request');
        re.setObjectId(a.Id);
        // now I am submitting on behalf of submitter.
        re.setSubmitterId(u.id);
        // now I submit the record to specific process and  we can skip criteria evaluation.
        re.setProcessDefinitionNameOrId('emailUpdate');
        re.setSkipEntryCriteria(true);
        // now i submit the approval request for account
        Approval.ProcessResult rs=Approval.process(re);
        
    }
}