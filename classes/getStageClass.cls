public class getStageClass {
	
    @AuraEnabled
    public static list<string> getValues(){
        
        
        list<string> options=new list<string>();
        Schema.DescribeFieldResult fieldResult = Opportunity.stagename.getDescribe();
        List<Schema.PicklistEntry> ple=fieldResult.getPicklistValues();
        for(Schema.PicklistEntry p:ple){
            options.add(p.getLabel());
        }
        return options;
    } 
}