public class UpdatedAccountsapex {
	@AuraEnabled
    public static list<Account> getaccounts(){
        List<Account> acc = [select id, name, Phone, Website, Rating, AnnualRevenue, Type from Account order by LastModifiedDate desc Limit 20];
        return acc;
    }
}