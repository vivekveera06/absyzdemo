import { LightningElement,track } from 'lwc';

export default class ConditionalRendering extends LightningElement {

    @track show= true;

    handleClick(){
        this.show = !this.show;

    }


}