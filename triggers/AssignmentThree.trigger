trigger AssignmentThree on Invoices__c (before delete) {
		if(trigger.isDelete && trigger.isBefore)
        {
            for(Invoices__c c:trigger.old)
            {
                if(c.status__c=='closed')
                    c.addError('delete not possible when status is closed');
                
                
            }
            
        }
}