trigger Bstatus on B__c (after insert) {

    public  list<A__c> a=new list<A__c>();    
    for(B__c b:trigger.new){
       
        if(b.status__c=='Active'){
            /* for(A__c ac:[select id,B_Statuscheck__c from A__c where id=:b.Bc__c]){}
                 the above for loop can also written in different manner*/
                  A__c ac=new A__c(id=b.Bc__c);
                
                ac.B_Statuscheck__c=true;
                a.add(ac);
            
           
        }
        
    } 
    update a;
}