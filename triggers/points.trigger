trigger points on Opportunity (before insert,before update) {
    for(Opportunity op:trigger.new)
    {
        if(op.Total_Reward_Points__c<10)
        {
            decimal d=op.Total_Reward_Points__c;
            decimal d1=d/10;
            if(d1!=0)
            {
                
                op.Total_Reward_Points__c.addError('Total Reward Points can only be a multiple of 10. Please correct the value');
            }
        }
    }
    
}