trigger sfdc55trigger on Account (after update) {

   // public static void callApprovalProcess(list<Account> tiggerNew,map<id,Account> tiggerOldmap){
        
        List<Approval.ProcessSubmitRequest> approvalReqList=new List<Approval.ProcessSubmitRequest>();
        
        for(Account fi: trigger.New){
          
                    //if(orderMap.containsKey(OrderItemMap.get(fi.id)) && CatalogMap.containsKey(catalogItemMap.get(fi.id)) ){                      
                        if(fi.Site=='absyz'){
                      
                       // create the new approval request to submit    
                     Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();   
                     req.setComments('Submitted for approval. Please approve.');
                     req.setObjectId(fi.Id);
                     approvalReqList.add(req);        
                    }
          
          
        }
        // submit the approval request for processing        
          List<Approval.ProcessResult> resultList = Approval.process(approvalReqList);        
          // display if the reqeust was successful
          for(Approval.ProcessResult result: resultList )
          {        
          System.debug('Submitted for approval successfully: '+result.isSuccess());      
          }
        
}